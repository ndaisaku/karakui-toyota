//
//  HelloWorldLayer.mm
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 13/02/05.
//  Copyright __MyCompanyName__ 2013年. All rights reserved.
//

// Import the interfaces
#import "HelloWorldLayer2.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#import "PhysicsSprite.h"
#import "GB2ShapeCache.h"

enum {
	kTagParentNode = 1,
    kTagParentNode2 = 2,
};


#pragma mark - HelloWorldLayer

@interface HelloWorldLayer2()
-(void) initPhysics;
-(void) addNewSpriteAtPosition:(CGPoint)p;
@end

@implementation HelloWorldLayer2

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer2 *layer = [HelloWorldLayer2 node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
	if( (self=[super init])) {
        
		// enable events
		
		self.isTouchEnabled = YES;
		self.isAccelerometerEnabled = NO;
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        
        CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:@"p8.pvr.gz"];
        spriteBack = [CCSprite spriteWithTexture:background rect:CGRectMake(0,0,winSize.width,winSize.height)];
        spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
        [self addChild: spriteBack];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        
        //CGSize s = [CCDirector sharedDirector].winSize;
		
		// init physics
		[self initPhysics];
		
		// create reset button
		//[self createMenu];
		
		//Set up sprite
        cloudFrameCash = [CCSpriteFrameCache sharedSpriteFrameCache];
        [cloudFrameCash addSpriteFramesWithFile:@"p8_amagumo.plist"];
        
        cloudBatch = [CCSpriteBatchNode batchNodeWithFile:@"p8_amagumo.pvr.gz"];
        [self addChild:cloudBatch];
        
        
#if 1
		// Use batch node. Faster
        CCSpriteFrameCache *gearFrame = [CCSpriteFrameCache sharedSpriteFrameCache];
        [gearFrame addSpriteFramesWithFile:@"p8_gear.plist"];
        CCSpriteBatchNode *parent = [CCSpriteBatchNode batchNodeWithFile:@"p8_gear.pvr.gz"];
        
        CCSpriteFrameCache *gearFrame2 = [CCSpriteFrameCache sharedSpriteFrameCache];
        [gearFrame2 addSpriteFramesWithFile:@"p8_gear2.plist"];
        CCSpriteBatchNode *parent2 = [CCSpriteBatchNode batchNodeWithFile:@"p8_gear2.pvr.gz"];
        
		spriteTexture_ = [parent texture];
		spriteTexture2_ = [parent2 texture];
#else
		// doesn't use batch node. Slower
		spriteTexture_ = [[CCTextureCache sharedTextureCache] addImage:@"p8_gear_center.png"];
		CCNode *parent = [CCNode node];
        
        spriteTexture2_ = [[CCTextureCache sharedTextureCache] addImage:@"p8_gear2.png"];
		CCNode *parent2 = [CCNode node];
#endif
		[self addChild:parent z:2 tag:kTagParentNode];
		[self addChild:parent2 z:3 tag:kTagParentNode2];
        
        //gearSprite = [CCSprite spriteWithSpriteFrameName:@"p8_gear.png"];
        //[self addChild:gearSprite];
		
        //gearSpriteCenter = [CCSprite spriteWithSpriteFrameName:@"p8_gear2.png"];
        //[self addChild:gearSpriteCenter];
        
        [self addNewSpriteAtPosition:ccp(CCRANDOM_0_1()*1000, CCRANDOM_0_1()*150)];
        
        
        mojiHidden = true;
        
        
        //メニュー
        CCSpriteFrameCache *menuSprite = [CCSpriteFrameCache sharedSpriteFrameCache];
        [menuSprite addSpriteFramesWithFile:@"common.plist"];
        CCSpriteBatchNode *menuSpriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"common.pvr.gz"];
        [self addChild:menuSpriteSheet];
        
        CCSprite *menu_button_text = [CCSprite spriteWithSpriteFrameName:@"menu_button_text.png"];
        CCSprite *menu_button_text2 = [CCSprite spriteWithSpriteFrameName:@"menu_button_text.png"];
        
        CCSprite *menu_button_home = [CCSprite spriteWithSpriteFrameName:@"menu_button_home.png"];
        CCSprite *menu_button_home2 = [CCSprite spriteWithSpriteFrameName:@"menu_button_home.png"];
        
        CCSprite *menu_button_help = [CCSprite spriteWithSpriteFrameName:@"menu_button_help.png"];
        CCSprite *menu_button_help2 = [CCSprite spriteWithSpriteFrameName:@"menu_button_help.png"];
        
        
        CCMenuItemSprite *menuText = [CCMenuItemSprite itemWithNormalSprite:menu_button_text
                                                             selectedSprite:menu_button_text2
                                                                     target:self
                                                                   selector:@selector(mainmenuAction:)];
        
        CCMenuItemSprite *menuHome = [CCMenuItemSprite itemWithNormalSprite:menu_button_home
                                                             selectedSprite:menu_button_home2
                                                                     target:self
                                                                   selector:@selector(mainmenuAction:)];
        menuHome.position = ccp(menuText.contentSize.width+20,0);
        
        CCMenuItemSprite *menuHelp = [CCMenuItemSprite itemWithNormalSprite:menu_button_help
                                                             selectedSprite:menu_button_help2
                                                                     target:self
                                                                   selector:@selector(mainmenuAction:)];
        menuHelp.position = ccp(menuText.contentSize.width*2+40,0);
        
        menuText.tag = 1;
        menuHome.tag = 2;
        menuHelp.tag = 3;
        
        CCMenu *mainMenu = [CCMenu menuWithItems:menuText,menuHome,menuHelp,nil];
        mainMenu.position = ccp(40,40);
        [self addChild:mainMenu z:11];
        
        
        //テキスト
        textBg = [CCSprite spriteWithSpriteFrameName:@"text_bg.png"];
        textBg.position= ccp(textBg.contentSize.width/2+16,textBg.contentSize.height/2+menuText.contentSize.height+30);
        [self addChild:textBg z:12];
        
        
        CCSpriteFrameCache *text = [CCSpriteFrameCache sharedSpriteFrameCache];
        [text addSpriteFramesWithFile:@"text.plist"];
        CCSpriteBatchNode *spriteText = [CCSpriteBatchNode batchNodeWithFile:@"text.pvr.gz"];
        [self addChild:spriteText];
        
        textImage = [CCSprite spriteWithSpriteFrameName:@"text_p1.png"];
        textImage.anchorPoint = ccp(0.0f,1.0f);
        textImage.position = ccp(20,textBg.contentSize.height-20);
        [textBg addChild:textImage];
        
        //ヘルプ
        helpBg = [CCSprite spriteWithSpriteFrameName:@"helpBg.png"];
        helpBg.scale = 0.0;
        helpBg.anchorPoint = ccp(0.5f,0.0f);
        helpBg.position = ccp(helpBg.contentSize.width/2+50,70);
        [self addChild:helpBg z:13];
        
        
		
        [self scheduleUpdate];
	}
	return self;
}

-(void)mainmenuAction:(id)no{
    if ([no tag] == 1) {
        if(!mojiHidden){
            id Fade = [CCFadeTo actionWithDuration:0.3f opacity:255];
            id Fade2 = [CCFadeTo actionWithDuration:0.3f opacity:255];
            [textImage runAction:Fade];
            [textBg runAction:Fade2];
            mojiHidden = true;
            
        }else{
            
            id Fade = [CCFadeTo actionWithDuration:0.3f opacity:0];
            id Fade2 = [CCFadeTo actionWithDuration:0.3f opacity:0];
            [textImage runAction:Fade];
            [textBg runAction:Fade2];
            mojiHidden = false;
            
        }
        
        if (helpHidden) {
            id scale =[CCScaleTo actionWithDuration:0.5f scale:0.0];
            id ease = [CCEaseExponentialInOut actionWithAction:scale];
            [helpBg runAction:ease];
            
            helpHidden = false;
        }
        
        
    }
    else if ([no tag] == 3) {
        if(!mojiHidden){
            if (!helpHidden) {
                id scale =[CCScaleTo actionWithDuration:0.5f scale:1.0];
                id ease = [CCEaseExponentialOut actionWithAction:scale];
                [helpBg runAction:ease];
                helpHidden = true;
            }else{
                id scale =[CCScaleTo actionWithDuration:0.5f scale:0.0];
                id ease = [CCEaseExponentialInOut actionWithAction:scale];
                [helpBg runAction:ease];
                helpHidden = false;
            }
            
        }else{
            if (!helpHidden) {
                id scale =[CCScaleTo actionWithDuration:0.5f scale:1.0];
                id ease = [CCEaseExponentialOut actionWithAction:scale];
                id Fade = [CCFadeTo actionWithDuration:0.3f opacity:0];
                id Fade2 = [CCFadeTo actionWithDuration:0.3f opacity:0];
                
                [textImage runAction:Fade];
                [textBg runAction:Fade2];
                
                [helpBg runAction:ease];
                helpHidden = true;
                mojiHidden = false;
                
            }else{
                id scale =[CCScaleTo actionWithDuration:0.5f scale:0.0];
                id ease = [CCEaseExponentialInOut actionWithAction:scale];
                id Fade = [CCFadeTo actionWithDuration:0.3f opacity:255];
                id Fade2 = [CCFadeTo actionWithDuration:0.3f opacity:255];
                
                [textImage runAction:Fade];
                [textBg runAction:Fade2];
                [helpBg runAction:ease];
                
                helpHidden = false;
            }
        }
        
    }
}

-(void) dealloc
{
	delete world;
	world = NULL;
	
	delete m_debugDraw;
	m_debugDraw = NULL;
	
	[super dealloc];
}	


-(void) initPhysics
{
	
	CGSize s = [[CCDirector sharedDirector] winSize];
	
	b2Vec2 gravity;
	gravity.Set(20.0, 0.0f);
	world = new b2World(gravity);
	
	
	// Do we want to let bodies sleep?
	world->SetAllowSleeping(true);
	
	world->SetContinuousPhysics(true);
	
	m_debugDraw = new GLESDebugDraw( PTM_RATIO );
	world->SetDebugDraw(m_debugDraw);
	
	uint32 flags = 0;
    flags += b2Draw::e_shapeBit;
    flags += b2Draw::e_jointBit;
    flags += b2Draw::e_aabbBit;
    flags += b2Draw::e_pairBit;
    flags += b2Draw::e_centerOfMassBit;
	m_debugDraw->SetFlags(flags);
	
	
	// Define the ground body.
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0, 0); // bottom-left corner
	
	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	groundBody = world->CreateBody(&groundBodyDef);
	
	// Define the ground box shape.
	b2EdgeShape groundBox;		
	
	// bottom
	
	groundBox.Set(b2Vec2(0,0), b2Vec2(s.width/PTM_RATIO,0));
	groundBody->CreateFixture(&groundBox,0);
	
	// top
	groundBox.Set(b2Vec2(0,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO));
	groundBody->CreateFixture(&groundBox,0);
	
	// left
	groundBox.Set(b2Vec2(0,s.height/PTM_RATIO), b2Vec2(0,0));
	groundBody->CreateFixture(&groundBox,0);
	
	// right
	groundBox.Set(b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,0));
	groundBody->CreateFixture(&groundBox,0);
}

-(void) draw
{
	//
	// IMPORTANT:
	// This is only for debug purposes
	// It is recommend to disable it
	//
	[super draw];
	
	ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
	
	kmGLPushMatrix();
	
	world->DrawDebugData();	
	
	kmGLPopMatrix();
}

-(void) addNewSpriteAtPosition:(CGPoint)p
{
    
    //CCLOG(@"Add sprite %0.2f x %02.f",p.x,p.y);
	CCNode *parent = [self getChildByTag:kTagParentNode];
	CCNode *parent2 = [self getChildByTag:kTagParentNode2];
    
	//We have a 64x64 sprite sheet with 4 different 32x32 images.  The following code is
	//just randomly picking one of the images
	PhysicsSprite *sprite = [PhysicsSprite spriteWithTexture:spriteTexture_ rect:CGRectMake(2,2.5,159,718)];
	[parent addChild:sprite];
    sprite.position = ccp( p.x, p.y);
    
    PhysicsSprite *sprite2 = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(2,1,32,32)];
	[parent2 addChild:sprite2];
    sprite2.position = ccp( p.x, p.y);

    
    //float boxhsize = gearSprite.contentSize.height / PTM_RATIO*0.5f;
    //float boxwsize = gearSprite.contentSize.width / PTM_RATIO*0.5f;
    
    //float boxhsize2 = gearSpriteCenter.contentSize.height / PTM_RATIO*0.5f;
    //float boxwsize2 = gearSpriteCenter.contentSize.width / PTM_RATIO*0.5f;
	
	// Define the dynamic body.
    
    b2CircleShape circle1;
    circle1.m_radius = 2.0f;
    
    b2CircleShape circle2;
    circle2.m_radius = 0.5f;
    
    b2PolygonShape box;
    box.SetAsBox(0.5f, 10.0f);
    
    b2BodyDef bd1;
    bd1.type = b2_dynamicBody;
    bd1.position.Set(16.0f, 12.0f);
    bd1.angularDamping = 10.0f;
    b2Body* body1 = world->CreateBody(&bd1);

    
    [[GB2ShapeCache sharedShapeCache] addShapesWithFile:@"p8_gear_polygon.plist"];
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:body1 forShapeName:@"p8_polygon"];
    
    
    b2RevoluteJointDef jd1;
    jd1.bodyA = groundBody;
    jd1.bodyB = body1;
    jd1.localAnchorA = bd1.position;
    jd1.localAnchorB = body1->GetLocalPoint(bd1.position);
    jd1.referenceAngle = body1->GetAngle() - groundBody->GetAngle();
    m_joint1 = (b2RevoluteJoint*)world->CreateJoint(&jd1);
    
    b2BodyDef bd2;
    bd2.type = b2_dynamicBody;
    bd2.position.Set(17.2f, 12.0f);
    bd2.angularDamping = 10.0f;
    body2 = world->CreateBody(&bd2);
    
    b2FixtureDef fixtureDef2;
    fixtureDef2.shape = &circle2;
    fixtureDef2.density = 1.0f;
    fixtureDef2.restitution = 0.01f;
    fixtureDef2.filter.maskBits = 0x0000;
	body2->CreateFixture(&fixtureDef2);
    
        
    b2RevoluteJointDef jd2;
    jd2.Initialize(groundBody, body2, bd2.position);
    jd2.collideConnected = false;
    m_joint2 = (b2RevoluteJoint*)world->CreateJoint(&jd2);
    
    
    b2GearJointDef jd4;
    jd4.bodyA = body1;
    jd4.bodyB = body2;
    jd4.joint1 = m_joint1;
    jd4.joint2 = m_joint2;
    jd4.ratio = 1.0f;
    m_joint4 = (b2GearJoint*)world->CreateJoint(&jd4);
    
    [sprite setPhysicsBody:body1];
    [sprite2 setPhysicsBody:body2];
    

    
}
-(void) p8AddCloud:(int) no{
    
    if(no == 1 && !boolAmagumi1){
        int spriteNo = CCRANDOM_0_1()*4+1;
        int positionWidth = CCRANDOM_0_1()*170;
        int positionHeight = CCRANDOM_0_1()*50;
        NSString *path = [NSString stringWithFormat:@"p8_amagumo%d.png",spriteNo];
        CCSprite *amagumo1 = [CCSprite spriteWithSpriteFrameName:path];
        amagumo1.position =ccp(420+positionWidth, 630+positionHeight);
        amagumo1.scale = 0.0;
        [self addChild:amagumo1 z:1];
        
        id Scale = [CCScaleTo actionWithDuration:0.5 scale:1.0];
        id Ease = [CCEaseBounceOut actionWithAction:Scale];
        
        [amagumo1 runAction:Ease];
    }
}

-(void) update: (ccTime) dt
{
	//It is recommended that a fixed time step is used with Box2D for stability
	//of the simulation, however, we are using a variable time step here.
	//You need to make an informed choice, the following URL is useful
	//http://gafferongames.com/game-physics/fix-your-timestep/
	
	int32 velocityIterations = 8;
	int32 positionIterations = 1;
    
	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	world->Step(dt, velocityIterations, positionIterations);
    
   
    
    for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
        if (b->GetUserData() != NULL) {
            CCSprite *sprite = (CCSprite*)b->GetUserData();
            if(sprite.position.x/PTM_RATIO < -19.0){
                [self removeChild:sprite cleanup:YES];
                world->DestroyBody(b);
                [self ballCriate];
            }
        }
        
        
    }
    
}
//消えたボールを生成する
- (void)ballCriate{
    [self addNewSpriteAtPosition:ccp(400+CCRANDOM_0_1()*600, CCRANDOM_0_1()*150)];
}
- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	//Add a new body/atlas sprite at the touched location

    if (_mouseJoint != NULL) return;
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];

    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    for (b2Body* b = world->GetBodyList(); b; b = b->GetNext()){
        if (b->GetType() == b2_dynamicBody) {
            for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext()) {
                if (f->TestPoint(locationWorld)) {
                        b2MouseJointDef md;
                        md.bodyA = groundBody;
                        md.bodyB = b;
                        md.target = locationWorld;
                        md.collideConnected = true;
                        md.maxForce = 50000.0f * b->GetMass();
                        
                        _mouseJoint = (b2MouseJoint *)world->CreateJoint(&md);
                        b->SetAwake(true);
                    }
                }
            }
        }
}
    
-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint == NULL) return;
    
    if(body2->GetAngle() >= 3*routNo && routNo <= 50){
        CCLOG(@"%d",routNo);
        [self p8AddCloud:1];
        routNo += 1;
    }
    
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    
    _mouseJoint->SetTarget(locationWorld);
    
}

-(void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint) {
        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
    
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_mouseJoint) {
        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }  
}

//デバイスの傾き
- (void)deviceOrientationDidChange:(NSNotification*)notification {
    UIDeviceOrientation orientation;
    orientation = [UIDevice currentDevice].orientation;
    if(orientation == UIDeviceOrientationUnknown) {
        NSLog(@"不明");
    }
    if(orientation == UIDeviceOrientationPortrait) {
        NSLog(@"縦(ホームボタン下)");
    }
    if(orientation == UIDeviceOrientationPortraitUpsideDown) {
        NSLog(@"縦(ホームボタン上)");
    }
    if(orientation == UIDeviceOrientationLandscapeLeft) {
        NSLog(@"横(ホームボタン右)");
    }
    if(orientation == UIDeviceOrientationLandscapeRight) {
        NSLog(@"横(ホームボタン左)");
    }
}

#pragma mark GameKit delegate

-(void) achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

-(void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

@end
