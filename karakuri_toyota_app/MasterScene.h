#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCTransitionHelper.h"
#import "SimpleAudioEngine.h"
#import "MenuObject.h"
#import "SoundEffect.h"

#import "p0.h"
#import "p1.h"
#import "p2.h"
#import "p3.h"
#import "p4.h"
#import "p5.h"
#import "p6.h"
#import "p7.h"
#import "p8.h"
#import "p9.h"
#import "p10.h"
#import "p11.h"
//#import "p12.h"
#import "p13.h"
#import "p14.h"
#import "p15.h"
#import "CDXPropertyModifierAction.h"



@interface MasterScene : CCScene {
}
+ (CCScene *)sceneWithSceneId:(NSInteger)sceneId nextBool:(BOOL)nextPage;

@end

@interface FirstLayer : CCLayer<AVAudioPlayerDelegate,CDLongAudioSourceDelegate>{
    bool start;
    int optionnum3[3];
    CCSprite *spriteBack;
    int sceneId2;
    CDLongAudioSource *playnarra;
}

@property (nonatomic, retain)MenuObject *menuObj;
@property (nonatomic, retain)p0 *p0;
@property (nonatomic, retain)p1 *p1;
@property (nonatomic, retain)p2 *p2;
@property (nonatomic, retain)p3 *p3;
@property (nonatomic, retain)p4 *p4;
@property (nonatomic, retain)p5 *p5;
@property (nonatomic, retain)p6 *p6;
@property (nonatomic, retain)p7 *p7;
@property (nonatomic, retain)p8 *p8;
@property (nonatomic, retain)p9 *p9;
@property (nonatomic, retain)p10 *p10;
@property (nonatomic, retain)p11 *p11;
//@property (nonatomic, retain)p12 *p12;
@property (nonatomic, retain)p13 *p13;
@property (nonatomic, retain)p14 *p14;
@property (nonatomic, retain)p15 *p15;
// 初期化処理を行った上でインスタンスを返すメソッド
+ (id)layerWithSceneId:(NSInteger)sceneId;
- (id) initWithSceneId:(NSInteger)sceneId;

@end


