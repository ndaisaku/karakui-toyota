//
//  p11.m
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "p11.h"
#import "AppDelegate.h"
#import "PhysicsSprite.h"
#import "Box2DExamples.h"
#import "GB2ShapeCache.h"

enum {
	kTagParentNode = 1,
	kTagParentNode2 = 2,
    kTagParentNode3 = 3,
    kTagParentNode4 = 4,
};

#define BUOYANCYOFFSET 90.0f

#pragma mark - p11

@interface p11()
-(void) initPhysics;
-(void) addNewSpriteAtPosition:(CGPoint)p;
@end

@implementation p11

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	p11 *layer = [p11 node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id)init{
    if(self = [super init]){
        self.ignoreAnchorPointForPosition = NO;
        self.isTouchEnabled = YES;
        winsize = [[CCDirector sharedDirector] winSize];

        [self preloadParticleEffect:@"p11_particle.plist"];
        [self preloadParticleEffect:@"p11_particle2.plist"];
        
        CCSpriteFrameCache *frameChash = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameChash addSpriteFramesWithFile:@"p11Resource.plist"];
        kago = [CCSprite spriteWithSpriteFrameName:@"p11_kago.png"];
        kago.position = ccp(360,290);
        [self addChild:kago z:6];
        
        usagi = [CCSprite spriteWithSpriteFrameName:@"p11_usagi.png"];
        usagi.position = ccp(435,300);
        [self addChild:usagi z:8];
       
        
        funeButton = [CCSprite spriteWithSpriteFrameName:@"p11_funeButton.png"];
        funeButton.position = ccp(814,619);
        [self addChild:funeButton z:2];
        
        mount = [CCSprite spriteWithSpriteFrameName:@"p11_mount.png"];
        mount.position = ccp(801,435);
        [self addChild:mount z:2];
        
        CCSprite *fune2 = [CCSprite spriteWithSpriteFrameName:@"p11_fune.png"];
        fune2.position = ccp(385,205);
        [self addChild:fune2 z:5];
        
        
        
        //蛇アニメーション
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"p5_hebi.plist"];
        bachNood = [CCSpriteBatchNode batchNodeWithFile:@"p5_hebi.png" ];
        [self addChild:bachNood z:2];
        
        
        
        //CGSize s = [CCDirector sharedDirector].winSize;
		
		// init physics
		[self initPhysics];
        
        CCSpriteFrameCache *p11framechash = [CCSpriteFrameCache sharedSpriteFrameCache];
        [p11framechash addSpriteFramesWithFile:@"p7Resource.plist"];
        
        CCSpriteBatchNode *p11batchNode = [CCSpriteBatchNode batchNodeWithFile:@"p7Resource.pvr.gz"];
        [self addChild:p11batchNode];
        
        
        //雲レイヤーの追加
        p11Kumo1 = [CCLayer node];
        p11Kumo1.anchorPoint = ccp(0.5f,1.0f);
        p11Kumo1.position = ccp(1100, 0);
        [self addChild:p11Kumo1 z:2];
        
        p11Kumo2 = [CCLayer node];
        p11Kumo2.anchorPoint = ccp(0.5f,1.0f);
        p11Kumo2.position = ccp(1100, 0);
        
        [self addChild:p11Kumo2 z:2];
        
        p11Kumo3 = [CCLayer node];
        p11Kumo3.anchorPoint = ccp(0.5f,1.0f);
        p11Kumo3.position = ccp(1100, 0);
        [self addChild:p11Kumo3 z:2];
        
        [self p11CloudCreat];

		
		//Set up sprite
		
#if 1
		// Use batch node. Faster
		CCSpriteBatchNode *parent = [CCSpriteBatchNode batchNodeWithFile:@"blocks2.png" capacity:100];
		spriteTexture_ = [parent texture];
        
        CCSpriteFrameCache *frameChache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameChache addSpriteFramesWithFile:@"p4_hana.plist"];
        CCSpriteBatchNode *parent2 = [CCSpriteBatchNode batchNodeWithFile:@"p11_hana.pvr.gz"];
        spriteTexture2_ = [parent2 texture];
        
#else
		// doesn't use batch node. Slower
		spriteTexture_ = [[CCTextureCache sharedTextureCache] addImage:@"blocks2.png"];
		CCNode *parent = [CCNode node];
        
        spriteTexture2_ = [[CCTextureCache sharedTextureCache] addImage:@"p4_hana.pvr.gz"];
		CCNode *parent2 = [CCNode node];
        
#endif
		[self addChild:parent z:3 tag:kTagParentNode];
        [self addChild:parent2 z:4 tag:kTagParentNode2];
        
        
		
		[self scheduleUpdate];
        [self addFlowerSprite];
    }
    return self;
}

-(void)preloadParticleEffect:(NSString *)particleFile{
    [CCParticleExplosion particleWithFile:particleFile];
}
-(void)kagoTouch{
    CCParticleSystem *particle;
    particle = [CCParticleSystemQuad particleWithFile:@"p11_particle.plist"];
    [self addChild:particle z:9];
    
    CCSprite *p11oosuno = [CCSprite spriteWithSpriteFrameName:@"p11_oosuno.png"];
    p11oosuno.position = ccp(winsize.width/2-200,winsize.height/2);
    p11oosuno.opacity = 0;
    [self addChild:p11oosuno z:7];
    
    id Fade = [CCFadeIn actionWithDuration:1.5];
    id seq = [CCSequence actions:[CCDelayTime actionWithDuration:1.0],Fade, nil];
    [p11oosuno runAction:seq];
    
    [self removeChild:kago cleanup:YES];
    
    for (int i=0; i<100; i++) {
        [self addNewSpriteAtPosition:ccp(CCRANDOM_0_1()*1000, CCRANDOM_0_1()*150)];
        
    }

    kagoHidden = true;
}

-(void)funeTouch{
    
    CCSprite *fune1 = [CCSprite spriteWithSpriteFrameName:@"p11_fune1.png"];
    fune1.position = ccp(762,550);
    [self addChild:fune1 z:1];
    
    CCSprite *fune2 = [CCSprite spriteWithSpriteFrameName:@"p11_fune2.png"];
    fune2.position = ccp(820,550);
    [self addChild:fune2 z:1];
    
    CCSprite *fune3 = [CCSprite spriteWithSpriteFrameName:@"p11_fune3.png"];
    fune3.position = ccp(888,550);
    [self addChild:fune3 z:1];
    
    
    id Move = [CCMoveTo actionWithDuration:0.6 position:ccp(762,664)];
    id Ease = [CCEaseElasticOut actionWithAction:Move];
    id seq = [CCSequence actions:[CCDelayTime actionWithDuration:0],Ease, nil];
    
    
    id Move2 = [CCMoveTo actionWithDuration:0.6 position:ccp(820,662)];
    id Ease2 = [CCEaseElasticOut actionWithAction:Move2];
    id seq2 = [CCSequence actions:[CCDelayTime actionWithDuration:0.05],Ease2, nil];
    
    id Move3 = [CCMoveTo actionWithDuration:0.6 position:ccp(888,661)];
    id Ease3 = [CCEaseElasticOut actionWithAction:Move3];
    id seq3 = [CCSequence actions:[CCDelayTime actionWithDuration:0.1],Ease3, nil];
    
    [fune1 runAction:seq];
    [fune2 runAction:seq2];
    [fune3 runAction:seq3];
    funeHidden = true;
}

-(void)yamaTouch{
    
    CCParticleSystem *particle;
    particle = [CCParticleSystemQuad particleWithFile:@"p11_particle2.plist"];
    [self addChild:particle z:18];
    
    NSMutableArray *hebiAnimeFrames =[NSMutableArray array];
    for(int i = 1; i<=2; i++){
        [hebiAnimeFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"p5_snake3_%d.png",i]]];
    }
    
    CCAnimation *hebiAnime2 = [CCAnimation animationWithSpriteFrames:hebiAnimeFrames delay:0.2f];
    
    CCSprite *hebiSprite = [CCSprite spriteWithSpriteFrameName:@"p5_snake3_1.png"];
    hebiSprite.scaleX = 0.45;
    hebiSprite.scaleY = 0.45;
    hebiSprite.rotation = -30;
    hebiSprite.anchorPoint = ccp(0, 0.5);
    hebiSprite.position = ccp(780,450);
    CCAnimate *chameleonAction = [CCAnimate actionWithAnimation:hebiAnime2];
    
    CCAction *hebiAnimation3 = [CCRepeat actionWithAction:chameleonAction times:5.0];
    CCSequence *seq = [CCSequence actions:[CCDelayTime actionWithDuration:1.5],hebiAnimation3, nil];
    
    [hebiSprite runAction:seq];
    [bachNood addChild:hebiSprite z:15];
    [SoundEffect sePlay:@"ofn_se_p11_snakemove.mp3"];

    hebiHidden = true;
}

//雲の生成
-(void)p11CloudCreat{
    
    //雲の高さを300〜480にランダムにする
    for (int j=1; j<=3; j++) {
        int ranPosyLayer = CCRANDOM_0_1()*180 + 300;
        int ranPosy;
        int ranPosx;
        
        for (int i=1; i<=7; i++) {
            int imageNo = CCRANDOM_0_1()*6+1;
            
            
            NSString *imagePath = [NSString stringWithFormat:@"p7_kumo_%d.png",imageNo];
            CCSprite *spriteCloud = [CCSprite spriteWithSpriteFrameName:imagePath];
            
            
            spriteCloud.scale = 0.66;
            ranPosy = CCRANDOM_0_1()*100+ranPosyLayer;
            ranPosx = CCRANDOM_0_1()*380;
            
            
            spriteCloud.position = ccp(ranPosx,ranPosy);
            
            switch (j) {
                case 1:
                    p11Kumo1.position = ccp(1400,120);
                    [p11Kumo1 addChild:spriteCloud];
                    break;
                case 2:
                    p11Kumo2.position = ccp(1400,120);
                    [p11Kumo2 addChild:spriteCloud];
                    break;
                case 3:
                    p11Kumo3.position = ccp(1400,120);
                    [p11Kumo3 addChild:spriteCloud];
                    break;
                    
                default:
                    break;
            }
        }
        
    }
    [self p11CloudMove];
}


//雲の移動
-(void)p11CloudMove{
    
    //雲のスピードをランダムにする
    int randCloudSpeed = CCRANDOM_0_1()*40+60;
    
    id move = [CCMoveBy actionWithDuration:randCloudSpeed position:ccp(-2000,0)];
    id move2 = [CCMoveBy actionWithDuration:randCloudSpeed position:ccp(-2000,0)];
    id move3 = [CCMoveBy actionWithDuration:randCloudSpeed position:ccp(-2000,0)];
    
    id func1 = [CCCallFunc actionWithTarget:self selector:@selector(p11CloudRemove_1)];
    id func2 = [CCCallFunc actionWithTarget:self selector:@selector(p11CloudRemove_2)];
    id func3 = [CCCallFunc actionWithTarget:self selector:@selector(p11CloudRemove_3)];

    
    id seq1 = [CCSequence actions:move,func1,nil];
    id seq2 = [CCSequence actions:[CCDelayTime actionWithDuration:3.0],move2,func2,nil];
    id seq3 = [CCSequence actions:[CCDelayTime actionWithDuration:6.0],move3,func3,nil];
    
    [p11Kumo1 runAction:seq1];
    [p11Kumo2 runAction:seq2];
    [p11Kumo3 runAction:seq3];
}

-(void)p11CloudCreate{
    //雲の高さを300〜480にランダムにする
    int ranPosyLayer = CCRANDOM_0_1()*180 + 300;
    int ranPosy;
    int ranPosx;
    
    for (int i=1; i<=7; i++) {
        int imageNo = CCRANDOM_0_1()*6+1;
        
        NSString *imagePath = [NSString stringWithFormat:@"p7_kumo_%d.png",imageNo];
        CCSprite *spriteCloud = [CCSprite spriteWithSpriteFrameName:imagePath];
        
        spriteCloud.scale = 0.66;
        ranPosy = CCRANDOM_0_1()*100+ranPosyLayer;
        ranPosx = CCRANDOM_0_1()*380;
        
        spriteCloud.position = ccp(ranPosx,ranPosy);
        
        p11Kumo1.position = ccp(1400,120);
        [p11Kumo1 addChild:spriteCloud];
    }
    [self p11CloudMove1];
}

-(void)p11CloudCreate2{
    //雲の高さを300〜480にランダムにする
    int ranPosyLayer = CCRANDOM_0_1()*180 + 300;
    int ranPosy;
    int ranPosx;
    
    for (int i=1; i<=7; i++) {
        int imageNo = CCRANDOM_0_1()*6+1;
        
        NSString *imagePath = [NSString stringWithFormat:@"p7_kumo_%d.png",imageNo];
        CCSprite *spriteCloud = [CCSprite spriteWithSpriteFrameName:imagePath];
        
        
        spriteCloud.scale = 0.66;
        ranPosy = CCRANDOM_0_1()*100+ranPosyLayer;
        ranPosx = CCRANDOM_0_1()*380;
        
        
        spriteCloud.position = ccp(ranPosx,ranPosy);
        
        p11Kumo2.position = ccp(1400,120);
        [p11Kumo2 addChild:spriteCloud];
    }
    [self p11CloudMove2];
}

-(void)p11CloudCreate3{
    //雲の高さを300〜480にランダムにする
    int ranPosyLayer = CCRANDOM_0_1()*180 + 300;
    int ranPosy;
    int ranPosx;
    
    for (int i=1; i<=7; i++) {
        int imageNo = CCRANDOM_0_1()*6+1;
        
        NSString *imagePath = [NSString stringWithFormat:@"p7_kumo_%d.png",imageNo];
        CCSprite *spriteCloud = [CCSprite spriteWithSpriteFrameName:imagePath];
        
        
        spriteCloud.scale = 0.66;
        ranPosy = CCRANDOM_0_1()*100+ranPosyLayer;
        ranPosx = CCRANDOM_0_1()*380;
        
        
        spriteCloud.position = ccp(ranPosx,ranPosy);
        
        p11Kumo3.position = ccp(1400,120);
        [p11Kumo3 addChild:spriteCloud];
    }
    [self p11CloudMove3];
}

-(void)p11CloudMove1{
    //雲のスピードをランダムにする
    int randCloudSpeed = CCRANDOM_0_1()*20+60;
    
    id move = [CCMoveBy actionWithDuration:randCloudSpeed position:ccp(-2000,0)];
    id func1 = [CCCallFunc actionWithTarget:self selector:@selector(p11CloudRemove_1)];
    id seq1 = [CCSequence actions:move,func1,nil];
    [p11Kumo1 runAction:seq1];
}


-(void)p11CloudMove2{
    //雲のスピードをランダムにする
    int randCloudSpeed = CCRANDOM_0_1()*20+60;
    
    id move = [CCMoveBy actionWithDuration:randCloudSpeed position:ccp(-2000,0)];
    id func1 = [CCCallFunc actionWithTarget:self selector:@selector(p11CloudRemove_2)];
    id seq1 = [CCSequence actions:move,func1,nil];
    [p11Kumo2 runAction:seq1];
}


-(void)p11CloudMove3{
    //雲のスピードをランダムにする
    int randCloudSpeed = CCRANDOM_0_1()*20+60;
    
    id move = [CCMoveBy actionWithDuration:randCloudSpeed position:ccp(-2000,0)];
    id func1 = [CCCallFunc actionWithTarget:self selector:@selector(p11CloudRemove_3)];
    id seq1 = [CCSequence actions:move,func1,nil];
    [p11Kumo3  runAction:seq1];
}

-(void)p11CloudRemove_1{
    [p11Kumo1 removeAllChildrenWithCleanup:YES];
    p11Kumo1 = [CCLayer node];
    p11Kumo1.position = ccp(0, 0);
    p11Kumo1.anchorPoint = ccp(0.5f,0.5f);
    [self addChild:p11Kumo1 z:2];
    [self p11CloudCreate];
}

-(void)p11CloudRemove_2{
    [p11Kumo2 removeAllChildrenWithCleanup:YES];
    p11Kumo2 = [CCLayer node];
    p11Kumo2.position = ccp(0, 0);
    p11Kumo2.anchorPoint = ccp(0.5f,0.5f);
    [self addChild:p11Kumo2 z:2];
    [self p11CloudCreate2];
}

-(void)p11CloudRemove_3{
    [p11Kumo3 removeAllChildrenWithCleanup:YES];
    p11Kumo3 = [CCLayer node];
    p11Kumo3.position = ccp(0, 0);
    p11Kumo3.anchorPoint = ccp(0.5f,0.5f);
    [self addChild:p11Kumo3 z:2];
    [self p11CloudCreate3];
}

-(void) dealloc
{
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    
	delete world;
	world = NULL;
	
	delete m_debugDraw;
	m_debugDraw = NULL;
	
	[super dealloc];
}

-(void) initPhysics
{
	
	CGSize s = [[CCDirector sharedDirector] winSize];
	
	b2Vec2 gravity;
	gravity.Set(0.0f, -2.0f);
    bool doSleep = false;
    
	world = new b2World(gravity,doSleep);
	
	
	// Do we want to let bodies sleep?
	
	world->SetContinuousPhysics(true);
    
    /*
     m_debugDraw = new GLESDebugDraw( PTM_RATIO );
     world->SetDebugDraw(m_debugDraw);
     uint32 flags = 0;
     flags += b2Draw::e_shapeBit;
     flags += b2Draw::e_jointBit;
     flags += b2Draw::e_aabbBit;
     flags += b2Draw::e_pairBit;
     flags += b2Draw::e_centerOfMassBit;
     flags += b2Draw::e_controllerBit;
     
     m_debugDraw->SetFlags(flags);
     */
    
	
	// Define the ground body.
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0, 0); // bottom-left corner
	
	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	groundBody = world->CreateBody(&groundBodyDef);
	
	// Define the ground box shape.
	b2EdgeShape groundBox;
	
	// bottom
	groundBox.Set(b2Vec2(0,-100/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,-100/PTM_RATIO));
	groundBody->CreateFixture(&groundBox,0);
	
	// top
	groundBox.Set(b2Vec2(0,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO));
	groundBody->CreateFixture(&groundBox,0);
	
	// left
	groundBox.Set(b2Vec2(-10,s.height/PTM_RATIO), b2Vec2(-10,0));
	groundBody->CreateFixture(&groundBox,0);
	
	// right
	groundBox.Set(b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,-100/PTM_RATIO));
	groundBody->CreateFixture(&groundBox,0);
    
    /*
    //船のポリゴンシェイプ
    b2BodyDef shipBody;
	shipBody.position.Set(0, 0);
	b2Body *body1 = world->CreateBody(&shipBody);
    [[GB2ShapeCache sharedShapeCache] addShapesWithFile:@"p4_ship_polygon.plist"];
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:body1 forShapeName:@"p4_ship"];
     */
}

-(void) draw
{
	//
	// IMPORTANT:
	// This is only for debug purposes
	// It is recommend to disable it
	//
	[super draw];
	
	ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
	
	kmGLPushMatrix();
	
	world->DrawDebugData();
	
	kmGLPopMatrix();
}
-(void) addFlowerSprite{
    
    float xpos = 22.0;
    float ypos = 7.0;
    float ypos2 = 7.0+0.3;
	
    
    b2Body* ground = NULL;
    {
        b2BodyDef bd;
        ground = world->CreateBody(&bd);
        
        b2EdgeShape shape;
        shape.Set(b2Vec2(-0.0f, 0.0f), b2Vec2(0.0, 0.0));
        ground->CreateFixture(&shape, 0.0f);
    }
    b2Body* ground2 = NULL;
    {
        b2BodyDef bd2;
        ground2 = world->CreateBody(&bd2);
        
        b2EdgeShape shape;
        shape.Set(b2Vec2(-0.0f, 0.0f), b2Vec2(0.0, 0.0));
        ground2->CreateFixture(&shape, 0.0f);
    }
    
    b2Body* ground3 = NULL;
    {
        b2BodyDef bd3;
        ground3 = world->CreateBody(&bd3);
        
        b2EdgeShape shape;
        shape.Set(b2Vec2(-0.0f, 0.0f), b2Vec2(0.0, 0.0));
        ground3->CreateFixture(&shape, 0.0f);
    }
    
    b2Body* ground4 = NULL;
    {
        b2BodyDef bd4;
        ground4 = world->CreateBody(&bd4);
        
        b2EdgeShape shape;
        shape.Set(b2Vec2(-0.0f, 0.0f), b2Vec2(0.0, 0.0));
        ground4->CreateFixture(&shape, 0.0f);
    }
    
    b2Body* ground5= NULL;
    {
        b2BodyDef bd5;
        ground5 = world->CreateBody(&bd5);
        
        b2EdgeShape shape;
        shape.Set(b2Vec2(-0.0f, 0.0f), b2Vec2(0.0, 0.0));
        ground5->CreateFixture(&shape, 0.0f);
    }
    
    b2Body* ground6 = NULL;
    {
        b2BodyDef bd6;
        ground6 = world->CreateBody(&bd6);
        
        b2EdgeShape shape;
        shape.Set(b2Vec2(-0.0f, 0.0f), b2Vec2(0.0, 0.0));
        ground6->CreateFixture(&shape, 0.0f);
    }
    
    b2PolygonShape shape;
    shape.SetAsBox(1.4f, 0.3f);
    
    b2PolygonShape shape1_1;
    shape1_1.SetAsBox(0.7f, 1.0f);
    
    b2PolygonShape shape1_2;
    shape1_2.SetAsBox(0.03f, 0.2f);
    
    b2PolygonShape shape2_1;
    shape2_1.SetAsBox(0.6f, 0.7f);
    
    b2PolygonShape shape2_2;
    shape2_2.SetAsBox(0.03f, 0.2f);
    
    b2PolygonShape shape3_1;
    shape3_1.SetAsBox(1.0f, 0.7f);
    
    b2PolygonShape shape3_2;
    shape3_2.SetAsBox(0.03f, 0.2f);
    
    b2PolygonShape shape4_1;
    shape4_1.SetAsBox(1.0f, 0.7f);
    
    b2PolygonShape shape4_2;
    shape4_2.SetAsBox(0.03f, 0.2f);
    
    
    b2PolygonShape shape2;
    shape2.SetAsBox(0.6f, 1.0f);
    
    b2PolygonShape shape3;
    shape3.SetAsBox(0.6f, 0.7f);
    
    b2FixtureDef fd;
    fd.shape = &shape1_1;
    fd.density =0.7f;

    fd.filter.maskBits = 0x0000;
    
    b2FixtureDef fd1_2;
    fd1_2.shape = &shape1_2;
    fd1_2.density = 30000.0f;

    fd1_2.filter.maskBits = 0x0000;
    
    b2FixtureDef fd2;
    fd2.shape = &shape2_1;
    fd2.density = 1.2f;
    fd2.friction = 0.1;
    fd2.restitution = 0.05;
    fd2.filter.maskBits = 0x0000;
    
    b2FixtureDef fd2_2;
    fd2_2.shape = &shape2_2;
    fd2_2.density =30000.0f;
    fd2_2.friction = 0.1;
    fd2_2.restitution = 0.05;
    fd2_2.filter.maskBits = 0x0000;
    
    b2FixtureDef fd3;
    fd3.shape = &shape3_1;
    fd3.density = 50.0f;
    fd3.friction = 1.0;
    fd3.restitution = 0.05;
    fd3.filter.maskBits = 0x0000;
    
    b2FixtureDef fd3_2;
    fd3_2.shape = &shape3_2;
    fd3_2.density =4000.0;
    fd3_2.friction = 2.0;
    fd3_2.restitution = 0.1;
    fd3_2.filter.maskBits = 0x0000;
    
    b2FixtureDef fd4;
    fd4.shape = &shape3_1;
    fd4.density = 8.0f;
    fd4.friction = 2.0;
    fd4.filter.maskBits = 0x0000;
    
    b2FixtureDef fd4_2;
    fd4_2.shape = &shape3_2;
    fd4_2.density =30000.0f;
    fd4_2.friction = 2.0;
    fd4_2.restitution = 0.1;
    fd4_2.filter.maskBits = 0x0000;
    
    
    b2FixtureDef fd5;
    fd5.shape = &shape4_1;
    fd5.density = 1.0f;
    fd5.friction = 2.0;
    fd5.filter.maskBits = 0x0000;
    
    b2FixtureDef fd5_2;
    fd5_2.shape = &shape4_2;
    fd5_2.density =30000.0f;
    fd5_2.friction = 2.0;
    fd5_2.restitution = 0.1;
    fd5_2.filter.maskBits = 0x0000;
    
    b2FixtureDef fd6;
    fd6.shape = &shape3_1;
    fd6.density = 1.0f;
    fd6.friction = 0.0;
    fd6.filter.maskBits = 0x0000;
    
    b2FixtureDef fd6_2;
    fd6_2.shape = &shape3_2;
    fd6_2.density =30000.0f;
    fd6_2.friction = 0.0;
    fd6_2.filter.maskBits = 0x0000;
    

    
    b2WeldJointDef jd;
    b2WeldJointDef jd2;
    b2WeldJointDef jd3;
    b2WeldJointDef jd4;
    b2WeldJointDef jd5;
    b2WeldJointDef jd6;

    
    b2Body* flowerBody = ground;
    b2Body* flowerBody2 = ground2;
    b2Body* flowerBody4 = ground4;
    b2Body* flowerBody5 = ground5;
    b2Body* flowerBody6 = ground6;
    
    //花１
    for (int32 i = 0; i < 10; ++i)
    {
        CCNode *parent = [self getChildByTag:kTagParentNode2];
        PhysicsSprite *sprite;
        b2BodyDef bd;
        b2Body* body;
        bd.type = b2_dynamicBody;
        //sprite.position = ccp(20.0f,11.0f+ 1.0f * i);
        
        if(i == 9){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(41,0,60,53)];
            [parent addChild:sprite];
            bd.position.Set(xpos,ypos2+0.5f+0.4f * i);
            body = world->CreateBody(&bd);
            body->CreateFixture(&fd);
            
            
        }else if(i == 0){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(-48,0,28,22)];
            [parent addChild:sprite];
            bd.position.Set(xpos,ypos2+ 0.4f * i);
            body = world->CreateBody(&bd);
            body->CreateFixture(&fd1_2);
            
        }else if(i == 1){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41,0,28,22)];
            [parent addChild:sprite];
            bd.position.Set(xpos,ypos2+ 0.4f * i);
            body = world->CreateBody(&bd);
            body->CreateFixture(&fd1_2);
            
            
        }else if(i == 3){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41,0,28,22)];
            [parent addChild:sprite];
            bd.position.Set(xpos,ypos2+ 0.4f * i);
            body = world->CreateBody(&bd);
            body->CreateFixture(&fd1_2);
            
        }else if(i == 5){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41+30,0,28,22)];
            [parent addChild:sprite];
            bd.position.Set(xpos,ypos2+ 0.4f * i);
            body = world->CreateBody(&bd);
            body->CreateFixture(&fd1_2);
            
        }else{
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41+30*2,0,28,22)];
            [parent addChild:sprite];
            bd.position.Set(xpos,ypos2+ 0.4f * i);
            body = world->CreateBody(&bd);
            body->CreateFixture(&fd1_2);
            
        }
        
        
        b2Vec2 anchor(xpos, ypos+ 0.4f * i);
        jd.Initialize(flowerBody, body, anchor);
        world->CreateJoint(&jd);
        [sprite setPhysicsBody:body];
        flowerBody = body;
        
        
    }
    
    //花2
    for (int32 i = 0; i < 9; ++i)
    {
        CCNode *parent = [self getChildByTag:kTagParentNode2];
        PhysicsSprite *sprite;
        b2BodyDef bd2;
        b2Body* body2;
        bd2.type = b2_dynamicBody;
        //sprite.position = ccp(20.0f,11.0f+ 1.0f * i);
        
        if(i == 8){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(41+62,0,60,27)];
            [parent addChild:sprite];
            bd2.position.Set(xpos+ 2.0f,ypos+0.2f+ 0.4f * i);
            body2 = world->CreateBody(&bd2);
            body2->CreateFixture(&fd2);
        }else if(i == 0){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(-48,0,46,36)];
            [parent addChild:sprite];
            bd2.position.Set(xpos+ 2.0f,ypos2+ 0.4f * i);
            body2 = world->CreateBody(&bd2);
            body2->CreateFixture(&fd2_2);
        }else if(i == 1){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41+30*1,0,28,22)];
            [parent addChild:sprite];
            bd2.position.Set(xpos+ 2.0f,ypos2+ 0.4f * i);
            body2 = world->CreateBody(&bd2);
            body2->CreateFixture(&fd2_2);
            
        }else if(i == 3){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41+30*1,0,28,22)];
            [parent addChild:sprite];
            bd2.position.Set(xpos+ 2.0f,ypos2+ 0.4f * i);
            body2 = world->CreateBody(&bd2);
            body2->CreateFixture(&fd2_2);
        }else if(i == 5){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41,0,28,22)];
            [parent addChild:sprite];
            bd2.position.Set(xpos+ 2.0f,ypos2+ 0.4f * i);
            body2 = world->CreateBody(&bd2);
            body2->CreateFixture(&fd2_2);
        }else{
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41+30*2,0,28,22)];
            [parent addChild:sprite];
            bd2.position.Set(xpos+ 2.0f,ypos2+ 0.4f * i);
            body2 = world->CreateBody(&bd2);
            body2->CreateFixture(&fd2_2);
        }
        
        b2Vec2 anchor2(xpos+ 2.0f, ypos+ 0.4f * i);
        jd2.Initialize(flowerBody2, body2, anchor2);
        world->CreateJoint(&jd2);
        [sprite setPhysicsBody:body2];
        flowerBody2 = body2;
        
    }
    
    //花4
    for (int32 i = 0; i < 5; ++i)
    {
        CCNode *parent = [self getChildByTag:kTagParentNode2];
        PhysicsSprite *sprite;
        b2BodyDef bd4;
        b2Body* body4;
        bd4.type = b2_dynamicBody;
        //sprite.position = ccp(20.0f,11.0f+ 1.0f * i);
        
        if(i == 4){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(0,0,17,24)];
            [parent addChild:sprite];
            bd4.position.Set(xpos+ 3.0f,ypos+0.7f+ 0.4f * i);
            body4 = world->CreateBody(&bd4);
            body4->CreateFixture(&fd4);
            
        }else if(i == 0){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(-48,0,46,36)];
            [parent addChild:sprite];
            bd4.position.Set(xpos+ 3.0f,ypos2+ 0.4f * i);
            body4 = world->CreateBody(&bd4);
            body4->CreateFixture(&fd4_2);
            
            
        }else if(i == 2){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41+30*1,0,28,22)];
            [parent addChild:sprite];
            bd4.position.Set(xpos+ 3.0f,ypos2+ 0.4f * i);
            body4 = world->CreateBody(&bd4);
            body4->CreateFixture(&fd4_2);
            
        }else{
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41+30*2,0,28,22)];
            [parent addChild:sprite];
            bd4.position.Set(xpos+ 3.0f,ypos2+ 0.4f * i);
            body4 = world->CreateBody(&bd4);
            body4->CreateFixture(&fd4_2);
            
        }
        
        b2Vec2 anchor4(xpos+ 3.0f, ypos+ 0.4f * i);
        jd4.Initialize(flowerBody4, body4, anchor4);
        world->CreateJoint(&jd4);
        [sprite setPhysicsBody:body4];
        flowerBody4 = body4;
        
    }
    
    //花5
    for (int32 i = 0; i < 10; ++i)
    {
        CCNode *parent = [self getChildByTag:kTagParentNode2];
        PhysicsSprite *sprite;
        b2BodyDef bd5;
        b2Body* body5;
        bd5.type = b2_dynamicBody;
        //sprite.position = ccp(20.0f,11.0f+ 1.0f * i);
        
        if(i == 9){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(41,0,60,53)];
            [parent addChild:sprite];
            bd5.position.Set(xpos+ 5.0f,ypos+1.0+ 0.4f * i);
            body5 = world->CreateBody(&bd5);
            body5->CreateFixture(&fd5);
        }else if(i == 0){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(-48,0,46,36)];
            [parent addChild:sprite];
            bd5.position.Set(xpos+ 5.0f,ypos2+ 0.4f * i);
            body5 = world->CreateBody(&bd5);
            body5->CreateFixture(&fd5_2);
        }else if(i == 1){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41,0,28,22)];
            [parent addChild:sprite];
            bd5.position.Set(xpos+ 5.0f,ypos2+ 0.4f * i);
            body5 = world->CreateBody(&bd5);
            body5->CreateFixture(&fd5_2);
        }else if(i == 3){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41,0,28,22)];
            [parent addChild:sprite];
            bd5.position.Set(xpos+ 5.0f,ypos2+ 0.4f * i);
            body5 = world->CreateBody(&bd5);
            body5->CreateFixture(&fd5_2);
        }else if(i == 5){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41+30*1,0,28,22)];
            [parent addChild:sprite];
            bd5.position.Set(xpos+ 5.0f,ypos2+ 0.4f * i);
            body5 = world->CreateBody(&bd5);
            body5->CreateFixture(&fd5_2);
        }else{
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41+30*2,0,28,22)];
            [parent addChild:sprite];
            bd5.position.Set(xpos+ 5.0f,ypos2+ 0.4f * i);
            body5 = world->CreateBody(&bd5);
            body5->CreateFixture(&fd5_2);
        }
        
        b2Vec2 anchor5(xpos+ 5.0f, ypos+ 0.4f * i);
        jd5.Initialize(flowerBody5, body5, anchor5);
        world->CreateJoint(&jd5);
        [sprite setPhysicsBody:body5];
        flowerBody5 = body5;
    }
    
    
    //花6
    for (int32 i = 0; i < 9; ++i)
    {
        CCNode *parent = [self getChildByTag:kTagParentNode2];
        PhysicsSprite *sprite;
        b2BodyDef bd6;
        b2Body* body6;
        bd6.type = b2_dynamicBody;
        
        if(i == 8){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(19,0,22,24)];
            [parent addChild:sprite];
            bd6.position.Set(xpos+ 7.0f,ypos+0.5f+ 0.4f * i);
            body6 = world->CreateBody(&bd6);
            body6->CreateFixture(&fd6);
            
            b2Vec2 anchor6(xpos+ 7.0f, ypos+0.5f+ 0.4f * i);
            jd6.Initialize(flowerBody6, body6, anchor6);
            world->CreateJoint(&jd6);
            [sprite setPhysicsBody:body6];
            flowerBody6 = body6;
            
        }else if(i == 0){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(-48,0,46,36)];
            [parent addChild:sprite];
            bd6.position.Set(xpos+ 7.0f,ypos2+ 0.4f * i);
            body6 = world->CreateBody(&bd6);
            body6->CreateFixture(&fd6_2);
            
            b2Vec2 anchor6(xpos+ 7.0f, ypos+ 0.4f * i);
            jd6.Initialize(flowerBody6, body6, anchor6);
            world->CreateJoint(&jd6);
            [sprite setPhysicsBody:body6];
            flowerBody6 = body6;
        }else if(i == 1){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41,0,28,22)];
            [parent addChild:sprite];
            bd6.position.Set(xpos+ 7.0f,ypos2+ 0.4f * i);
            body6 = world->CreateBody(&bd6);
            body6->CreateFixture(&fd6_2);
            
            b2Vec2 anchor6(xpos+ 7.0f, ypos+ 0.4f * i);
            jd6.Initialize(flowerBody6, body6, anchor6);
            world->CreateJoint(&jd6);
            [sprite setPhysicsBody:body6];
            flowerBody6 = body6;
        }else if(i == 3){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41,0,28,22)];
            [parent addChild:sprite];
            bd6.position.Set(xpos+ 7.0f,ypos2+ 0.4f * i);
            body6 = world->CreateBody(&bd6);
            body6->CreateFixture(&fd6_2);
            
            b2Vec2 anchor6(xpos+ 7.0f, ypos+ 0.4f * i);
            jd6.Initialize(flowerBody6, body6, anchor6);
            world->CreateJoint(&jd6);
            [sprite setPhysicsBody:body6];
            flowerBody6 = body6;
        }else if(i == 5){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41+30*1,0,28,22)];
            [parent addChild:sprite];
            bd6.position.Set(xpos+ 7.0f,ypos2+ 0.4f * i);
            body6 = world->CreateBody(&bd6);
            body6->CreateFixture(&fd6_2);
            
            b2Vec2 anchor6(xpos+ 7.0f, ypos+ 0.4f * i);
            jd6.Initialize(flowerBody6, body6, anchor6);
            world->CreateJoint(&jd6);
            [sprite setPhysicsBody:body6];
            flowerBody6 = body6;
        }else{
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(62+62+41+30*2,0,28,22)];
            [parent addChild:sprite];
            bd6.position.Set(xpos+ 7.0f,ypos2+ 0.4f * i);
            body6 = world->CreateBody(&bd6);
            body6->CreateFixture(&fd6_2);
            
            b2Vec2 anchor6(xpos+ 7.0f, ypos+ 0.4f * i);
            jd6.Initialize(flowerBody6, body6, anchor6);
            world->CreateJoint(&jd6);
            [sprite setPhysicsBody:body6];
            flowerBody6 = body6;
        }
        
        
    }
    
    
}
-(void) addNewSpriteAtPosition:(CGPoint)p
{
    
    b2BuoyancyControllerDef bcd;
    
    
	//CCLOG(@"Add sprite %0.2f x %02.f",p.x,p.y);
	CCNode *parent = [self getChildByTag:kTagParentNode];
	
	//We have a 64x64 sprite sheet with 4 different 32x32 images.  The following code is
	//just randomly picking one of the images
	int idx = (CCRANDOM_0_1()*13);
	int idy = (CCRANDOM_0_1()*12);
	PhysicsSprite *sprite = [PhysicsSprite spriteWithTexture:spriteTexture_ rect:CGRectMake(38 * idx,38 * idy,38,38)];
	[parent addChild:sprite];
	
	sprite.position = ccp( p.x, p.y);
	
	// Define the dynamic body.
	//Set up a 1m squared box in the physics world
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO);
    bodyDef.userData = sprite;
	b2Body *body = world->CreateBody(&bodyDef);
	
	// Define another box shape for our dynamic body.
	//b2PolygonShape dynamicBox;
    b2CircleShape circle;
    circle.m_radius = 20.0/PTM_RATIO;
	//dynamicBox.SetAsBox(.5f, .5f);//These are mid points for our 1m box
    
    
    
    b2PolygonShape shape;
    shape.SetAsBox(0.5f, 0.125f);
    
    b2FixtureDef fd;
    fd.shape = &shape;
    fd.density = 20.0f;
    
    b2WeldJointDef jd;
    
    
	
	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;
	fixtureDef.density = 1.0f; //密度
	fixtureDef.friction = 0.3f; //摩擦係数
    fixtureDef.restitution = 0.5f; //反発
	body->CreateFixture(&fixtureDef);
    
    bcd.normal.Set(0.08f, 0.3f);
    bcd.offset = ptm(BUOYANCYOFFSET);
    bcd.density = 2.0f; //密度
    bcd.linearDrag = 2.0f; //水中反発
    bcd.angularDrag = 1.0f; //オブジェクト回転時の抵抗
    bcd.useWorldGravity = false;
    bcd.gravity = b2Vec2(1.0f, -2.0f);
    bc = (b2BuoyancyController *)world->CreateController(&bcd);
    
    
    bc->AddBody(body);
	[sprite setPhysicsBody:body];
    
    
    
    
}

-(void) update: (ccTime) dt
{
	//It is recommended that a fixed time step is used with Box2D for stability
	//of the simulation, however, we are using a variable time step here.
	//You need to make an informed choice, the following URL is useful
	//http://gafferongames.com/game-physics/fix-your-timestep/
	
	int32 velocityIterations = 8;
	int32 positionIterations = 1;
    
	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	world->Step(dt, velocityIterations, positionIterations);
    
    for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
        if (b->GetUserData() != NULL) {
            CCSprite *sprite = (CCSprite*)b->GetUserData();
            sprite.position = CGPointMake( b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO);
            
            if(sprite.position.x/PTM_RATIO < -9.0){
                //bc->RemoveBody(b);
                [self removeChild:sprite cleanup:YES];
                world->DestroyBody(b);
                [self ballCriate];
            }
        }
    }
}
//消えたボールを生成する
- (void)ballCriate{
    [self addNewSpriteAtPosition:ccp(500+CCRANDOM_0_1()*500, CCRANDOM_0_1()*150)];
}

-(CGRect)spriteTouch:(CCSprite *) sprite{
    float h = [sprite contentSize].height;
    float w = [sprite contentSize].width;
    float x = sprite.position.x-w/2;
    float y = sprite.position.y-h/2;
    CGRect rect = CGRectMake(x, y, w, h);

    return rect;
}
-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touchs  = [touches anyObject];
    CGPoint location = [touchs locationInView:[touchs view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    CGRect obj2 = [self spriteTouch:funeButton];
    CGRect obj3 = [self spriteTouch:mount];
    
    if (!kagoHidden) {
        CGRect obj1 = [self spriteTouch:kago];
        if (CGRectContainsPoint(obj1,location)) {
            [self kagoTouch];
            [SoundEffect sePlay:@"ofn_se_p1_ohusuvoice.caf"];

        }
    }
    if (!funeHidden) {
        if (CGRectContainsPoint(obj2,location)) {
            [self funeTouch];
            [SoundEffect sePlay:@"ofn_se_p8_pon.mp3"];

        }
    }
    if (!hebiHidden) {
        if (CGRectContainsPoint(obj3,location)) {
            [SoundEffect sePlay:@"ofn_se_p11_snakemove.mp3"];
            [self yamaTouch];
        }
    }
    
    if (_mouseJoint != NULL) return;
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    for (b2Body* b = world->GetBodyList(); b; b = b->GetNext()){
        if (b->GetType() == b2_dynamicBody) {
            for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext()) {
                if (f->TestPoint(locationWorld)) {
                    b2MouseJointDef md;
                    md.bodyA = groundBody;
                    md.bodyB = b;
                    md.target = locationWorld;
                    md.collideConnected = true;
                    md.maxForce = 4000.0f * b->GetMass();
                    
                    _mouseJoint = (b2MouseJoint *)world->CreateJoint(&md);
                    b->SetAwake(true);
                }
            }
        }
    }
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint == NULL) return;
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    
    _mouseJoint->SetTarget(locationWorld);
    
}

-(void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint) {
        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
    
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_mouseJoint) {
        [SoundEffect sePlay:@"ofn_se_shirahige.mp3"];
        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
}

@end
