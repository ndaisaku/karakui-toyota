//
//  p6.h
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SoundEffect.h"

@interface p6 : CCLayer {
    CCSpriteFrameCache *p6framechash;
    CCSpriteBatchNode *p6batchNode;
    
    
    CCSprite *p6Murabito1;
    CCSprite *p6Murabito2;
    CCSprite *p6Murabito3;
    CCSprite *p6Murabito4;
    CCSprite *p6Murabito5;
    CCSprite *p6Murabito6;
    
    CCSprite *p6Murabito1_te;
    CCSprite *p6Murabito2_te;
    CCSprite *p6Murabito3_te;
    CCSprite *p6Murabito4_te;
    CCSprite *p6Murabito5_te;
    CCSprite *p6Murabito6_te;
    CCSprite *p6Mountain;
    
    bool p6MurabitoTouch1;
    bool p6MurabitoTouch2;
    bool p6MurabitoTouch3;
    bool p6MurabitoTouch4;
    bool p6MurabitoTouch5;
    bool p6MurabitoTouch6;
    bool p6MurabitoTouch_all;
    
    CCLayer *p6MurabitoNode;

    
    CCLayer *murabitoNoboruLayer;
}

@end
