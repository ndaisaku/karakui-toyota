//
//  p7.h
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface p7 : CCLayer {
    CCLayer *p7Kumo1;
    CCLayer *p7Kumo2;
    CCLayer *p7Kumo3;
    CCSprite *p7Mountain;
    int kumoLayerNum;
    CCLayer *murabitoNoboruLayer;
    bool p7Kakudai;    
    
    CCSprite *murabitoMoveFoot2;
    CCSprite *murabitoMoveFoot3;
    CCSprite *murabitoMoveFoot4;
}
@end
