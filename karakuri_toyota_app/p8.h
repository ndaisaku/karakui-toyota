//
//  p8.h
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 13/02/05.
//  Copyright __MyCompanyName__ 2013年. All rights reserved.
//


#import <GameKit/GameKit.h>

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"
#import "SoundEffect.h"

//Pixel to metres ratio. Box2D uses metres as the unit for measurement.
//This ratio defines how many pixels correspond to 1 Box2D "metre"
//Box2D is optimized for objects of 1x1 metre therefore it makes sense
//to define the ratio so that your most common object type is 1x1 metre.
#define PTM_RATIO 32

// HelloWorldLayer
@interface p8 : CCLayer
{
	CCTexture2D *spriteTexture_;	// weak ref
	CCTexture2D *spriteTexture2_;	// weak ref
	b2World* world;					// strong ref
	GLESDebugDraw *m_debugDraw;		// strong ref
    b2BodyDef bodyDef;
    b2BodyDef bodyDef2;
    b2MouseJoint *_mouseJoint;
    b2Body* groundBody;
    CCSprite  *gearSprite;
    CCSprite  *gearSpriteCenter;
    b2RevoluteJoint* m_joint1;
	b2RevoluteJoint* m_joint2;
	b2RevoluteJoint* m_joint3;
	b2RevoluteJoint* m_joint6;
	b2GearJoint* m_joint4;
	b2GearJoint* m_joint5;
    b2RevoluteJointDef jd1;
    
    CCSpriteFrameCache *cloudFrameCash;
    CCSpriteBatchNode *cloudBatch;
    
    CCSprite *p8mountain;
    CCSprite *p8mountain2;
    CCSprite *p8mountain3;
    CCSprite *p8mountain4;
    CCSprite *p8mountain5;
    CCSprite *p8mountain6;
    
    
    
    b2Body* body1;
    b2Body* body2;
    bool boolAmagumi1,boolAmagumi2,boolAmagumi3,boolAmagumi4,contBool;
    int routNo;
    int routCount;
    
    int red;
    int green;
    int blue;

}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

@end
