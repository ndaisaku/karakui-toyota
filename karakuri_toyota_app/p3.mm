//
//  p3.mm
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "p3.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#import "PhysicsSprite.h"
#import "Box2DExamples.h"
#import "GB2ShapeCache.h"

enum {
	kTagParentNode = 1,
};

#define BUOYANCYOFFSET 90.0f


@interface p3()
-(void) initPhysics;
-(void) addNewSpriteAtPosition:(CGPoint)p;
@end

@implementation p3

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	p3 *layer = [p3 node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init{
    if( (self=[super init])) {
        self.ignoreAnchorPointForPosition = NO;
        self.isTouchEnabled = YES;
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"fune.plist"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"fune.pvr.gz"];
        [self addChild: spriteSheet];
        
        fune1 = [CCSprite spriteWithSpriteFrameName:@"fune1.png"];
        fune1.position = ccp(553,374);
        [self addChild: fune1 z:0];
        
        fune2  = [CCSprite spriteWithSpriteFrameName:@"fune2.png"];
        fune2.position = ccp(280,334);
        [self addChild: fune2 z:0];
        
        fune3  = [CCSprite spriteWithSpriteFrameName:@"fune3.png"];
        fune3.position = ccp(100,325);
        [self addChild: fune3 z:0];
        [self shipMove];
        
        // init physics
		[self initPhysics];
        
        
        
		
		//Set up sprite
		
#if 1
		// Use batch node. Faster
		CCSpriteBatchNode *parent = [CCSpriteBatchNode batchNodeWithFile:@"blocks2.png" capacity:100];
		spriteTexture_ = [parent texture];
#else
		// doesn't use batch node. Slower
		spriteTexture_ = [[CCTextureCache sharedTextureCache] addImage:@"blocks2.png"];
		CCNode *parent = [CCNode node];
#endif
		[self addChild:parent z:0 tag:kTagParentNode];
		
		for (int i=0; i<120; i++) {
            [self addNewSpriteAtPosition:ccp(CCRANDOM_0_1()*1000, CCRANDOM_0_1()*150)];
            
        }
        
		
		[self scheduleUpdate];

    }
    return self;
}

//船の上下運動
-(void)shipMove{
    id move1 = [CCMoveTo actionWithDuration:3.0 position:ccp(553,344)];
    id move2 = [CCMoveTo actionWithDuration:3.0 position:ccp(553,374)];
    id ease = [CCEaseSineOut actionWithAction:move1 ];
    id ease2 = [CCEaseSineInOut actionWithAction:move2 ];
    
    id move3 = [CCMoveTo actionWithDuration:3.0 position:ccp(280,324)];
    id move4 = [CCMoveTo actionWithDuration:3.0 position:ccp(280,334)];
    
    id move5 = [CCMoveTo actionWithDuration:3.0 position:ccp(100,315)];
    id move6 = [CCMoveTo actionWithDuration:3.0 position:ccp(100,325)];
    
    id seq = [CCSequence actions:ease, ease2,[CCDelayTime actionWithDuration:0.2f], nil];
    id seq2 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0f],move3, move4,[CCDelayTime actionWithDuration:0.2f], nil];
    id seq3 = [CCSequence actions:[CCDelayTime actionWithDuration:2.0f],move5, move6,[CCDelayTime actionWithDuration:0.2f], nil];
    
    id repeat = [CCRepeatForever actionWithAction:seq];
    id repeat2 = [CCRepeatForever actionWithAction:seq2];
    id repeat3 = [CCRepeatForever actionWithAction:seq3];
    
    [fune1 runAction:repeat];
    [fune2 runAction:repeat2];
    [fune3 runAction:repeat3];
}


-(void) dealloc
{
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    
	delete world;
	world = NULL;
	
	delete m_debugDraw;
	m_debugDraw = NULL;
	
	[super dealloc];
}

-(void) initPhysics
{
	
	CGSize s = [[CCDirector sharedDirector] winSize];
	
	b2Vec2 gravity;
	gravity.Set(0.0f, -2.0f);
    bool doSleep = false;
    
	world = new b2World(gravity,doSleep);
	
	
	// Do we want to let bodies sleep?
	
	world->SetContinuousPhysics(true);
	
	m_debugDraw = new GLESDebugDraw( PTM_RATIO );
	world->SetDebugDraw(m_debugDraw);
    /*
     uint32 flags = 0;
     flags += b2Draw::e_shapeBit;
     flags += b2Draw::e_jointBit;
     flags += b2Draw::e_aabbBit;
     flags += b2Draw::e_pairBit;
     flags += b2Draw::e_centerOfMassBit;
     flags += b2Draw::e_controllerBit;
     
     m_debugDraw->SetFlags(flags);
     */
    
	
	// Define the ground body.
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0, 0); // bottom-left corner
	
	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	groundBody = world->CreateBody(&groundBodyDef);
	
	// Define the ground box shape.
	b2EdgeShape groundBox;
	
	// bottom
	
	groundBox.Set(b2Vec2(0,-100/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,-100/PTM_RATIO));
	groundBody->CreateFixture(&groundBox,0);
	
	// top
	groundBox.Set(b2Vec2(0,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO));
	groundBody->CreateFixture(&groundBox,0);
	
	// left
	groundBox.Set(b2Vec2(-10,s.height/PTM_RATIO), b2Vec2(-10,0));
	groundBody->CreateFixture(&groundBox,0);
	
	// right
	groundBox.Set(b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,0));
	groundBody->CreateFixture(&groundBox,0);
    

    
    
}

-(void) draw
{
	//
	// IMPORTANT:
	// This is only for debug purposes
	// It is recommend to disable it
	//
	[super draw];
	
	ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
	
	kmGLPushMatrix();
	
	world->DrawDebugData();
	
	kmGLPopMatrix();
}

-(void) addNewSpriteAtPosition:(CGPoint)p
{
    
    b2BuoyancyControllerDef bcd;
    
    
	//CCLOG(@"Add sprite %0.2f x %02.f",p.x,p.y);
	CCNode *parent = [self getChildByTag:kTagParentNode];
	
	//We have a 64x64 sprite sheet with 4 different 32x32 images.  The following code is
	//just randomly picking one of the images
	int idx = (CCRANDOM_0_1()*13);
	int idy = (CCRANDOM_0_1()*12);
	PhysicsSprite *sprite = [PhysicsSprite spriteWithTexture:spriteTexture_ rect:CGRectMake(38 * idx,38 * idy,38,38)];
	[parent addChild:sprite];
	
	sprite.position = ccp( p.x, p.y);
	
	// Define the dynamic body.
	//Set up a 1m squared box in the physics world
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO);
    bodyDef.userData = sprite;
	b2Body *body = world->CreateBody(&bodyDef);
	
	// Define another box shape for our dynamic body.
	//b2PolygonShape dynamicBox;
    b2CircleShape circle;
    circle.m_radius = 20.0/PTM_RATIO;
	//dynamicBox.SetAsBox(.5f, .5f);//These are mid points for our 1m box
    
    
    
    
    
	
	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;
	fixtureDef.density = 1.0f; //密度
	fixtureDef.friction = 0.3f; //摩擦係数
    fixtureDef.restitution = 0.5f; //反発
	body->CreateFixture(&fixtureDef);
    
    bcd.normal.Set(0.0f, 0.75f);
    bcd.offset = ptm(BUOYANCYOFFSET);
    bcd.density = 4.0f; //密度
    bcd.linearDrag = 1.0f; //水中反発
    bcd.angularDrag = 1.0f; //オブジェクト回転時の抵抗
    bcd.useWorldGravity = false;
    bcd.gravity = b2Vec2(0.5f, -2.0f);
    bc = (b2BuoyancyController *)world->CreateController(&bcd);
    
    
    bc->AddBody(body);
	[sprite setPhysicsBody:body];
    
    
}

-(void) update: (ccTime) dt
{
	//It is recommended that a fixed time step is used with Box2D for stability
	//of the simulation, however, we are using a variable time step here.
	//You need to make an informed choice, the following URL is useful
	//http://gafferongames.com/game-physics/fix-your-timestep/
	
	int32 velocityIterations = 8;
	int32 positionIterations = 1;
    
	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	world->Step(dt, velocityIterations, positionIterations);
    
    for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
        if (b->GetUserData() != NULL) {
            CCSprite *sprite = (CCSprite*)b->GetUserData();
            sprite.position = CGPointMake( b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO);
            
            if(sprite.position.x/PTM_RATIO < -9.0){
                //bc->RemoveBody(b);
                [self removeChild:sprite cleanup:YES];
                world->DestroyBody(b);
                [self ballCriate];
            }
        }
        
        
    }
    
}
//消えたボールを生成する
- (void)ballCriate{
    [self addNewSpriteAtPosition:ccp(500+CCRANDOM_0_1()*500, CCRANDOM_0_1()*150)];
}
- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	//Add a new body/atlas sprite at the touched location
    
    if (_mouseJoint != NULL) return;
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    for (b2Body* b = world->GetBodyList(); b; b = b->GetNext()){
        if (b->GetType() == b2_dynamicBody) {
            for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext()) {
                if (f->TestPoint(locationWorld)) {
                    b2MouseJointDef md;
                    md.bodyA = groundBody;
                    md.bodyB = b;
                    md.target = locationWorld;
                    md.collideConnected = true;
                    md.maxForce = 2000.0f * b->GetMass();
                    
                    _mouseJoint = (b2MouseJoint *)world->CreateJoint(&md);
                    b->SetAwake(true);
                }
            }
        }
    }
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint == NULL) return;
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    
    _mouseJoint->SetTarget(locationWorld);
    
}

-(void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint) {
        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
    
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_mouseJoint) {
        [SoundEffect sePlay:@"ofn_se_shirahige.mp3"];

        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
}


/*
#pragma mark GameKit delegate

-(void) achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

-(void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}
*/


@end
