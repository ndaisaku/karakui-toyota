//
//  p10.m
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "p10.h"
typedef enum nodeTags
{
    kTagMurabito,
} Tags;

@implementation p10
-(id)init{
    if(self = [super init]){
        self.ignoreAnchorPointForPosition = NO;
        self.isTouchEnabled = YES;
        
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        
        CCSpriteFrameCache *frameChash = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameChash addSpriteFramesWithFile:@"p10Resource.plist"];
        
        CCSpriteBatchNode *frameBach = [CCSpriteBatchNode batchNodeWithFile:@"p10Resource.pvr.gz"];
        [self addChild:frameBach];
        
        CCSprite *p10fune = [CCSprite spriteWithSpriteFrameName:@"p10_fune.png"];
        p10fune.position = ccp(p10fune.contentSize.width/2+80,768-p10fune.contentSize.height/2-100);
        p10fune.opacity = 0.0;
        [self addChild:p10fune];
        
        CCSprite *p10murabito = [CCSprite spriteWithSpriteFrameName:@"p10murabito.png"];
        p10murabito.position = ccp(winSize.width-p10murabito.contentSize.width/2,winSize.height-375);
        [self addChild:p10murabito z:2 tag:kTagMurabito];
        
        //オオスノ現れる
        id Fade =[CCFadeTo actionWithDuration:5.0f opacity:180];
        id seq = [CCSequence actions:[CCDelayTime actionWithDuration:2.0f],Fade, nil];
        [p10fune runAction:seq];
    }
    return self;
}
-(CGRect)spriteTouch:(CCSprite *) sprite{
    float h = [sprite contentSize].height;
    float w = [sprite contentSize].width;
    float x = sprite.position.x-w/2;
    float y = sprite.position.y-h/2;
    CGRect rect = CGRectMake(x, y, w, h);
    
    return rect;
}

#pragma mark tochEvent
-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch  = [touches anyObject];
    CGPoint location = [touch locationInView:[touch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    CCSprite *sprite = (CCSprite *)[self getChildByTag:kTagMurabito];
    
    CGRect obj01Rect = [self spriteTouch:sprite];
    if(CGRectContainsPoint(obj01Rect, location)){
        [SoundEffect sePlay:@"ofn_se_p10_te.mp3"];
    }
}

-(void) dealloc
{
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [super dealloc];
}

@end
