//
//  p9.m
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "p9.h"


@implementation p9
-(id)init{
    if(self = [super init]){
        self.ignoreAnchorPointForPosition = NO;
        self.isTouchEnabled = YES;
        
        [self preloadParticleEffect:@"p9Particle.plist"];


        CGSize winsize = [[CCDirector sharedDirector] winSize];
        CCSpriteFrameCache *frameChash = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameChash addSpriteFramesWithFile:@"p9Resource.plist"];
        
        CCSpriteBatchNode *frameBach = [CCSpriteBatchNode batchNodeWithFile:@"p9Resource.pvr.gz"];
        [self addChild:frameBach];
        
        CCSprite *p9Amagumo = [CCSprite spriteWithSpriteFrameName:@"p9_amagumo.png"];
        p9Amagumo.position = ccp(p9Amagumo.contentSize.width/2+10,winsize.height-p9Amagumo.contentSize.height/2-10);
        [self addChild:p9Amagumo];
        
        p9kuma[0] = [CCSprite spriteWithSpriteFrameName:@"p9_kuma1.png"];
        p9kuma[0].position = ccp(886,320);
        [self addChild:p9kuma[0]];
        
        p9hituji[0] = [CCSprite spriteWithSpriteFrameName:@"p9_hituji1.png"];
        p9hituji[0].position = ccp(990,335);
        [self addChild:p9hituji[0]];
        
        p9nezumi[0] = [CCSprite spriteWithSpriteFrameName:@"p9_nezumi1.png"];
        p9nezumi[0].position = ccp(835,302);
        [self addChild:p9nezumi[0]];
        
        p9uma[0] = [CCSprite spriteWithSpriteFrameName:@"p9_uma1.png"];
        p9uma[0].position = ccp(780,348);
        [self addChild:p9uma[0]];
        
        p9tori[0] = [CCSprite spriteWithSpriteFrameName:@"p9_tori1.png"];
        p9tori[0].position = ccp(945,325);
        [self addChild:p9tori[0]];
        
        p9iwa =  [CCSprite spriteWithSpriteFrameName:@"p9_iwa1.png"];
        p9iwa.position = ccp(210, 530);
        [self addChild:p9iwa];
        
        
        CCSprite *p9yama =  [CCSprite spriteWithSpriteFrameName:@"p9_yama.png"];
        p9yama.position = ccp(510, 385);
        [self addChild:p9yama];
        for (int i = 0; i < 30; i++) {
            [self criateAme];
        }

        
    }
    return self;
}

#pragma mark Ame


-(void)criateAme{
    int ameNo = CCRANDOM_0_1()*32+1;
    int ame_x = CCRANDOM_0_1()*450+30;
    if (!p9_fall_iwa) {
        ameSpeed = CCRANDOM_0_1()+1;
    }else{
        ameSpeed = CCRANDOM_0_1()+5;
    }
    

    
    NSString *ameSpriteName = [NSString stringWithFormat:@"p9_ame%d.png",ameNo];
    CCSprite *p9ame_2 = [CCSprite spriteWithSpriteFrameName:ameSpriteName];
    p9ame_2.position = ccp(ame_x, 600);
    [self addChild:p9ame_2];
    
    id Moveto = [CCMoveTo actionWithDuration:ameSpeed position:ccp(ame_x+240,-50)];
    id ease = [CCEaseSineIn actionWithAction:Moveto];
    id func = [CCCallBlock actionWithBlock:^{
        [self removeChild:p9ame_2 cleanup:YES];
        
        [self criateAme];
    }];
    id Seq = [CCSequence actions:ease,func,nil];
    
    [p9ame_2 runAction:Seq];
}

-(void)iwaMove{
    
    if (p9iwaCount <= 2) {
        [SoundEffect sePlay:@"ofn_se_p9_rock1.mp3"];
        
        id rote = [CCRotateTo actionWithDuration:0.03 angle:20];
        id rote2 = [CCRotateTo actionWithDuration:0.03 angle:0];
        id moveBy = [CCMoveBy actionWithDuration:0.05 position:ccp(3.5,1)];
        id seq = [CCSequence actions:rote,rote2,moveBy,nil];
        id rep = [CCRepeat actionWithAction:seq times:4];
        [p9iwa runAction:rep];
        p9iwaCount += 1;
    }else if (p9iwaCount == 3) {
        p9iwaCount += 1;
        p9_fall_iwa = true;
        CCSprite *iwa2 = [CCSprite spriteWithSpriteFrameName:@"p9_iwa2.png"];
        iwa2.position = ccp(400, 500);
        iwa2.opacity = 0;
        [self addChild:iwa2];
        
        CCSprite *iwa3 = [CCSprite spriteWithSpriteFrameName:@"p9_iwa3.png"];
        iwa3.position = ccp(650, 350);
        iwa3.opacity = 0;
        [self addChild:iwa3];
        
        CCSprite *iwa4 = [CCSprite spriteWithSpriteFrameName:@"p9_iwa4.png"];
        iwa4.position = ccp(900, 150);
        iwa4.rotation = -20;
        iwa4.opacity = 0;

        [self addChild:iwa4];
        
        
        id Fade = [CCFadeOut actionWithDuration:0.5];
        [p9iwa runAction:Fade];
        
        id FadeIn1 = [CCFadeIn actionWithDuration:0.4];
        id Move1 =  [CCMoveBy actionWithDuration:1.5 position:ccp(20,-30)];
        id Rote1 =  [CCRotateBy actionWithDuration:1.5 angle:10];
        id MoveEase1 = [CCEaseSineOut actionWithAction:Move1];
        id RoteEase1 = [CCEaseSineOut actionWithAction:Rote1];
        id FadeOut1 =  [CCFadeOut actionWithDuration:0.4];
        id FadeSeq1 =   [CCSequence actions:[CCDelayTime actionWithDuration:1.1],FadeOut1,nil];
        id Spawn1 = [CCSpawn actions:FadeIn1,MoveEase1,RoteEase1,FadeSeq1,nil];
        id Seq1 =   [CCSequence actions:Spawn1,nil];
        [iwa2 runAction:Seq1];
        
        id FadeIn2 = [CCFadeIn actionWithDuration:0.4];
        id Move2 =  [CCMoveBy actionWithDuration:1.5 position:ccp(20,-30)];
        id Rote2 =  [CCRotateBy actionWithDuration:1.5 angle:10];
        id MoveEase2 = [CCEaseSineOut actionWithAction:Move2];
        id RoteEase2 = [CCEaseSineOut actionWithAction:Rote2];
        id FadeOut2 =  [CCFadeOut actionWithDuration:0.4];
        id FadeSeq2 =   [CCSequence actions:[CCDelayTime actionWithDuration:1.1],FadeOut2,nil];
        id Spawn2 = [CCSpawn actions:FadeIn2,MoveEase2,RoteEase2,FadeSeq2,nil];
        id Seq2 =   [CCSequence actions:[CCDelayTime actionWithDuration:1.0],Spawn2,nil];
        [iwa3 runAction:Seq2];
        
        id FadeIn3 = [CCFadeIn actionWithDuration:0.4];
        id Move3 =  [CCMoveBy actionWithDuration:1.2 position:ccp(20,-50)];
        id Rote3 =  [CCRotateBy actionWithDuration:1.2 angle:20];
        id MoveEase3 = [CCEaseSineInOut actionWithAction:Move3];
        id RoteEase3 = [CCEaseSineInOut actionWithAction:Rote3];
        id Spawn3 = [CCSpawn actions:FadeIn3,MoveEase3,RoteEase3,nil];
        id func = [CCCallBlock actionWithBlock:^{
            [SoundEffect sePlay:@"ofn_se_p9_rock1.mp3"];

            CCParticleSystem *particle;
            particle = [CCParticleSystemQuad particleWithFile:@"p9Particle.plist"];
            [self addChild:particle z:9];
            p9_fall_iwa = false;
            
            
            //村人の顔を替える
            [self removeChild:p9kuma[0] cleanup:YES];
            [self removeChild:p9nezumi[0] cleanup:YES];
            [self removeChild:p9hituji[0] cleanup:YES];
            [self removeChild:p9uma[0] cleanup:YES];
            [self removeChild:p9tori[0] cleanup:YES];
            
            p9kuma[1] = [CCSprite spriteWithSpriteFrameName:@"p9_kuma2.png"];
            p9kuma[1].position = ccp(886,320);
            [self addChild:p9kuma[1]];
            
            p9hituji[1] = [CCSprite spriteWithSpriteFrameName:@"p9_hituji2.png"];
            p9hituji[1].position = ccp(990,335);
            [self addChild:p9hituji[1]];
            
            p9nezumi[1] = [CCSprite spriteWithSpriteFrameName:@"p9_nezumi2.png"];
            p9nezumi[1].position = ccp(835,302);
            [self addChild:p9nezumi[1]];
            
            p9uma[1] = [CCSprite spriteWithSpriteFrameName:@"p9_uma2.png"];
            p9uma[1].position = ccp(780,348);
            [self addChild:p9uma[1]];
            
            p9tori[1] = [CCSprite spriteWithSpriteFrameName:@"p9_tori2.png"];
            p9tori[1].position = ccp(945,325);
            [self addChild:p9tori[1]];
            

            
        }];
        id Seq3 =   [CCSequence actions:[CCDelayTime actionWithDuration:1.9],Spawn3,func,nil];
        [iwa4 runAction:Seq3];
        
        //[p9iwa removeAllChildrenWithCleanup:YES];
    }
    
    
}

-(void)preloadParticleEffect:(NSString *)particleFile{
    [CCParticleExplosion particleWithFile:particleFile];
}

-(CGRect)spriteTouch:(CCSprite *) sprite{
    float h = [sprite contentSize].height;
    float w = [sprite contentSize].width;
    float x = sprite.position.x-w/2;
    float y = sprite.position.y-h/2;
    CGRect rect = CGRectMake(x, y, w, h);
    
    return rect;
}

-(void) update: (ccTime) dt
{

}

#pragma mark tochEvent
-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch  = [touches anyObject];
    CGPoint location = [touch locationInView:[touch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    
        CGRect obj01Rect = [self spriteTouch:p9iwa];
        if(CGRectContainsPoint(obj01Rect, location) && p9iwaCount <= 3){
            [self iwaMove];
        }
}

-(void) dealloc
{
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [super dealloc];
}

@end
