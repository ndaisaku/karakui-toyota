//
//  p7.m
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "p7.h"


@implementation p7

-(id)init{
    if(self = [super init]){
        
        self.ignoreAnchorPointForPosition = NO;

        CGSize winSize = [[CCDirector sharedDirector] winSize];

        CCSpriteFrameCache *p7framechash = [CCSpriteFrameCache sharedSpriteFrameCache];
        [p7framechash addSpriteFramesWithFile:@"p7Resource.plist"];
        
        CCSpriteBatchNode *p7batchNode = [CCSpriteBatchNode batchNodeWithFile:@"p7Resource.pvr.gz"];
        [self addChild:p7batchNode];
        
        
        //雲レイヤーの追加
        p7Kumo1 = [CCLayer node];
        p7Kumo1.anchorPoint = ccp(0.5f,1.0f);
        p7Kumo1.position = ccp(1100, 0);
        [self addChild:p7Kumo1 z:2];
        
        p7Kumo2 = [CCLayer node];
        p7Kumo2.anchorPoint = ccp(0.5f,1.0f);
        p7Kumo2.position = ccp(1100, 0);
        
        [self addChild:p7Kumo2 z:2];
        
        p7Kumo3 = [CCLayer node];
        p7Kumo3.anchorPoint = ccp(0.5f,1.0f);
        p7Kumo3.position = ccp(1100, 0);
        
        [self addChild:p7Kumo3 z:2];
        
        p7Mountain  = [CCSprite spriteWithSpriteFrameName:@"p7_mountain.png"];
        p7Mountain.position = ccp(winSize.width/2,winSize.height/2+100);
        p7Mountain.anchorPoint = ccp(0.5,0.8);
        [self addChild:p7Mountain z:0];
        
        [self p7CloudCreat];
        [self p7_walk];
        
    }
    return self;
}


//雲の生成
-(void)p7CloudCreat{
    
    //雲の高さを300〜480にランダムにする
    for (int j=1; j<=3; j++) {
        int ranPosyLayer = CCRANDOM_0_1()*180 + 300;
        int ranPosy;
        int ranPosx;
        
        for (int i=1; i<=7; i++) {
            int imageNo = CCRANDOM_0_1()*6+1;
            
            
            NSString *imagePath = [NSString stringWithFormat:@"p7_kumo_%d.png",imageNo];
            CCSprite *spriteCloud = [CCSprite spriteWithSpriteFrameName:imagePath];
            
            
            spriteCloud.scale = 0.66;
            ranPosy = CCRANDOM_0_1()*100+ranPosyLayer;
            ranPosx = CCRANDOM_0_1()*380;
            
            
            spriteCloud.position = ccp(ranPosx,ranPosy);
            
            switch (j) {
                case 1:
                    p7Kumo1.position = ccp(200,0);
                    [p7Kumo1 addChild:spriteCloud];
                    break;
                case 2:
                    p7Kumo2.position = ccp(600,0);
                    [p7Kumo2 addChild:spriteCloud];
                    break;
                case 3:
                    p7Kumo3.position = ccp(900,0);
                    [p7Kumo3 addChild:spriteCloud];
                    break;
                default:
                    break;
            }
        }
        
    }
    [self p7CloudMove];
}

-(void)p7CloudCreat2{
    
    //雲の高さを300〜480にランダムにする
    for (int j=1; j<=3; j++) {
        int ranPosyLayer = CCRANDOM_0_1()*180 + 10;
        int ranPosy;
        int ranPosx;
        
        for (int i=1; i<=7; i++) {
            int imageNo = CCRANDOM_0_1()*6+1;
            
            
            NSString *imagePath = [NSString stringWithFormat:@"p7_kumo_%d.png",imageNo];
            CCSprite *spriteCloud = [CCSprite spriteWithSpriteFrameName:imagePath];
            
            ranPosy = CCRANDOM_0_1()*100+ranPosyLayer;
            ranPosx = CCRANDOM_0_1()*380;
            
            spriteCloud.position = ccp(ranPosx,ranPosy);
            
            switch (j) {
                case 1:
                    [p7Kumo1 addChild:spriteCloud];
                    break;
                case 2:
                    [p7Kumo2 addChild:spriteCloud];
                    break;
                case 3:
                    [p7Kumo3 addChild:spriteCloud];
                    break;
                    
                default:
                    break;
            }
        }
    }
    [self p7CloudMove2];
}


-(void)p7CloudCreat2_1{
    
    //雲の高さを300〜480にランダムにする
    int ranPosyLayer = CCRANDOM_0_1()*180 + 10;
    int ranPosy;
    int ranPosx;
    
    for (int i=1; i<=7; i++) {
        int imageNo = CCRANDOM_0_1()*6+1;
        
        
        NSString *imagePath = [NSString stringWithFormat:@"p7_kumo_%d.png",imageNo];
        CCSprite *spriteCloud = [CCSprite spriteWithSpriteFrameName:imagePath];
        
        ranPosy = CCRANDOM_0_1()*100+ranPosyLayer;
        ranPosx = CCRANDOM_0_1()*380;
        
        spriteCloud.position = ccp(ranPosx,ranPosy);
        [p7Kumo1 addChild:spriteCloud];
    }
    [self p7CloudMove2_1];
}

-(void)p7CloudCreat2_2{
    
    //雲の高さを300〜480にランダムにする
    int ranPosyLayer = CCRANDOM_0_1()*180 + 10;
    int ranPosy;
    int ranPosx;
    
    for (int i=1; i<=7; i++) {
        int imageNo = CCRANDOM_0_1()*6+1;
        
        
        NSString *imagePath = [NSString stringWithFormat:@"p7_kumo_%d.png",imageNo];
        CCSprite *spriteCloud = [CCSprite spriteWithSpriteFrameName:imagePath];
        
        ranPosy = CCRANDOM_0_1()*100+ranPosyLayer;
        ranPosx = CCRANDOM_0_1()*380;
        
        spriteCloud.position = ccp(ranPosx,ranPosy);
        [p7Kumo2 addChild:spriteCloud];
    }
    [self p7CloudMove2_2];
}


-(void)p7CloudCreat2_3{
    
    //雲の高さを300〜480にランダムにする
    int ranPosyLayer = CCRANDOM_0_1()*180 + 10;
    int ranPosy;
    int ranPosx;
    
    for (int i=1; i<=7; i++) {
        int imageNo = CCRANDOM_0_1()*6+1;
        
        
        NSString *imagePath = [NSString stringWithFormat:@"p7_kumo_%d.png",imageNo];
        CCSprite *spriteCloud = [CCSprite spriteWithSpriteFrameName:imagePath];
        
        ranPosy = CCRANDOM_0_1()*100+ranPosyLayer;
        ranPosx = CCRANDOM_0_1()*380;
        
        spriteCloud.position = ccp(ranPosx,ranPosy);
        [p7Kumo3 addChild:spriteCloud];
    }
    [self p7CloudMove2_3];
}


//雲の移動
-(void)p7CloudMove{
    
    //雲のスピードをランダムにする
    int randCloudSpeed = CCRANDOM_0_1()*10+30;
    
    id move = [CCMoveBy actionWithDuration:randCloudSpeed position:ccp(-300,0)];
    id move2 = [CCMoveBy actionWithDuration:randCloudSpeed position:ccp(-300,0)];
    id move3 = [CCMoveBy actionWithDuration:randCloudSpeed position:ccp(-300,0)];
    
    id seq1 = [CCSequence actions:move,nil];
    id seq2 = [CCSequence actions:move2,nil];
    id seq3 = [CCSequence actions:move3,nil];
    
    [p7Kumo1 runAction:seq1];
    [p7Kumo2 runAction:seq2];
    [p7Kumo3 runAction:seq3];
    
    
}

-(void)p7CloudMove2{
    
    //雲のスピードをランダムにする
    int randCloudSpeed = CCRANDOM_0_1()*10 + 30;
    int randCloudSpeed2 = CCRANDOM_0_1()*5+10;
    
    id move = [CCMoveTo actionWithDuration:randCloudSpeed position:ccp(-480,0)];
    id move2 = [CCMoveTo actionWithDuration:randCloudSpeed position:ccp(-480,0)];
    id move3 = [CCMoveTo actionWithDuration:randCloudSpeed position:ccp(-480,0)];
    
    id func2 = [CCCallFunc actionWithTarget:self selector:@selector(p7CloudRemove_1)];
    id func3 = [CCCallFunc actionWithTarget:self selector:@selector(p7CloudRemove_2)];
    id func4 = [CCCallFunc actionWithTarget:self selector:@selector(p7CloudRemove_3)];
    
    
    id seq1 = [CCSequence actions:move,func2,nil];
    id seq2 = [CCSequence actions:[CCDelayTime actionWithDuration:randCloudSpeed2], move2,func3,nil];
    id seq3 = [CCSequence actions:[CCDelayTime actionWithDuration:randCloudSpeed2*2], move3,func4,nil];
    
    [p7Kumo1 runAction:seq1];
    [p7Kumo2 runAction:seq2];
    [p7Kumo3 runAction:seq3];
}

-(void)p7CloudMove2_1{
    
    //雲のスピードをランダムにする
    int randCloudSpeed = CCRANDOM_0_1()*10 + 30;
    id move = [CCMoveTo actionWithDuration:randCloudSpeed position:ccp(-480,0)];
    id func2 = [CCCallFunc actionWithTarget:self selector:@selector(p7CloudRemove_1)];
    id seq1 = [CCSequence actions:move,func2,nil];
    
    [p7Kumo1 runAction:seq1];
}

-(void)p7CloudMove2_2{
    
    //雲のスピードをランダムにする
    int randCloudSpeed = CCRANDOM_0_1()*10 + 30;
    id move = [CCMoveTo actionWithDuration:randCloudSpeed position:ccp(-480,0)];
    id func2 = [CCCallFunc actionWithTarget:self selector:@selector(p7CloudRemove_2)];
    id seq1 = [CCSequence actions:move,func2,nil];
    
    [p7Kumo2 runAction:seq1];
}

-(void)p7CloudMove2_3{
    
    //雲のスピードをランダムにする
    int randCloudSpeed = CCRANDOM_0_1()*10 + 30;
    id move = [CCMoveTo actionWithDuration:randCloudSpeed position:ccp(-480,0)];
    id func2 = [CCCallFunc actionWithTarget:self selector:@selector(p7CloudRemove_3)];
    id seq1 = [CCSequence actions:move,func2,nil];
    
    [p7Kumo3 runAction:seq1];
}

//拡大後の処理

-(void)p7CloudRemove_1{
    [p7Kumo1 removeAllChildrenWithCleanup:YES];
    p7Kumo1 = [CCLayer node];
    p7Kumo1.position = ccp(1100, 0);
    p7Kumo1.anchorPoint = ccp(0.5f,0.5f);
    [self addChild:p7Kumo1 z:2];
    [self p7CloudCreat2_1];
}

-(void)p7CloudRemove_2{
    [p7Kumo2 removeAllChildrenWithCleanup:YES];
    p7Kumo2 = [CCLayer node];
    p7Kumo2.position = ccp(1100, 0);
    p7Kumo2.anchorPoint = ccp(0.5f,0.5f);
    [self addChild:p7Kumo2 z:2];
    [self p7CloudCreat2_2];
}

-(void)p7CloudRemove_3{
    [p7Kumo3 removeAllChildrenWithCleanup:YES];
    p7Kumo3 = [CCLayer node];
    p7Kumo3.position = ccp(1100, 0);
    p7Kumo3.anchorPoint = ccp(0.5f,0.5f);
    [self addChild:p7Kumo3 z:2];
    [self p7CloudCreat2_3];
}

-(void)p7CloudRemove2{
    
    [p7Kumo1 stopAllActions];
    [p7Kumo1 removeAllChildrenWithCleanup:YES];
    
    p7Kumo1 = [CCLayer node];
    p7Kumo1.anchorPoint = ccp(0.5f,0.5f);
    p7Kumo1.position = ccp(1100, 0);
    [self addChild:p7Kumo1 z:2];
    
    [p7Kumo2 stopAllActions];
    [p7Kumo2 removeAllChildrenWithCleanup:YES];
    p7Kumo2 = [CCLayer node];
    p7Kumo2.anchorPoint = ccp(0.5f,0.5f);
    p7Kumo2.position = ccp(1100, 0);
    [self addChild:p7Kumo2 z:2];
    
    [p7Kumo3 stopAllActions];
    [p7Kumo3 removeAllChildrenWithCleanup:YES];
    p7Kumo3 = [CCLayer node];
    p7Kumo3.anchorPoint = ccp(0.5f,0.5f);
    p7Kumo3.position = ccp(1100, 0);
    [self addChild:p7Kumo3 z:2];
    
    [self p7CloudCreat2];
}

- (void)p7_walk3{
    
    //通常船の削除
    
    [self removeChild:murabitoNoboruLayer cleanup:YES];
    
    CCLayer *murabitoNoboruLayer2 = [CCLayer node];
    [self addChild:murabitoNoboruLayer2];
    
    CCSprite *murabitoMoveFoot2_1 = [CCSprite spriteWithSpriteFrameName:@"p7_fune2_1.png"];
    murabitoMoveFoot2_1.anchorPoint = ccp(0.5,0);
    murabitoMoveFoot2_1.position = ccp(430, 578);
    
    CCSprite *murabitoMoveFoot3_1 = [CCSprite spriteWithSpriteFrameName:@"p7_fune2_2.png"];
    murabitoMoveFoot3_1.anchorPoint = ccp(0.5,0);
    murabitoMoveFoot3_1.position = ccp(505, 578);
    
    CCSprite *murabitoMoveFoot4_1 = [CCSprite spriteWithSpriteFrameName:@"p7_fune2_3.png"];
    murabitoMoveFoot4_1.anchorPoint = ccp(0.5,0);
    murabitoMoveFoot4_1.position = ccp(587, 578);
    
    CCSprite *murabitoFune1 = [CCSprite spriteWithSpriteFrameName:@"p7_fune3_1.png"];
    murabitoFune1.anchorPoint = ccp(0.5,0);
    murabitoFune1.position = ccp(430, 630);
    
    CCSprite *murabitoFune2 = [CCSprite spriteWithSpriteFrameName:@"p7_fune3_2.png"];
    murabitoFune2.anchorPoint = ccp(0.5,0);
    murabitoFune2.position = ccp(505, 630);
    
    CCSprite *murabitoFune3 = [CCSprite spriteWithSpriteFrameName:@"p7_fune3_3.png"];
    murabitoFune3.anchorPoint = ccp(0.5,0);
    murabitoFune3.position = ccp(587, 630);
    
    
    CCSprite *murabitoFunenoru1 = [CCSprite spriteWithSpriteFrameName:@"p7_fune5.png"];
    murabitoFunenoru1.anchorPoint = ccp(0.5,0);
    murabitoFunenoru1.scale = 0.3;
    murabitoFunenoru1.position = ccp(508, 575);
    
    [murabitoNoboruLayer2 addChild:murabitoMoveFoot2_1];
    [murabitoNoboruLayer2 addChild:murabitoMoveFoot3_1];
    [murabitoNoboruLayer2 addChild:murabitoMoveFoot4_1];
    
    [murabitoNoboruLayer2 addChild:murabitoFune1];
    [murabitoNoboruLayer2 addChild:murabitoFune2];
    [murabitoNoboruLayer2 addChild:murabitoFune3];
    
    
    
    //船落ちる
    
    CCLayer *murabitoNoboruLayer3 = [CCLayer node];
    [self addChild:murabitoNoboruLayer3];
    
    id move1 =[CCMoveBy actionWithDuration:0.5f position:ccp(0, -55)];
    id ease1 = [CCEaseBackIn actionWithAction:move1];
    
    id move2 =[CCMoveBy actionWithDuration:0.5f position:ccp(0, -55)];
    id ease2 = [CCEaseBackIn actionWithAction:move2];
    
    id move3 =[CCMoveBy actionWithDuration:0.5f position:ccp(0, -52)];
    id ease3 = [CCEaseBackIn actionWithAction:move3];
    
    //船の拡大最終
    id func5 = [CCCallBlock actionWithBlock:^{
        
        p7Kakudai = true;
        
        id scale = [CCScaleTo actionWithDuration:0.5 scale:1.0];
        id move = [CCMoveBy actionWithDuration:0.5 position:ccp(0,-300)];
        
        id scale2 = [CCScaleTo actionWithDuration:0.5 scale:3.5];
        id move2 = [CCMoveBy actionWithDuration:0.5 position:ccp(0,-540)];
        
        id ease = [CCEaseExponentialOut actionWithAction:scale];
        id ease2 = [CCEaseExponentialOut actionWithAction:move];
        
        id ease3 = [CCEaseExponentialOut actionWithAction:scale2];
        id ease4 = [CCEaseExponentialOut actionWithAction:move2];
        
        id spawn = [CCSpawn actions:ease,ease2,nil];
        id spawn2 = [CCSpawn actions:ease3,ease4,nil];
        
        
        [murabitoFunenoru1 runAction:spawn];
        [p7Mountain runAction:spawn2];
        [self p7CloudRemove2];
        
    }];
    
    id func4 = [CCCallBlock actionWithBlock:^{
        
        [self removeChild:murabitoNoboruLayer2 cleanup:YES];
        
        
        [murabitoNoboruLayer3 addChild:murabitoFunenoru1];
        [murabitoNoboruLayer3 runAction:func5];
    }];
    
    id func1 = [CCCallBlock actionWithBlock:^{
        id jump1 = [CCJumpBy actionWithDuration:0.5 position:ccp(0, 0) height:30 jumps:1];
        [murabitoMoveFoot2_1 runAction:jump1];
        
    }];
    
    id func2 = [CCCallBlock actionWithBlock:^{
        
        id jump2 = [CCJumpBy actionWithDuration:0.5 position:ccp(0, 0) height:30 jumps:1];
        [murabitoMoveFoot3_1 runAction:jump2];
    }];
    
    id func3 = [CCCallBlock actionWithBlock:^{
        
        id jump3 = [CCJumpBy actionWithDuration:0.5 position:ccp(0, 0) height:30 jumps:1];
        id seq = [CCSequence actions:jump3,func4,nil];
        [murabitoMoveFoot4_1 runAction:seq];
    }];
    
    
    
    
    id seq1 = [CCSequence actions:ease1,func1, nil];
    id seq2 = [CCSequence actions:ease2,func2, nil];
    id seq3 = [CCSequence actions:ease3,func3, nil];
    
    [murabitoFune1 runAction:seq1];
    [murabitoFune2 runAction:seq2];
    [murabitoFune3 runAction:seq3];
    
    
    
    
}
- (void)p7_walk2{
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    //のぼるアクション
    murabitoNoboruLayer = [CCLayer node];
    [self addChild:murabitoNoboruLayer];
    
    murabitoMoveFoot2 = [CCSprite spriteWithSpriteFrameName:@"p7_murabitoMove2_foot2_1.png"];
    murabitoMoveFoot2.anchorPoint = ccp(0.5,0);
    murabitoMoveFoot2.position = ccp(winSize.width+murabitoMoveFoot2.contentSize.width/2, 100);
    
    murabitoMoveFoot3 = [CCSprite spriteWithSpriteFrameName:@"p7_murabitoMove2_foot3_1.png"];
    murabitoMoveFoot3.anchorPoint = ccp(0.5,0);
    murabitoMoveFoot3.position = ccp(winSize.width+murabitoMoveFoot2.contentSize.width/2, 100);
    
    murabitoMoveFoot4 = [CCSprite spriteWithSpriteFrameName:@"p7_murabitoMove2_foot4_1.png"];
    murabitoMoveFoot4.anchorPoint = ccp(0.5,0);
    murabitoMoveFoot4.position = ccp(winSize.width+murabitoMoveFoot2.contentSize.width/2, 100);
    
    [murabitoNoboruLayer addChild:murabitoMoveFoot2];
    [murabitoNoboruLayer addChild:murabitoMoveFoot3];
    [murabitoNoboruLayer addChild:murabitoMoveFoot4];
    
    
    NSMutableArray *arrayWalk2 = [NSMutableArray array];
    NSMutableArray *arrayWalk3 = [NSMutableArray array];
    NSMutableArray *arrayWalk4 = [NSMutableArray array];
    
    for(int i=1; i<=2; i++){
        [arrayWalk2 addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"p7_murabitoMove2_foot2_%d.png",i]]];
        [arrayWalk3 addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"p7_murabitoMove2_foot3_%d.png",i]]];
        [arrayWalk4 addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"p7_murabitoMove2_foot4_%d.png",i]]];
    }
    
    CCAnimation *murabitoWalkAnime2 = [CCAnimation animationWithSpriteFrames:arrayWalk2 delay:0.2];
    CCAnimate *murabitoWalkAnimete2 = [CCAnimate actionWithAnimation:murabitoWalkAnime2];
    
    CCAnimation *murabitoWalkAnime3 = [CCAnimation animationWithSpriteFrames:arrayWalk3 delay:0.2];
    CCAnimate *murabitoWalkAnimete3 = [CCAnimate actionWithAnimation:murabitoWalkAnime3];
    
    CCAnimation *murabitoWalkAnime4 = [CCAnimation animationWithSpriteFrames:arrayWalk4 delay:0.2];
    CCAnimate *murabitoWalkAnimete4 = [CCAnimate actionWithAnimation:murabitoWalkAnime4];
    
    id rep2 = [CCRepeatForever actionWithAction:murabitoWalkAnimete2];
    id rep3 = [CCRepeatForever actionWithAction:murabitoWalkAnimete3];
    id rep4 = [CCRepeatForever actionWithAction:murabitoWalkAnimete4];
    
    [murabitoMoveFoot2 runAction:rep2];
    [murabitoMoveFoot3 runAction:rep3];
    [murabitoMoveFoot4 runAction:rep4];
    
    
    
    id move2 = [CCMoveBy actionWithDuration:0.5f position:ccp(-100-murabitoMoveFoot2.contentSize.width/2, 0)];
    id rote = [CCRotateTo actionWithDuration:0.2 angle:56];
    id move3 = [CCMoveBy actionWithDuration:1.5f position:ccp(-308, 477)];
    id rote2 = [CCRotateTo actionWithDuration:0.2 angle:0];
    id move4_1 = [CCMoveBy actionWithDuration:1.0f position:ccp(-180, 0)];
    
    id move2_2 = [CCMoveBy actionWithDuration:0.5f position:ccp(-100-murabitoMoveFoot2.contentSize.width/2, 0)];
    id rote_2 = [CCRotateTo actionWithDuration:0.2 angle:56];
    id move3_2 = [CCMoveBy actionWithDuration:1.5f position:ccp(-308, 477)];
    id rote2_2 = [CCRotateTo actionWithDuration:0.2 angle:0];
    id move4_2 = [CCMoveBy actionWithDuration:1.0f position:ccp(-105, 0)];
    
    id move2_3 = [CCMoveBy actionWithDuration:0.5f position:ccp(-100-murabitoMoveFoot2.contentSize.width/2, 0)];
    id rote_3 = [CCRotateTo actionWithDuration:0.2 angle:56];
    id move3_3 = [CCMoveBy actionWithDuration:1.5f position:ccp(-308, 477)];
    id rote2_3 = [CCRotateTo actionWithDuration:0.2 angle:0];
    id move4_3 = [CCMoveBy actionWithDuration:1.0f position:ccp(-30, 0)];
    id func = [CCCallFunc actionWithTarget:self selector:@selector(p7_walk3)];
    
    
    id seq2 = [CCSequence actions:[CCDelayTime actionWithDuration:1.0],move2,rote,move3,rote2,move4_1,nil];
    id seq3 = [CCSequence actions:[CCDelayTime actionWithDuration:1.4],move2_2,rote_2,move3_2,rote2_2,move4_2,nil];
    id seq4 = [CCSequence actions:[CCDelayTime actionWithDuration:1.8],move2_3,rote_3,move3_3,rote2_3,move4_3,[CCDelayTime actionWithDuration:1.0f],func,nil];
    
    [murabitoMoveFoot2 runAction:seq2];
    [murabitoMoveFoot3 runAction:seq3];
    [murabitoMoveFoot4 runAction:seq4];
    
    
}
- (void)p7_walk{
    
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    CCLayer *murabitoWalkrLayer =  [CCLayer node];
    [self addChild:murabitoWalkrLayer];
    
    CCSprite *murabitoMove = [CCSprite spriteWithSpriteFrameName:@"p7_murabitoMove1.png"];
    murabitoMove.position = ccp(winSize.width/2, 170);
    [murabitoWalkrLayer addChild:murabitoMove];
    
    CCSprite *murabitoMoveFoot = [CCSprite spriteWithSpriteFrameName:@"p7_murabitoMove1_foot1.png"];
    murabitoMoveFoot.position = ccp(winSize.width/2, 110);
    [murabitoWalkrLayer addChild:murabitoMoveFoot];
    
    
    NSMutableArray *arrayWalk = [NSMutableArray array];
    for(int i=1; i<=2; i++){
        [arrayWalk addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"p7_murabitoMove1_foot%d.png",i]]];
    }
    
    CCAnimation *murabitoWalkAnime = [CCAnimation animationWithSpriteFrames:arrayWalk delay:0.2];
    CCAnimate *murabitoWalkAnimete = [CCAnimate actionWithAnimation:murabitoWalkAnime];
    id rep = [CCRepeatForever actionWithAction:murabitoWalkAnimete];
    
    [murabitoMoveFoot runAction:rep];
    
    id move = [CCMoveBy actionWithDuration:3.0f position:ccp(650, 0)];
    id func = [CCCallFunc actionWithTarget:self selector:@selector(p7_walk2)];
    id seq = [CCSequence actions:[CCDelayTime actionWithDuration:1.0],move,func,nil];
    [murabitoWalkrLayer runAction:seq];
    
}
-(void) dealloc
{
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [super dealloc];
}

@end
