

#import <Foundation/Foundation.h>
#import "cocos2d.h"

#define kSceneChoiseA   0
#define kSceneChoiseB   1
#define kSceneChoiseC   2
#define kSceneChoiseD   3
#define kSceneChoiseE   4

@interface SceneManager : NSObject {
}

+ (CCScene *)nextSceneOfScene:(CCNode *)from choice:(NSInteger)choise nextBool:(BOOL)nextPage;
@end