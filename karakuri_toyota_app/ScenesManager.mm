
#import "ScenesManager.h"
#include "MasterScene.h"


static struct {
    int current;
    int next;
    NSString *sceneClassName;
} Graph[] = {
    { 0,  1, @"MasterScene"},
    { 1,  2, @"MasterScene"},
    { 2,  3, @"MasterScene"},
    { 3,  4, @"MasterScene"},
    { 4,  5, @"MasterScene"},
    { 5,  6, @"MasterScene"},
    { 6,  7, @"MasterScene"},
    { 7,  8, @"MasterScene"},
    { 8,  9, @"MasterScene"},
    { 9,  10, @"MasterScene"},
    { 10,  11, @"MasterScene"},
    { 11,  12, @"MasterScene"},
    { 12,  13, @"MasterScene"},
    { 13,  14, @"MasterScene"},
    { 14,  15, @"MasterScene"},
    { 15,  16, @"MasterScene"},
};

@implementation SceneManager
+ (CCScene *)nextSceneOfScene:(CCNode *)from choice:(NSInteger)choise nextBool:(BOOL)nextPage {
    int nextSceneId;
    
    if (choise == 0) {
        nextSceneId = Graph[from.tag].next;
        //nextSceneId = 11;

    }else if (choise == 1){
        nextSceneId = Graph[from.tag].current-1;
    }else if (choise == 2){
        nextSceneId = 0;
    }else if (choise == 3){
        nextSceneId = 13;
    }else{
        nextSceneId = 15;
    }
    CCScene *nextScene;
    if(nextSceneId == 16){
        CCLOG(@"next%d",nextSceneId);
        nextSceneId = 0;
        nextScene = [NSClassFromString(Graph[nextSceneId].sceneClassName)sceneWithSceneId:nextSceneId nextBool:nextPage];
    }else{
        nextScene = [NSClassFromString(Graph[nextSceneId].sceneClassName)sceneWithSceneId:nextSceneId nextBool:nextPage];
    }
    nextScene.tag = nextSceneId;
    return nextScene;
}

@end
