//
//  p0.h
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/08/06.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCButton.h"
#import "CCTransitionHelper.h"
#import "ScenesManager.h"
#import "SoundEffect.h"

@interface p0 : CCLayer <UIWebViewDelegate>{
    CCSprite *murabito1;
    CCSprite *murabito2;
    CCSprite *murabito3;
    CCSprite *settingBackground;
    CCSprite *settingBackground2;
    CCSprite *settingBackground3;
    
    CCSprite *settei_btn1;
    CCSprite *settei_btn2;
    CCSprite *settei_btn3;
    CCSprite *settei_btn4;
    CCSprite *settei_btn5;
    CCSprite *settei_btn6;
    CCSprite *settei_btn7;
    CCSprite *settei_close;
    CCSprite *settei_close2;
    CCSprite *settei_close3;
    CCSprite *settei_close4;
    
    CCSprite *startButton;
    CCSprite *startButton_selected;
    
    CCSprite *setteiButton;
    CCSprite *setteiButton_selected;
    
    //設定
    int setting_text;
    int setting_narra;
    int setting_effect;

    bool touch_setting;
    bool touch_start;
    bool touch_howto;
    bool tochh_enable;
    
    UIWebView *webView_;
    UIToolbar *toolBar;
    UIActivityIndicatorView *activityInducator_;
    
}

@end
