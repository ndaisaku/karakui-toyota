//
//  p4.h
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 13/02/05.
//  Copyright __MyCompanyName__ 2013年. All rights reserved.
//


#import <GameKit/GameKit.h>

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"
#import "SoundEffect.h"

//Pixel to metres ratio. Box2D uses metres as the unit for measurement.
//This ratio defines how many pixels correspond to 1 Box2D "metre"
//Box2D is optimized for objects of 1x1 metre therefore it makes sense
//to define the ratio so that your most common object type is 1x1 metre.
#define PTM_RATIO 32

// p4
@interface p4 : CCLayer
{
	CCTexture2D *spriteTexture_;	// weak ref
    CCTexture2D *spriteTexture2_;	// weak ref
	b2World* world;					// strong ref
	GLESDebugDraw *m_debugDraw;		// strong ref
    b2BodyDef bodyDef;
    b2MouseJoint *_mouseJoint;
    b2Body* groundBody;
    b2BuoyancyController *bc;
}

// returns a CCScene that contains the p4 as the only child
+(CCScene *) scene;

@end
