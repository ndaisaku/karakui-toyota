//
//  p9.h
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SoundEffect.h"

@interface p9 : CCLayer {
    CCSprite *p9kuma[2];
    CCSprite *p9hituji[2];
    CCSprite *p9nezumi[2];
    CCSprite *p9uma[2];
    CCSprite *p9tori[2];
    
    CCSprite *p9iwa;
    int p9iwaCount;
    bool p9_fall_iwa;
    float ameSpeed;
}

@end
