//
//  SoundEffect.m
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/10/27.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "SoundEffect.h"

@implementation SoundEffect

+(void)seLoad{
    [self seLoading:@"ofn_se_p1_hiyoko1.caf"];
    [self seLoading:@"ofn_se_p1_ohusuvoice.caf"];
    [self seLoading:@"ofn_se_p1_yamatovoice.caf"];
    [self seLoading:@"ofn_se_p1_honou.mp3"];
    [self seLoading:@"ofn_se_p9_rock1.mp3"];
    [self seLoading:@"ofn_se_p2_ken.mp3"];
    [self seLoading:@"ofn_se_p2_ya.mp3"];
    [self seLoading:@"ofn_se_p5_dead1.caf"];
    [self seLoading:@"ofn_se_p5_dead6.caf"];
    [self seLoading:@"ofn_se_p5_dead7.caf"];
    [self seLoading:@"ofn_se_p5_snakemove1.caf"];
    [self seLoading:@"ofn_se_p5_snakemove3.caf"];
    [self seLoading:@"ofn_se_p5_ki.mp3"];
    [self seLoading:@"ofn_se_p5_kanaduti.mp3"];
    [self seLoading:@"ofn_se_p5_snakebite1.mp3"];
    [self seLoading:@"ofn_se_p5_kuwa.mp3"];
    [self seLoading:@"ofn_se_p5_kama.mp3"];
    [self seLoading:@"ofn_se_p6_te.mp3"];
    [self seLoading:@"ofn_se_p8_pon.mp3"];
    [self seLoading:@"ofn_se_p8_kati.mp3"];
    [self seLoading:@"ofn_se_p8_kata.mp3"];
    [self seLoading:@"ofn_se_p10_te.mp3"];
    [self seLoading:@"ofn_se_p11_snakemove.mp3"];
    
}

+(void)seLoading:(NSString *)soundName{
    [[SimpleAudioEngine sharedEngine] preloadEffect:soundName];
}

+(void)sePlay:(NSString *)soundName{
    NSUserDefaults *userDefault =[NSUserDefaults standardUserDefaults];
    int setting_effect = [userDefault integerForKey:@"SETTING3"];
    if (!setting_effect ) {
        [[SimpleAudioEngine sharedEngine] playEffect:soundName];
    }
    
}



@end
