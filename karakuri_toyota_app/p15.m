//
//  p14.m
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "p15.h"

typedef enum nodeTags
{
    kTagMenu1,
    kTagMenu1_2,
    kTagtMenu2,
    kTagtMenu2_2,
    kTagtMenu3,
    kTagtMenu3_2,
    kTagtMenu4,
    kTagtMenu4_2,
    kTagtMenu5,
    kTagtMenu5_2,
    kTagtMenu6,
    kTagtMenu6_2,
    kTagscroll,
    kTagtxtTouch,
    kTagpictSanageyama1,
    kTagpictSanageyama2,
    kTagpictSanageyama3,
    kTagYamaFrame,
    kTagMapmenu,
    kTagMapmenu1,
    kTagMapmenu2,
} Tags;

@implementation p15
-(id)init{
    if(self = [super init]){
        self.ignoreAnchorPointForPosition = NO;
        self.isTouchEnabled = YES;
        CGSize winsize = [[CCDirector sharedDirector] winSize];
        
        int menuPosition = 110;
        int menuPosition_margin = 0;

        CCSpriteFrameCache *frameChash = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameChash addSpriteFramesWithFile:@"yukari.plist"];
        
        CCSpriteBatchNode *frameBach = [CCSpriteBatchNode batchNodeWithFile:@"yukari.pvr.gz"];
        [self addChild:frameBach];
        
        CCSprite *p14bg = [CCSprite spriteWithSpriteFrameName:@"bg_frame.png"];
        p14bg.position = ccp(winsize.width/2,winsize.height/2);
        [self addChild:p14bg];
        
        CCSprite *pict_p14murabito1 = [CCSprite spriteWithSpriteFrameName:@"pict_murabito_right.png"];
        pict_p14murabito1.position = ccp(winsize.width-150,110);
        [self addChild:pict_p14murabito1 z:10];
        
        //村人
        CCSprite *murabito1 = [CCSprite spriteWithSpriteFrameName:@"pict_murabito_top.png"];
        murabito1.position = ccp(100, 690);
        [self addChild:murabito1];
        
        //タイトル
        CCSprite *txt_titlep14 = [CCSprite spriteWithSpriteFrameName:@"txt_titleyukari.png"];
        txt_titlep14.position = ccp(winsize.width/2, winsize.height-txt_titlep14.contentSize.height/2-38);
        [self addChild:txt_titlep14 z:10];
        
        //メニュー
        CCSprite *menu_on_p14sanageyama = [CCSprite spriteWithSpriteFrameName:@"btn_on_sanageyama.png"];
        menu_on_p14sanageyama.position = ccp(menuPosition, winsize.height-165);
        //menu_on_p14sanageyama.opacity = 0;
        [self addChild:menu_on_p14sanageyama z:14 tag:kTagMenu1_2];
        
        CCSprite *menu_on_p14sanagejinjya = [CCSprite spriteWithSpriteFrameName:@"btn_on_sanagejinjya.png"];
        menu_on_p14sanagejinjya.position = ccp(menu_on_p14sanageyama.contentSize.width+menuPosition_margin+menuPosition, winsize.height-165);
        menu_on_p14sanagejinjya.opacity = 0;

        [self addChild:menu_on_p14sanagejinjya z:14 tag:kTagtMenu2_2];
        
        CCSprite *menu_on_p14sanagematuri = [CCSprite spriteWithSpriteFrameName:@"btn_on_sanagematuri.png"];
        menu_on_p14sanagematuri.position = ccp(menu_on_p14sanageyama.contentSize.width*2+menuPosition_margin*2+menuPosition, winsize.height-165);
        menu_on_p14sanagematuri.opacity = 0;
        [self addChild:menu_on_p14sanagematuri z:14 tag:kTagtMenu3_2];
        
        CCSprite *menu_on_p14yahagigawa = [CCSprite spriteWithSpriteFrameName:@"btn_on_yahagigawa.png"];
        menu_on_p14yahagigawa.position = ccp(menu_on_p14sanageyama.contentSize.width*3+menuPosition_margin*3+menuPosition, winsize.height-165);
        menu_on_p14yahagigawa.opacity = 0;
        [self addChild:menu_on_p14yahagigawa z:14 tag:kTagtMenu4_2];
        
        CCSprite *menu_on_p14shirahige = [CCSprite spriteWithSpriteFrameName:@"btn_on_shirahigesou.png"];
        menu_on_p14shirahige.position = ccp(menu_on_p14sanageyama.contentSize.width*4+menuPosition_margin*4+menuPosition, winsize.height-165);
        menu_on_p14shirahige.opacity = 0;
        [self addChild:menu_on_p14shirahige z:14 tag:kTagtMenu5_2];
        
        CCSprite *menu_on_p14link = [CCSprite spriteWithSpriteFrameName:@"btn_on_link.png"];
        menu_on_p14link.position = ccp(menu_on_p14sanageyama.contentSize.width*5+menuPosition_margin*5+menuPosition, winsize.height-165);
        menu_on_p14link.opacity = 0;
        [self addChild:menu_on_p14link z:14 tag:kTagtMenu6_2];
        
        
        
        
        CCSprite *menu_p14sanageyama = [CCSprite spriteWithSpriteFrameName:@"btn_sanageyama.png"];
        menu_p14sanageyama.position = ccp(menuPosition, winsize.height-165);
        [self addChild:menu_p14sanageyama z:10 tag:kTagMenu1];
        
        CCSprite *menu_p14sanagejinjya = [CCSprite spriteWithSpriteFrameName:@"btn_sanagejinjya.png"];
        menu_p14sanagejinjya.position = ccp(menu_on_p14sanageyama.contentSize.width+menuPosition_margin+menuPosition, winsize.height-165);
        [self addChild:menu_p14sanagejinjya z:11 tag:kTagtMenu2];
        
        CCSprite *menu_p14sanagematuri = [CCSprite spriteWithSpriteFrameName:@"btn_sanagematuri.png"];
        menu_p14sanagematuri.position = ccp(menu_on_p14sanageyama.contentSize.width*2+menuPosition_margin*2+menuPosition, winsize.height-165);
        [self addChild:menu_p14sanagematuri z:12 tag:kTagtMenu3];
        
        CCSprite *menu_p14yahagigawa = [CCSprite spriteWithSpriteFrameName:@"btn_yahagigawa.png"];
        menu_p14yahagigawa.position = ccp(menu_on_p14sanageyama.contentSize.width*3+menuPosition_margin*3+menuPosition, winsize.height-165);
        [self addChild:menu_p14yahagigawa z:12 tag:kTagtMenu4];
        
        CCSprite *menu_p14shirahige = [CCSprite spriteWithSpriteFrameName:@"btn_shirahigesou.png"];
        menu_p14shirahige.position = ccp(menu_on_p14sanageyama.contentSize.width*4+menuPosition_margin*4+menuPosition, winsize.height-165);
        [self addChild:menu_p14shirahige z:12 tag:kTagtMenu5];
        
        CCSprite *menu_p14link = [CCSprite spriteWithSpriteFrameName:@"btn_link.png"];
        menu_p14link.position = ccp(menu_on_p14sanageyama.contentSize.width*5+menuPosition_margin*5+menuPosition, winsize.height-165);
        [self addChild:menu_p14link z:12 tag:kTagtMenu6];
        
        
        
        
        //スクロールレイヤー
        CCLayer *layer_scroll = [CCLayer node];
        layer_scroll.anchorPoint = ccp(0.5f,0.5f);
        [self addChild:layer_scroll z:1 tag:kTagscroll];
        
        //猿投山
        layer_sanageyama = [CCLayer node];
        layer_sanageyama.anchorPoint = ccp(0.5f,0.5f);
        [layer_scroll addChild:layer_sanageyama];
        
        CCSprite *sanageyama_txt = [CCSprite spriteWithSpriteFrameName:@"txt_sanageyama.png"];
        sanageyama_txt.position = ccp(winsize.width-sanageyama_txt.contentSize.width/2-60,winsize.height-260);
        sanageyama_txt.anchorPoint = ccp(0.5, 1.0);

        [layer_sanageyama addChild:sanageyama_txt];
        
        
        //猿投山写真
        

        
        CCSprite *pict_sanageyama1 = [CCSprite spriteWithSpriteFrameName:@"pict_sanageyama1.png"];
        pict_sanageyama1.position = ccp(272, 390);
        [layer_sanageyama addChild:pict_sanageyama1 z:10 tag:kTagpictSanageyama1];
        
        CCSprite *pict_sanageyama2 = [CCSprite spriteWithSpriteFrameName:@"pict_sanageyama2.png"];
        pict_sanageyama2.position = ccp(272, 390);
        pict_sanageyama2.opacity = 0;
        [layer_sanageyama addChild:pict_sanageyama2 z:10 tag:kTagpictSanageyama2];
        
        CCSprite *pict_sanageyama3 = [CCSprite spriteWithSpriteFrameName:@"pict_sanageyama3.png"];
        pict_sanageyama3.position = ccp(272, 390);
        pict_sanageyama3.opacity = 0;
        [layer_sanageyama addChild:pict_sanageyama3 z:10 tag:kTagpictSanageyama3];
        
        CCSprite *pict_frame = [CCSprite spriteWithSpriteFrameName:@"pict_photoFrame.png"];
        pict_frame.position = ccp(273, 335);
        [layer_sanageyama addChild:pict_frame z:11];
    
        
        //thumbnail btn
        CCSprite *btn_sanageyama1 = [CCSprite spriteWithSpriteFrameName:@"pict_sanageyama1_thumb.png"];
        CCSprite *btn_sanageyama2 = [CCSprite spriteWithSpriteFrameName:@"pict_on_sanageyama1_thumb.png"];
        
        CCSprite *btn_sanageyama2_1 = [CCSprite spriteWithSpriteFrameName:@"pict_sanageyama2_thumb.png"];
        CCSprite *btn_sanageyama2_2 = [CCSprite spriteWithSpriteFrameName:@"pict_on_sanageyama2_thumb.png"];
        
        CCSprite *btn_sanageyama3_1 = [CCSprite spriteWithSpriteFrameName:@"pict_sanageyama3_thumb.png"];
        CCSprite *btn_sanageyama3_2 = [CCSprite spriteWithSpriteFrameName:@"pict_on_sanageyama3_thumb.png"];
        
        CCMenuItemSprite * btnItem_sanage1 = [CCMenuItemSprite itemWithNormalSprite:btn_sanageyama1 selectedSprite:btn_sanageyama2 target:self selector:@selector(photoChange:)];
        btnItem_sanage1.position = ccp(-350, -200);
        btnItem_sanage1.tag = 1;
        
        CCMenuItemSprite * btnItem_sanage2 = [CCMenuItemSprite itemWithNormalSprite:btn_sanageyama2_1 selectedSprite:btn_sanageyama2_2 target:self selector:@selector(photoChange:)];
        btnItem_sanage2.position = ccp(-240, -200);
        btnItem_sanage2.tag = 2;

        
        CCMenuItemSprite * btnItem_sanage3 = [CCMenuItemSprite itemWithNormalSprite:btn_sanageyama3_1 selectedSprite:btn_sanageyama3_2 target:self selector:@selector(photoChange:)];
        btnItem_sanage3.position = ccp(-130, -200);
        btnItem_sanage3.tag = 3;

        
        CCMenu *sanageMenu = [CCMenu menuWithItems:btnItem_sanage1,btnItem_sanage2,btnItem_sanage3,nil];
        
        [sanageMenu setPosition: ccp(winsize.width/2, winsize.height/2)];
        [layer_sanageyama addChild:sanageMenu z:12];
        
        CCSprite *pict_thumb_frame = [CCSprite spriteWithSpriteFrameName:@"pict_thumb_frame.png"];
        pict_thumb_frame.position = ccp(162, 183);
        [layer_sanageyama addChild:pict_thumb_frame z:20 tag:kTagYamaFrame];

        
 
        
        
        //猿投神社
        layer_jinjya = [CCLayer node];
        layer_jinjya.anchorPoint = ccp(0.5f,0.5f);
        layer_jinjya.position = ccp(1024, 0);
        [layer_scroll addChild:layer_jinjya];
        
        CCSprite *sanagejinjya_txt = [CCSprite spriteWithSpriteFrameName:@"txt_sanagejinjya.png"];
        sanagejinjya_txt.position = ccp(winsize.width-sanageyama_txt.contentSize.width/2-60,winsize.height-260);
        sanagejinjya_txt.anchorPoint = ccp(0.5, 1.0);

        [layer_jinjya addChild:sanagejinjya_txt];
        
        CCSprite *pict_frame2 = [CCSprite spriteWithSpriteFrameName:@"pict_photoFrame.png"];
        pict_frame2.position = ccp(273, 335);
        [layer_jinjya addChild:pict_frame2 z:11];
        
        
        CCSprite *pict_sanagejinjya1 = [CCSprite spriteWithSpriteFrameName:@"pict_sanagejinjya1.png"];
        pict_sanagejinjya1.position = ccp(272, 390);
        [layer_jinjya addChild:pict_sanagejinjya1 z:10 tag:kTagpictSanageyama1];
        
        CCSprite *pict_sanagejinjya2 = [CCSprite spriteWithSpriteFrameName:@"pict_sanagejinjya2.png"];
        pict_sanagejinjya2.position = ccp(272, 390);
        pict_sanagejinjya2.opacity = 0;
        [layer_jinjya addChild:pict_sanagejinjya2 z:10 tag:kTagpictSanageyama2];

        
        
        //thumbnail btn
        CCSprite *btn_sanagejinjya1 = [CCSprite spriteWithSpriteFrameName:@"pict_sanagejinjya1_thumb.png"];
        CCSprite *btn_sanagejinjya2 = [CCSprite spriteWithSpriteFrameName:@"pict_on_sanagejinjya1_thumb.png"];
        
        CCSprite *btn_sanagejinjya2_1 = [CCSprite spriteWithSpriteFrameName:@"pict_sanagejinjya2_thumb.png"];
        CCSprite *btn_sanagejinjya2_2 = [CCSprite spriteWithSpriteFrameName:@"pict_on_sanagejinjya2_thumb.png"];
        

        CCMenuItemSprite * btnItem_jinjya1 = [CCMenuItemSprite itemWithNormalSprite:btn_sanagejinjya1 selectedSprite:btn_sanagejinjya2 target:self selector:@selector(photoChange:)];
        btnItem_jinjya1.position = ccp(-280, -200);
        btnItem_jinjya1.tag = 4;
        
        
        CCMenuItemSprite * btnItem_jinjya2 = [CCMenuItemSprite itemWithNormalSprite:btn_sanagejinjya2_1 selectedSprite:btn_sanagejinjya2_2 target:self selector:@selector(photoChange:)];
        btnItem_jinjya2.position = ccp(-180, -200);
        btnItem_jinjya2.tag = 5;
        

        CCMenu *sanagejinjyaMenu = [CCMenu menuWithItems:btnItem_jinjya1,btnItem_jinjya2,nil];
        
        [sanagejinjyaMenu setPosition: ccp(winsize.width/2, winsize.height/2)];
        [layer_jinjya addChild:sanagejinjyaMenu z:12];
        
        CCSprite *pict_thumb_frame2 = [CCSprite spriteWithSpriteFrameName:@"pict_thumb_frame.png"];
        pict_thumb_frame2.position = ccp(231, 183);
        [layer_jinjya addChild:pict_thumb_frame2 z:20 tag:kTagYamaFrame];
        
        
        
        
        
        //猿投まつり
        layer_maturi = [CCLayer node];
        layer_maturi.anchorPoint = ccp(0.5f,0.5f);
        layer_maturi.position = ccp(1024*2, 0);
        [layer_scroll addChild:layer_maturi];
        
        CCSprite *maturi_txt = [CCSprite spriteWithSpriteFrameName:@"txt_samagematuri.png"];
        maturi_txt.position = ccp(winsize.width-sanageyama_txt.contentSize.width/2-80,winsize.height-260);
        maturi_txt.anchorPoint = ccp(0.5, 1.0);

        [layer_maturi addChild:maturi_txt];
        
        CCSprite *pict_frame3 = [CCSprite spriteWithSpriteFrameName:@"pict_photoFrame.png"];
        pict_frame3.position = ccp(273, 335);
        [layer_maturi addChild:pict_frame3 z:11];
        
        CCSprite *pict_sanagematuri1 = [CCSprite spriteWithSpriteFrameName:@"pict_sanagematuri1.png"];
        pict_sanagematuri1.position = ccp(272, 390);
        [layer_maturi addChild:pict_sanagematuri1 z:10 tag:kTagpictSanageyama1];
        
        CCSprite *pict_sanagematuri2 = [CCSprite spriteWithSpriteFrameName:@"pict_sanagematuri2.png"];
        pict_sanagematuri2.position = ccp(272, 390);
        pict_sanagematuri2.opacity = 0;
        [layer_maturi addChild:pict_sanagematuri2 z:10 tag:kTagpictSanageyama2];
        
        CCSprite *pict_sanagematuri3 = [CCSprite spriteWithSpriteFrameName:@"pict_sanagematuri3.png"];
        pict_sanagematuri3.position = ccp(272, 390);
        pict_sanagematuri3.opacity = 0;
        [layer_maturi addChild:pict_sanagematuri3 z:10 tag:kTagpictSanageyama3];
        
        
        //thumbnail btn
        CCSprite *btn_sanagematuri1 = [CCSprite spriteWithSpriteFrameName:@"pict_sanagematuri1_thumb.png"];
        CCSprite *btn_sanagematuri2 = [CCSprite spriteWithSpriteFrameName:@"pict_on_sanagematuri1_thumb.png"];
        
        CCSprite *btn_sanagematuri2_1 = [CCSprite spriteWithSpriteFrameName:@"pict_sanagematuri2_thumb.png"];
        CCSprite *btn_sanagematuri2_2 = [CCSprite spriteWithSpriteFrameName:@"pict_on_sanagematuri2_thumb.png"];
        
        CCSprite *btn_sanagematuri3_1 = [CCSprite spriteWithSpriteFrameName:@"pict_sanagematuri3_thumb.png"];
        CCSprite *btn_sanagematuri3_2 = [CCSprite spriteWithSpriteFrameName:@"pict_on_sanagematuri3_thumb.png"];
        
        CCMenuItemSprite * btnItem_sanagematuri1 = [CCMenuItemSprite itemWithNormalSprite:btn_sanagematuri1 selectedSprite:btn_sanagematuri2 target:self selector:@selector(photoChange:)];
        btnItem_sanagematuri1.position = ccp(-350, -200);
        btnItem_sanagematuri1.tag = 6;
        
        CCMenuItemSprite * btnItem_sanagematuri2 = [CCMenuItemSprite itemWithNormalSprite:btn_sanagematuri2_1 selectedSprite:btn_sanagematuri2_2 target:self selector:@selector(photoChange:)];
        btnItem_sanagematuri2.position = ccp(-240, -200);
        btnItem_sanagematuri2.tag = 7;
        
        
        CCMenuItemSprite * btnItem_sanagematuri3 = [CCMenuItemSprite itemWithNormalSprite:btn_sanagematuri3_1 selectedSprite:btn_sanagematuri3_2 target:self selector:@selector(photoChange:)];
        btnItem_sanagematuri3.position = ccp(-130, -200);
        btnItem_sanagematuri3.tag = 8;
        
        
        CCMenu *sanageMaturiMenu = [CCMenu menuWithItems:btnItem_sanagematuri1,btnItem_sanagematuri2,btnItem_sanagematuri3,nil];
        
        [sanageMaturiMenu setPosition: ccp(winsize.width/2, winsize.height/2)];
        [layer_maturi addChild:sanageMaturiMenu z:12];
        
        CCSprite *pict_thumb_frame3 = [CCSprite spriteWithSpriteFrameName:@"pict_thumb_frame.png"];
        pict_thumb_frame3.position = ccp(162, 183);
        [layer_maturi addChild:pict_thumb_frame3 z:20 tag:kTagYamaFrame];
        
        
        
        //矢作川
        layer_yahagi = [CCLayer node];
        layer_yahagi.anchorPoint = ccp(0.5f,0.5f);
        layer_yahagi.position = ccp(1024*3, 0);
        [layer_scroll addChild:layer_yahagi];
        
        CCSprite *yahagi_txt = [CCSprite spriteWithSpriteFrameName:@"txt_yahagigawa.png"];
        yahagi_txt.position = ccp(winsize.width-sanageyama_txt.contentSize.width/2-80,winsize.height-260);
        yahagi_txt.anchorPoint = ccp(0.5, 1.0);

        [layer_yahagi addChild:yahagi_txt];
        
        CCSprite *pict_frame4 = [CCSprite spriteWithSpriteFrameName:@"pict_photoFrame.png"];
        pict_frame4.position = ccp(273, 335);
        [layer_yahagi addChild:pict_frame4 z:11];
        
        CCSprite *pict_yahagi1 = [CCSprite spriteWithSpriteFrameName:@"pict_yahagi1.png"];
        pict_yahagi1.position = ccp(272, 390);
        [layer_yahagi addChild:pict_yahagi1 z:10 tag:kTagpictSanageyama1];
        
        CCSprite *pict_yahagi2 = [CCSprite spriteWithSpriteFrameName:@"pict_yahagi2.png"];
        pict_yahagi2.position = ccp(272, 390);
        pict_yahagi2.opacity = 0;
        [layer_yahagi addChild:pict_yahagi2 z:10 tag:kTagpictSanageyama2];
        
        CCSprite *pict_yahagi3 = [CCSprite spriteWithSpriteFrameName:@"pict_yahagi3.png"];
        pict_yahagi3.position = ccp(272, 390);
        pict_yahagi3.opacity = 0;
        [layer_yahagi addChild:pict_yahagi3 z:10 tag:kTagpictSanageyama3];
        
        
        //thumbnail btn
        CCSprite *btn_yahagi1 = [CCSprite spriteWithSpriteFrameName:@"pict_yahagi1_thumb.png"];
        CCSprite *btn_yahagi2 = [CCSprite spriteWithSpriteFrameName:@"pict_on_yahagi1_thumb.png"];
        
        CCSprite *btn_yahagi2_1 = [CCSprite spriteWithSpriteFrameName:@"pict_yahagi2_thumb.png"];
        CCSprite *btn_yahagi2_2 = [CCSprite spriteWithSpriteFrameName:@"pict_on_yahagi2_thumb.png"];
        
        CCSprite *btn_yahagi3_1 = [CCSprite spriteWithSpriteFrameName:@"pict_yahagi3_thumb.png"];
        CCSprite *btn_yahagi3_2 = [CCSprite spriteWithSpriteFrameName:@"pict_on_yahagi3_thumb.png"];
        
        CCMenuItemSprite * btnItem_yahagi1 = [CCMenuItemSprite itemWithNormalSprite:btn_yahagi1 selectedSprite:btn_yahagi2 target:self selector:@selector(photoChange:)];
        btnItem_yahagi1.position = ccp(-350, -200);
        btnItem_yahagi1.tag = 9;
        
        CCMenuItemSprite * btnItem_yahagi2 = [CCMenuItemSprite itemWithNormalSprite:btn_yahagi2_1 selectedSprite:btn_yahagi2_2 target:self selector:@selector(photoChange:)];
        btnItem_yahagi2.position = ccp(-240, -200);
        btnItem_yahagi2.tag = 10;
        
        
        CCMenuItemSprite * btnItem_yahagi3 = [CCMenuItemSprite itemWithNormalSprite:btn_yahagi3_1 selectedSprite:btn_yahagi3_2 target:self selector:@selector(photoChange:)];
        btnItem_yahagi3.position = ccp(-130, -200);
        btnItem_yahagi3.tag = 11;
        
        
        CCMenu *yahagiMenu = [CCMenu menuWithItems:btnItem_yahagi1,btnItem_yahagi2,btnItem_yahagi3,nil];
        
        [yahagiMenu setPosition: ccp(winsize.width/2, winsize.height/2)];
        [layer_yahagi addChild:yahagiMenu z:12];
        
        CCSprite *pict_thumb_frame4 = [CCSprite spriteWithSpriteFrameName:@"pict_thumb_frame.png"];
        pict_thumb_frame4.position = ccp(162, 183);
        [layer_yahagi addChild:pict_thumb_frame4 z:20 tag:kTagYamaFrame];
        
        
        
        
        //シラヒゲ
        layer_shirahige = [CCLayer node];
        layer_shirahige.anchorPoint = ccp(0.5f,0.5f);
        layer_shirahige.position = ccp(1024*4, 0);
        [layer_scroll addChild:layer_shirahige];
        
        CCSprite *shirahige_txt = [CCSprite spriteWithSpriteFrameName:@"txt_shirahige.png"];
        shirahige_txt.position = ccp(winsize.width-sanageyama_txt.contentSize.width/2-80,winsize.height-260);
        shirahige_txt.anchorPoint = ccp(0.5, 1.0);

        [layer_shirahige addChild:shirahige_txt];
        
        
        CCSprite *pict_frame5 = [CCSprite spriteWithSpriteFrameName:@"pict_photoFrame.png"];
        pict_frame5.position = ccp(273, 335);
        [layer_shirahige addChild:pict_frame5 z:11];
        
        CCSprite *pict_shirahige1 = [CCSprite spriteWithSpriteFrameName:@"pict_shirahige1.png"];
        pict_shirahige1.position = ccp(272, 390);
        [layer_shirahige addChild:pict_shirahige1 z:10 tag:kTagpictSanageyama1];
        
        CCSprite *pict_shirahige2 = [CCSprite spriteWithSpriteFrameName:@"pict_shirahige2.png"];
        pict_shirahige2.position = ccp(272, 390);
        pict_shirahige2.opacity = 0;
        [layer_shirahige addChild:pict_shirahige2 z:10 tag:kTagpictSanageyama2];
        
        
        //thumbnail btn
        CCSprite *btn_shirahige1 = [CCSprite spriteWithSpriteFrameName:@"pict_shirahige1_thumb.png"];
        CCSprite *btn_shirahige2 = [CCSprite spriteWithSpriteFrameName:@"pict_on_shirahige1_thumb.png"];
        
        CCSprite *btn_shirahige2_1 = [CCSprite spriteWithSpriteFrameName:@"pict_shirahige2_thumb.png"];
        CCSprite *btn_shirahige2_2 = [CCSprite spriteWithSpriteFrameName:@"pict_on_shirahige2_thumb.png"];
        
        
        CCMenuItemSprite * btnItem_shirahige1 = [CCMenuItemSprite itemWithNormalSprite:btn_shirahige1 selectedSprite:btn_shirahige2 target:self selector:@selector(photoChange:)];
        btnItem_shirahige1.position = ccp(-280, -200);
        btnItem_shirahige1.tag = 12;
        
        CCMenuItemSprite * btnItem_shirahige2 = [CCMenuItemSprite itemWithNormalSprite:btn_shirahige2_1 selectedSprite:btn_shirahige2_2 target:self selector:@selector(photoChange:)];
        btnItem_shirahige2.position = ccp(-180, -200);
        btnItem_shirahige2.tag = 13;
        
        
        CCMenu *shirahigeMenu = [CCMenu menuWithItems:btnItem_shirahige1,btnItem_shirahige2,nil];
        
        [shirahigeMenu setPosition: ccp(winsize.width/2, winsize.height/2)];
        [layer_shirahige addChild:shirahigeMenu z:12];
        
        CCSprite *pict_thumb_frame5 = [CCSprite spriteWithSpriteFrameName:@"pict_thumb_frame.png"];
        pict_thumb_frame5.position = ccp(231, 183);
        [layer_shirahige addChild:pict_thumb_frame5 z:20 tag:kTagYamaFrame];
        
        
        
        
        //リンク
        CCLayer *layer_link = [CCLayer node];
        layer_link.anchorPoint = ccp(0.5f,0.5f);
        layer_link.position = ccp(1024*5, 0);
        [layer_scroll addChild:layer_link];
        
        CCSprite *link_txt = [CCSprite spriteWithSpriteFrameName:@"txt_link.png"];
        link_txt.position = ccp(link_txt.contentSize.width/2+50,winsize.height-260);
        link_txt.anchorPoint = ccp(0.5, 1.0);
        [layer_link addChild:link_txt];
        
        //web button
        CCSprite *menu_button_text = [CCSprite spriteWithSpriteFrameName:@"btn_web.png"];
        CCSprite *menu_button_text2 = [CCSprite spriteWithSpriteFrameName:@"btn_web.png"];
        
        CCSprite *menu_button_text3 = [CCSprite spriteWithSpriteFrameName:@"btn_web.png"];
        CCSprite *menu_button_text4 = [CCSprite spriteWithSpriteFrameName:@"btn_web.png"];
        
        CCMenuItemSprite * linkMenuItem1 = [CCMenuItemSprite itemWithNormalSprite:menu_button_text selectedSprite:menu_button_text2 target:self selector:@selector(webView:)];
        linkMenuItem1.tag =1;
        linkMenuItem1.position = ccp(-410, -5);
        
        CCMenuItemSprite * linkMenuItem2 = [CCMenuItemSprite itemWithNormalSprite:menu_button_text3 selectedSprite:menu_button_text4 target:self selector:@selector(webView:)];
        linkMenuItem2.tag =2;
        linkMenuItem2.position = ccp(-410, -210);
        CCMenu *linkMenu = [CCMenu menuWithItems:linkMenuItem1,linkMenuItem2,nil];

        [linkMenu setPosition: ccp(winsize.width/2, winsize.height/2)];
        [layer_link addChild:linkMenu z:10];
        
        //マップボタン
        //web button
        CCSprite *map_button_text = [CCSprite spriteWithSpriteFrameName:@"btn_map.png"];
        CCSprite *map_button_text2 = [CCSprite spriteWithSpriteFrameName:@"btn_map.png"];
        
        CCSprite *map_button_text3 = [CCSprite spriteWithSpriteFrameName:@"btn_map.png"];
        CCSprite *map_button_text4 = [CCSprite spriteWithSpriteFrameName:@"btn_map.png"];
        
        CCMenuItemSprite * mapMenuItem1 = [CCMenuItemSprite itemWithNormalSprite:map_button_text selectedSprite:map_button_text2 target:self selector:@selector(mapView:)];
        mapMenuItem1.tag =1;
        mapMenuItem1.position = ccp(75, -240);
        CCMenu *mapMenu = [CCMenu menuWithItems:mapMenuItem1,nil];
        
        [mapMenu setPosition: ccp(winsize.width/2, winsize.height/2)];
        [layer_sanageyama addChild:mapMenu z:10 tag:kTagMapmenu1];
        
        CCMenuItemSprite * mapMenuItem2 = [CCMenuItemSprite itemWithNormalSprite:map_button_text3 selectedSprite:map_button_text4 target:self selector:@selector(mapView:)];
        mapMenuItem2.tag =2;
        mapMenuItem2.position = ccp(75, -240);
        CCMenu *mapMenu2 = [CCMenu menuWithItems:mapMenuItem2,nil];
        
        [mapMenu2 setPosition: ccp(winsize.width/2, winsize.height/2)];
        [layer_jinjya addChild:mapMenu2 z:10 tag:kTagMapmenu2];
        
        

    }
    return self;
}


-(CGRect)spriteTouch:(CCSprite *) sprite{
    float h = [sprite contentSize].height;
    float w = [sprite contentSize].width;
    float x = sprite.position.x-w/2;
    float y = sprite.position.y-h/2;
    CGRect rect = CGRectMake(x, y, w, h);
    return rect;
}

-(void)opacity{
    

    
    CCSprite *menu1_2 = (CCSprite*)[self getChildByTag:kTagMenu1_2];
    CCSprite *menu2_2 = (CCSprite*)[self getChildByTag:kTagtMenu2_2];
    CCSprite *menu3_2 = (CCSprite*)[self getChildByTag:kTagtMenu3_2];
    CCSprite *menu4_2 = (CCSprite*)[self getChildByTag:kTagtMenu4_2];
    CCSprite *menu5_2 = (CCSprite*)[self getChildByTag:kTagtMenu5_2];
    CCSprite *menu6_2 = (CCSprite*)[self getChildByTag:kTagtMenu6_2];
    
    menu1_2.opacity = 0;
    menu2_2.opacity = 0;
    menu3_2.opacity = 0;
    menu4_2.opacity = 0;
    menu5_2.opacity = 0;
    menu6_2.opacity = 0;
    

}

-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touchs  = [touches anyObject];
    CGPoint location = [touchs locationInView:[touchs view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    CCSprite *menu1 = (CCSprite*)[self getChildByTag:kTagMenu1];
    CCSprite *menu2 = (CCSprite*)[self getChildByTag:kTagtMenu2];
    CCSprite *menu3 = (CCSprite*)[self getChildByTag:kTagtMenu3];
    CCSprite *menu4 = (CCSprite*)[self getChildByTag:kTagtMenu4];
    CCSprite *menu5 = (CCSprite*)[self getChildByTag:kTagtMenu5];
    CCSprite *menu6 = (CCSprite*)[self getChildByTag:kTagtMenu6];
    
    CCSprite *menu1_2 = (CCSprite*)[self getChildByTag:kTagMenu1_2];
    CCSprite *menu2_2 = (CCSprite*)[self getChildByTag:kTagtMenu2_2];
    CCSprite *menu3_2 = (CCSprite*)[self getChildByTag:kTagtMenu3_2];
    CCSprite *menu4_2 = (CCSprite*)[self getChildByTag:kTagtMenu4_2];
    CCSprite *menu5_2 = (CCSprite*)[self getChildByTag:kTagtMenu5_2];
    CCSprite *menu6_2 = (CCSprite*)[self getChildByTag:kTagtMenu6_2];
    
    CCSprite *scroll = (CCSprite *)[self getChildByTag:kTagscroll];
    
    CGRect obj1 = [self spriteTouch:menu1];
    CGRect obj2 = [self spriteTouch:menu2];
    CGRect obj3 = [self spriteTouch:menu3];
    CGRect obj4 = [self spriteTouch:menu4];
    CGRect obj5 = [self spriteTouch:menu5];
    CGRect obj6 = [self spriteTouch:menu6];
    
    if(CGRectContainsPoint(obj1, location)){
        [self opacity];
        menu1_2.opacity = 255;
        
        id MoveTo = [CCMoveTo actionWithDuration:0.8 position:ccp(0, 0)];
        id ease = [CCEaseExponentialOut actionWithAction:MoveTo];
        [scroll runAction:ease];
        
    }else if(CGRectContainsPoint(obj2,location)){
        [self opacity];
        menu2_2.opacity = 255;
        
        id MoveTo = [CCMoveTo actionWithDuration:0.8 position:ccp(-1024, 0)];
        id ease = [CCEaseExponentialOut actionWithAction:MoveTo];
        [scroll runAction:ease];

        
    }else if(CGRectContainsPoint(obj3,location)){
        [self opacity];
        menu3_2.opacity = 255;
        
        id MoveTo = [CCMoveTo actionWithDuration:0.8 position:ccp(-1024*2, 0)];
        id ease = [CCEaseExponentialOut actionWithAction:MoveTo];
        [scroll runAction:ease];
        
    }else if(CGRectContainsPoint(obj4,location)){
        [self opacity];
        menu4_2.opacity = 255;
        
        id MoveTo = [CCMoveTo actionWithDuration:0.8 position:ccp(-1024*3, 0)];
        id ease = [CCEaseExponentialOut actionWithAction:MoveTo];
        [scroll runAction:ease];
    }else if(CGRectContainsPoint(obj5,location)){
        [self opacity];
        menu5_2.opacity = 255;
        
        id MoveTo = [CCMoveTo actionWithDuration:0.8 position:ccp(-1024*4, 0)];
        id ease = [CCEaseExponentialOut actionWithAction:MoveTo];
        [scroll runAction:ease];
    }else if(CGRectContainsPoint(obj6,location)){
        [self opacity];
        menu6_2.opacity = 255;
        
        id MoveTo = [CCMoveTo actionWithDuration:0.8 position:ccp(-1024*5, 0)];
        id ease = [CCEaseExponentialOut actionWithAction:MoveTo];
        [scroll runAction:ease];
    }
}

#pragma photo change
-(void)photoChange:(id)sender{
    CCLayer *layer = [CCLayer node];
    if([sender tag] < 4){
        layer = layer_sanageyama;
    }else if([sender tag] < 6){
        layer = layer_jinjya;
    }else if([sender tag] < 9){
        layer = layer_maturi;
    }else if([sender tag] < 12){
        layer = layer_yahagi;
    }else{
        layer = layer_shirahige;
    }
    
    CCSprite *sprite1 = (CCSprite *)[layer getChildByTag:kTagpictSanageyama1];
    CCSprite *sprite2 = (CCSprite *)[layer getChildByTag:kTagpictSanageyama2];
    CCSprite *sprite3 = (CCSprite *)[layer getChildByTag:kTagpictSanageyama3];
    CCSprite *spriteFrame = (CCSprite *)[layer getChildByTag:kTagYamaFrame];

    if([sender tag] == 1 || [sender tag] == 4 || [sender tag] == 6 || [sender tag] == 9 || [sender tag] == 12){
        id fade = [CCFadeTo actionWithDuration:0.5 opacity:255];
        id fade2 = [CCFadeTo actionWithDuration:0.5 opacity:0];
        id fade3 = [CCFadeTo actionWithDuration:0.5 opacity:0];
        
        if([sender tag] == 4 || [sender tag] == 12){
            spriteFrame.position =  ccp(232, 183);
        }else{
            spriteFrame.position =  ccp(162, 183);
        }
        
        [sprite1 runAction:fade];
        [sprite2 runAction:fade2];
        [sprite3 runAction:fade3];
    }else if([sender tag] == 2 || [sender tag] == 5 || [sender tag] == 7 || [sender tag] == 10 || [sender tag] == 13){
        id fade = [CCFadeTo actionWithDuration:0.5 opacity:0];
        id fade2 = [CCFadeTo actionWithDuration:0.5 opacity:255];
        id fade3 = [CCFadeTo actionWithDuration:0.5 opacity:0];
        if([sender tag] == 5 || [sender tag] == 13){
            spriteFrame.position =  ccp(331, 183);
        }else{
            spriteFrame.position =  ccp(272, 183);
        }
        [sprite1 runAction:fade];
        [sprite2 runAction:fade2];
        [sprite3 runAction:fade3];
    }else if([sender tag] == 3 || [sender tag] == 8 || [sender tag] == 11){
        id fade = [CCFadeTo actionWithDuration:0.5 opacity:0];
        id fade2 = [CCFadeTo actionWithDuration:0.5 opacity:0];
        id fade3 = [CCFadeTo actionWithDuration:0.5 opacity:255];
        spriteFrame.position =  ccp(382, 183);
        [sprite1 runAction:fade];
        [sprite2 runAction:fade2];
        [sprite3 runAction:fade3];
    }
}

#pragma mark web view
-(void)webView:(id)sender{
    NSString *path = [[NSString alloc] init];
    if ( [sender tag] == 1) {
        path = [[NSString alloc] initWithFormat:@"http://www.citytoyota-kankou-jp.org/"];

    }else{
        path = [[NSString alloc] initWithFormat:@"http://decasu.jp/"];
    }
    
    
    NSURL *url = [NSURL URLWithString:path];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    
    webView_  = [[UIWebView alloc] initWithFrame:CGRectMake(0, 46, 1024, 768)];
    webView_.bounds = CGRectMake(0, 46, 1024, 768);
    
    webView_.scalesPageToFit = YES;
    
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 1024, 46)];
    toolBar.translucent = NO;
    //toolBar.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
    
    //インジケーターの追加
    activityInducator_ = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityInducator_.frame = CGRectMake(0, 0, 20, 20);
    UIBarButtonItem *inducator = [[UIBarButtonItem alloc] initWithCustomView:activityInducator_];
    UIBarButtonItem *adjustment =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    //戻るボタン
    UIBarButtonItem *goBackButton = [[UIBarButtonItem alloc]init];
    goBackButton.title = @"閉じる";
    goBackButton.style = UIBarButtonItemStyleBordered;
    goBackButton.target = self;
    goBackButton.action = @selector(goBack);
    
    NSArray *elements = [[NSArray alloc] initWithObjects:goBackButton,adjustment,inducator,nil];
    [adjustment release];
    [inducator release];
    [goBackButton release];
    adjustment = nil;
    inducator = nil;
    goBackButton = nil;
    [toolBar setItems:elements animated:YES];
    [elements release];
    elements = nil;
    [path release];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                           forView:[[CCDirector sharedDirector] view] cache:YES];
    webView_.delegate =self;
    [[[CCDirector sharedDirector] view] addSubview:webView_];
    [[[CCDirector sharedDirector] view] addSubview:toolBar];
    
    [UIView commitAnimations];
    [webView_ loadRequest:req];

}
#pragma Map View
-(void)mapView:(id)sender{
    CCLOG(@"mapview");
    self.isTouchEnabled = NO;
    
    if([sender tag] == 1){
        CCMenu *menu1 = (CCMenu *)[layer_sanageyama getChildByTag:kTagMapmenu1];
        menu1.isTouchEnabled = NO;
        
        destination.latitude = 35.205902;
        destination.longitude = 137.167143;
    }else{
        CCMenu *menu1 = (CCMenu *)[layer_jinjya getChildByTag:kTagMapmenu2];
        menu1.isTouchEnabled = NO;
        
        destination.latitude = 35.175282;
        destination.longitude = 137.177461;

    }
    mv = [[MKMapView alloc] init];
    mv.frame = 	CGRectMake(0,0,1024,768);
    mv.bounds = CGRectMake(0, 0, 1024, 768);
    mv.backgroundColor = [UIColor whiteColor];
    [mv setShowsUserLocation:NO];

    [mv setCenterCoordinate:destination animated:NO];
    
    // 縮尺を指定
    MKCoordinateRegion cr = mv.region;
    cr.center = destination;
    cr.span.latitudeDelta = 0.2;
    cr.span.longitudeDelta = 0.2;
    [mv setRegion:cr animated:NO];
    
    //ピンを立てる
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = destination ;
    [mv addAnnotation:annotation];
    
    //ツールバー
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 1024, 46)];
    toolBar.translucent = NO;
    
    //戻るボタン
    UIBarButtonItem *goBackButton = [[UIBarButtonItem alloc]init];
    goBackButton.title = @"閉じる";
    goBackButton.style = UIBarButtonItemStyleBordered;
    goBackButton.target = self;
    goBackButton.action = @selector(goBackMap);
    
    NSArray *elements = [[NSArray alloc] initWithObjects:goBackButton,nil];
    [goBackButton release];
    goBackButton = nil;
    [toolBar setItems:elements animated:YES];
    [elements release];
    elements = nil;
    
    mv.delegate =self;
    
    // addSubview
    [[[CCDirector sharedDirector] view] addSubview:mv];
    [[[CCDirector sharedDirector] view] addSubview:toolBar];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                           forView:[[CCDirector sharedDirector] view] cache:YES];
    [UIView commitAnimations];
    

}

-(void)mapViewDidFinishLoadingMap:(MKMapView *)mapView{


}

-(void)webViewDidStartLoad:webView_{
    [activityInducator_ startAnimating];
}

-(void)webViewDidFinishLoad:webView_{
    [activityInducator_ stopAnimating];
}

//webViewを閉じる-------------------------------------------------------------------
- (void)goBack {

	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                           forView:[[CCDirector sharedDirector] view] cache:YES];
    [webView_ removeFromSuperview];
    [toolBar removeFromSuperview];
    [UIView commitAnimations];
}

- (void)goBackMap {
    layer_jinjya.isTouchEnabled = YES;
    layer_sanageyama.isTouchEnabled = YES;
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                           forView:[[CCDirector sharedDirector] view] cache:YES];
    [mv removeFromSuperview];
    [toolBar removeFromSuperview];
    [UIView setAnimationDelegate:self]; 
    [UIView setAnimationDidStopSelector:@selector(goBackMap2)];

    [UIView commitAnimations];
}

-(void)goBackMap2{
    self.isTouchEnabled = YES;
    CCMenu *menu1 = (CCMenu *)[layer_sanageyama getChildByTag:kTagMapmenu1];
    menu1.isTouchEnabled = YES;
    
    CCMenu *menu2 = (CCMenu *)[layer_jinjya getChildByTag:kTagMapmenu2];
    menu2.isTouchEnabled = YES;
}

-(void)dealloc{
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    
    [webView_ release];
    [activityInducator_ release];
    [mv release];
    [super dealloc];
}

@end
