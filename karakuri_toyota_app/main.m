//
//  main.m
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 13/02/05.
//  Copyright __MyCompanyName__ 2013年. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
    int retVal;
    @try {
        retVal = UIApplicationMain(argc, argv, nil, @"AppController");
    }
    @catch (NSException *exception) {
        NSLog(@"%@", [exception callStackSymbols]); //< ★1
        @throw exception; //< ★2
    }
    @finally {
        [pool release];
    }
    return retVal;
}

