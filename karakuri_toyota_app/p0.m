//
//  p0.m
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/08/06.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "p0.h"


@implementation p0

CCSprite *murabito1;
CCSprite *murabito2;
CCMenuItemSprite *menuStart;
CGSize winSize;
int settingOpacity = 32;


-(id)init{
    
    if( (self=[super init])) {
        
        self.ignoreAnchorPointForPosition = NO;
        self.isTouchEnabled = YES;
        [SoundEffect seLoad];
        
        winSize = [[CCDirector sharedDirector] winSize];
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"start.plist"];
        
        CCSpriteBatchNode *bachNode = [CCSpriteBatchNode batchNodeWithFile:@"start.pvr.gz"];
        [self addChild:bachNode];
        
        //設定内容
        NSUserDefaults *userDefault =[NSUserDefaults standardUserDefaults];
        setting_text = [userDefault integerForKey:@"SETTING1"];
        setting_narra = [userDefault integerForKey:@"SETTING2"];
        setting_effect = [userDefault integerForKey:@"SETTING3"];
        CCLOG(@"setting_text%d",setting_text);
        CCLOG(@"setting_narra%d",setting_narra);
        CCLOG(@"setting_effect%d",setting_effect);
        
        //表紙のボタン
        startButton = [CCSprite spriteWithSpriteFrameName:@"start_button.png"];
        startButton_selected = [CCSprite spriteWithSpriteFrameName:@"start_button.png"];
        
        startButton.position = ccp(winSize.width/2, winSize.height/3);
        startButton_selected.position = ccp(winSize.width/2, winSize.height/3);
        startButton_selected.visible = true;

        [self addChild:startButton_selected z:5];
        [self addChild:startButton z:5];
        
        setteiButton = [CCSprite spriteWithSpriteFrameName:@"settei_button.png"];
        setteiButton_selected = [CCSprite spriteWithSpriteFrameName:@"settei_button.png"];
        
        setteiButton.position = ccp(winSize.width/2, winSize.height/6);
        [self addChild:setteiButton z:5];
        


        //設定の背景
        settingBackground = [CCSprite spriteWithSpriteFrameName:@"setting_bg.png"];
        settingBackground.position = ccp(winSize.width/2, winSize.height/2);
        settingBackground.opacity = 0;
        [self addChild:settingBackground z:3];
        
        settingBackground2 = [CCSprite spriteWithSpriteFrameName:@"setting_bg2.png"];
        settingBackground2.position = ccp(winSize.width/2, winSize.height+settingBackground2.contentSize.height/2);
        [self addChild:settingBackground2 z:10];
        
        settingBackground3 = [CCSprite spriteWithSpriteFrameName:@"setting_bg2.png"];
        settingBackground3.position = ccp(winSize.width/2, -winSize.height+settingBackground2.contentSize.height/2);
        [self addChild:settingBackground3 z:11];
        
        
        //設定のボタン群
        settei_btn1 = [CCSprite spriteWithSpriteFrameName:@"settei_button1.png"];
        settei_btn2 = [CCSprite spriteWithSpriteFrameName:@"settei_button2.png"];
        settei_btn3 = [CCSprite spriteWithSpriteFrameName:@"settei_button3.png"];
        settei_btn4 = [CCSprite spriteWithSpriteFrameName:@"settei_button4.png"];
        settei_btn5 = [CCSprite spriteWithSpriteFrameName:@"settei_button5.png"];
        settei_btn6 = [CCSprite spriteWithSpriteFrameName:@"settei_button6.png"];
        settei_btn7 = [CCSprite spriteWithSpriteFrameName:@"settei_button7.png"];
        settei_close = [CCSprite spriteWithSpriteFrameName:@"settei_close_btn.png"];
        settei_close2 = [CCSprite spriteWithSpriteFrameName:@"settei_close_btn2.png"];
        settei_close3 = [CCSprite spriteWithSpriteFrameName:@"settei_close_btn.png"];
        settei_close4 = [CCSprite spriteWithSpriteFrameName:@"settei_close_btn2.png"];
        
        int marginWidth = 130;
        int marginHeight = 290;
        
        settei_btn1.position =ccp(marginWidth,marginHeight);
        settei_btn2.position =ccp(marginWidth*2,marginHeight);
        settei_btn3.position =ccp(marginWidth*3,marginHeight);
        if (setting_text) {
            settei_btn1.opacity =settingOpacity;
        }else{
            settei_btn1.opacity =255;
        }
        
        if (setting_narra) {
            settei_btn2.opacity =settingOpacity;
        }else{
            settei_btn2.opacity =255;
        }
        
        if (setting_effect) {
            settei_btn3.opacity =settingOpacity;
        }else{
            settei_btn3.opacity =255;
        }

        [settingBackground2 addChild:settei_btn1];
        [settingBackground2 addChild:settei_btn2];
        [settingBackground2 addChild:settei_btn3];

        
        settei_btn4.position =ccp(marginWidth*4,marginHeight);
        [settingBackground2 addChild:settei_btn4];
        
        settei_btn5.position =ccp(marginWidth,marginHeight-settei_btn1.contentSize.height-30);
        [settingBackground2 addChild:settei_btn5];
        
        settei_btn6.position =ccp(marginWidth*2,marginHeight-settei_btn1.contentSize.height-30);
        [settingBackground2 addChild:settei_btn6];
        
        settei_btn7.position =ccp(marginWidth*3,marginHeight-settei_btn1.contentSize.height-30);
        [settingBackground2 addChild:settei_btn7];
        
        settei_close.position =ccp(settingBackground2.contentSize.width-10,settingBackground2.contentSize.height-10);
        [settingBackground2 addChild:settei_close];
        
        settei_close2.position =ccp(settingBackground2.contentSize.width-10,settingBackground2.contentSize.height-10);
        settei_close2.opacity = 0;
        [settingBackground2 addChild:settei_close2];
        
        settei_close3.position =ccp(settingBackground3.contentSize.width-10,settingBackground3.contentSize.height-10);
        [settingBackground3 addChild:settei_close3];
        
        settei_close4.position =ccp(settingBackground3.contentSize.width-10,settingBackground3.contentSize.height-50);
        settei_close4.opacity = 0;
        [settingBackground3 addChild:settei_close4];
        
        //setting title
        CCSprite *settingTitle = [CCSprite spriteWithSpriteFrameName:@"setting_title.png"];
        settingTitle.position = ccp(settingBackground2.contentSize.width/2,settingBackground2.contentSize.height-45);
        [settingBackground2 addChild:settingTitle];
        
        CCSprite *tukaikataTitle = [CCSprite spriteWithSpriteFrameName:@"setting_tukaikata.png"];
        tukaikataTitle.position = ccp(settingBackground3.contentSize.width/2,settingBackground2.contentSize.height-45);
        [settingBackground3 addChild:tukaikataTitle];
        
        //howto
        CCSprite *setteiHowto = [CCSprite spriteWithSpriteFrameName:@"setumei_how_to1.png"];
        setteiHowto.position = ccp(setteiHowto.contentSize.width/2+20, setteiHowto.contentSize.height/2+20);
        [settingBackground3 addChild:setteiHowto];
        
        
    }
    return self;
}


-(void)touchDown:(int)sender{
    id move = [CCMoveBy actionWithDuration:0.5 position:ccp(0,60)];
    id ease = [CCEaseElasticOut actionWithAction:move];
    
    id move2 = [CCMoveBy actionWithDuration:0.6 position:ccp(0,60)];
    id ease2 = [CCEaseElasticOut actionWithAction:move2];
    
    id move3 = [CCMoveBy actionWithDuration:0.7 position:ccp(0,60)];
    id ease3 = [CCEaseElasticOut actionWithAction:move3];
    
    id fade = [CCFadeIn actionWithDuration:0.2];
    id move4 = [CCMoveTo actionWithDuration:0.4 position:ccp(winSize.width/2,winSize.height/2)];
    id ease4 = [CCEaseBackOut actionWithAction:move4];
    
    id fadeOut = [CCFadeOut actionWithDuration:0.2];
    id move5 = [CCMoveTo actionWithDuration:0.4 position:ccp(winSize.width/2,winSize.height+settingBackground2.contentSize.height/2)];
    id ease5 = [CCEaseBackIn actionWithAction:move5];
    
    id move6 = [CCMoveTo actionWithDuration:0.4 position:ccp(winSize.width/2,winSize.height+settingBackground2.contentSize.height/2)];
    id ease6 = [CCEaseExponentialOut actionWithAction:move6];
    
    id move7 = [CCMoveTo actionWithDuration:0.4 position:ccp(winSize.width/2,winSize.height/2)];
    id ease7 = [CCEaseExponentialOut actionWithAction:move7];
    
    id move8 = [CCMoveTo actionWithDuration:0.4 position:ccp(winSize.width/2,-winSize.height+settingBackground2.contentSize.height/2)];
    id ease8 = [CCEaseExponentialOut actionWithAction:move8];
    
    id func = [CCCallBlock actionWithBlock:^{
        [self nextPage:nil];
    }];
    id func2 = [CCCallBlock actionWithBlock:^{
        tochh_enable = false;
    }];

    id seq = [CCSequence actions:ease3,func,nil];
    id seq2 = [CCSequence actions:ease4,func2,nil];
    id seq3 = [CCSequence actions:ease5,func2,nil];
    id seq4 = [CCSequence actions:ease6,func2,nil];
    id seq5 = [CCSequence actions:ease7,func2,nil];
    id seq6 = [CCSequence actions:ease8,func2,nil];
    
    if (!tochh_enable) {
    switch (sender) {
        case 1://はじめるボタン
            if(!touch_setting){
                tochh_enable = true;
                touch_start = true;
                murabito1 = [CCSprite spriteWithSpriteFrameName:@"start_murabito1.png"];
                murabito1.position = ccp(winSize.width/2-80, winSize.height/3);
                murabito2 = [CCSprite spriteWithSpriteFrameName:@"start_murabito2.png"];
                murabito2.position = ccp(winSize.width/2, winSize.height/3);
                murabito3 = [CCSprite spriteWithSpriteFrameName:@"start_murabito3.png"];
                murabito3.position = ccp(winSize.width/2+80, winSize.height/3);
                [self addChild:murabito1];
                [self addChild:murabito2];
                [self addChild:murabito3];
                [murabito1 runAction:ease];
                [murabito2 runAction:ease2];
                [murabito3 runAction:seq];
            }
            break;
        case 2://設定ボタン
            if(!touch_start && !touch_setting){
                tochh_enable = true;
                touch_setting = true;
                [settingBackground runAction:fade];
                [settingBackground2 runAction:seq2];
            }
            break;
        case 3://設定のクローズボタン
            tochh_enable = true;
            touch_setting = false;
            [settingBackground runAction:fadeOut];
            [settingBackground2 runAction:seq3];
            break;
        case 4://使い方の遷移
            tochh_enable = true;
            touch_howto = true;
            [settingBackground2 runAction:seq4];
            [settingBackground3 runAction:seq5];
            break;
        case 5://クレジット
            [self creditPage:nil];

            break;
        case 6://ゆかり後
            [self yukariPage:nil];

            break;
        case 7://使い方のクローズボタン
            tochh_enable = true;
            touch_howto = false;
            touch_setting = true;
            
            [settingBackground2 runAction:seq5];
            [settingBackground3 runAction:seq6];
            break;
        default:
            break;
        }
    }
}

//テキスト、ナレーション、効果音のスイッチ
-(void)touchDown2:(int)sender{
    NSUserDefaults *userDefault =[NSUserDefaults standardUserDefaults];
    switch (sender) {
        case 1:
            if(!setting_text){
                settei_btn1.opacity = settingOpacity;
                setting_text = 1;
                [userDefault setInteger:setting_text forKey:@"SETTING1"];
            }else{
                settei_btn1.opacity = 255;
                NSUserDefaults *userDefault =[NSUserDefaults standardUserDefaults];
                setting_text = 0;
                [userDefault setInteger:setting_text forKey:@"SETTING1"];
            }
            break;
        case 2:
            if(!setting_narra){
                settei_btn2.opacity = settingOpacity;
                setting_narra = 1;
                [userDefault setInteger:setting_narra forKey:@"SETTING2"];
            }else{
                settei_btn2.opacity = 255;
                NSUserDefaults *userDefault =[NSUserDefaults standardUserDefaults];
                setting_narra = 0;
                [userDefault setInteger:setting_narra forKey:@"SETTING2"];
            }
            break;
        case 3:
            if(!setting_effect){
                settei_btn3.opacity = settingOpacity;
                setting_effect = 1;
                [userDefault setInteger:setting_effect forKey:@"SETTING3"];
            }else{
                settei_btn3.opacity = 255;
                NSUserDefaults *userDefault =[NSUserDefaults standardUserDefaults];
                setting_effect = 0;
                [userDefault setInteger:setting_effect forKey:@"SETTING3"];
            }
            break;
        default:
            break;
    }
}
#pragma mark イベント処理
- (void)nextPage:(id)sender {
    CCScene *newScene = [SceneManager nextSceneOfScene:self.parent choice:kSceneChoiseA nextBool:true];
    [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.5];
}

- (void)prePage:(id)sender {
    CCScene *newScene = [SceneManager nextSceneOfScene:self.parent choice:kSceneChoiseB nextBool:false];
    [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageBackward duration:0.5];
}

- (void)creditPage:(id)sender {
    CCScene *newScene = [SceneManager nextSceneOfScene:self.parent choice:kSceneChoiseD nextBool:false];
    [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.5];
}

- (void)yukariPage:(id)sender {
    CCScene *newScene = [SceneManager nextSceneOfScene:self.parent choice:kSceneChoiseE nextBool:false];
    [CCTransitionHelper replaceScene:newScene transitionType:TransitionTypePageForward duration:0.5];
}

#pragma mark タッチイベント処理

-(CGRect)spriteTouch:(CCSprite *) sprite{
    float h = [sprite contentSize].height;
    float w = [sprite contentSize].width;
    float x = sprite.position.x-w/2;
    float y = sprite.position.y-h/2;
    CGRect rect = CGRectMake(x, y, w, h);
    return rect;
}

-(CGRect)spriteTouch2:(CCSprite *) sprite{
    float h = [sprite contentSize].height;
    float w = [sprite contentSize].width;
    float x = (winSize.width/2-[settingBackground2 contentSize].width/2)+sprite.position.x-w/2;
    float y = (winSize.height/2-[settingBackground2 contentSize].height/2)+sprite.position.y-h/2;
    CGRect rect = CGRectMake(x, y, w, h);

    return rect;
}


-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touchs  = [touches anyObject];
    CGPoint location = [touchs locationInView:[touchs view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    CGRect object1Rect = [self spriteTouch:startButton];
    CGRect object2Rect = [self spriteTouch:setteiButton];
    CGRect object3Rect = [self spriteTouch2:settei_close];
    CGRect object4Rect = [self spriteTouch2:settei_btn1];
    CGRect object5Rect = [self spriteTouch2:settei_btn2];
    CGRect object6Rect = [self spriteTouch2:settei_btn3];
    CGRect object7Rect = [self spriteTouch2:settei_btn4];
    CGRect object8Rect = [self spriteTouch2:settei_btn5];
    CGRect object9Rect = [self spriteTouch2:settei_btn6];
    CGRect object10Rect = [self spriteTouch2:settei_btn7];
    CGRect object11Rect = [self spriteTouch2:settei_close3];
    
    //スタートボタン
    if (!touch_howto) {
        
        if(CGRectContainsPoint(object1Rect, location)){
            [self touchDown:1];
        }
        //設定ボタン
        if(CGRectContainsPoint(object2Rect, location)){
            setteiButton.opacity = 180;
            [self touchDown:2];
        }
        //設定ページクローズボタン
        if(CGRectContainsPoint(object3Rect, location) && touch_setting && !touch_start && !touch_howto){
            [self touchDown:3];
        }
        //テキストボタン
        if(CGRectContainsPoint(object4Rect, location)){
            [self touchDown2:1];
        }
        //ナレーション
        if(CGRectContainsPoint(object5Rect, location)){
            [self touchDown2:2];
        }
        //効果音
        if(CGRectContainsPoint(object6Rect, location)){
            [self touchDown2:3];
        }
        //使い方
        if(CGRectContainsPoint(object7Rect, location) && touch_setting && !touch_start){
            [self touchDown:4];
        }
        //クレジット
        if(CGRectContainsPoint(object8Rect, location) && touch_setting && !touch_start){
            [self touchDown:5];
        }
        //ゆかりの地
        if(CGRectContainsPoint(object9Rect, location) && touch_setting && !touch_start){
            [self touchDown:6];
        }
        //ホームページ
        if(CGRectContainsPoint(object10Rect, location) && touch_setting && !touch_start){
            settei_btn7.opacity = 64;
            [self webSite];
        }
    }
    
    //使い方のクローズボタン
    if(CGRectContainsPoint(object11Rect, location) && touch_howto && !touch_start){
        [self touchDown:7];
    }
}


- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    setteiButton.opacity = 255;
    settei_btn7.opacity = 255;
    
}


//Webview
-(void)webSite{
    NSString *path =[[NSString alloc] initWithFormat:@"http://karakuri-books.com"];
    NSURL *url = [NSURL URLWithString:path];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    
    webView_  = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 1024, 768)];
    webView_.bounds = CGRectMake(0, 46, 1024, 768);
    webView_.scalesPageToFit = YES;
    
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 1024, 46)];
    toolBar.translucent = NO;

    
    //インジケーターの追加
    activityInducator_ = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityInducator_.frame = CGRectMake(0, 0, 20, 20);
    UIBarButtonItem *inducator = [[UIBarButtonItem alloc] initWithCustomView:activityInducator_];
    UIBarButtonItem *adjustment =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    //戻るボタン
    UIBarButtonItem *goBackButton = [[UIBarButtonItem alloc]init];
    goBackButton.title = @"閉じる";
    goBackButton.style = UIBarButtonItemStyleBordered;
    goBackButton.target = self;
    goBackButton.action = @selector(goBack);
    
    NSArray *elements = [[NSArray alloc] initWithObjects:goBackButton,adjustment,inducator,nil];
    [adjustment release];
    [inducator release];
    [goBackButton release];
    adjustment = nil;
    inducator = nil;
    goBackButton = nil;
    [toolBar setItems:elements animated:YES];
    [elements release];
    elements = nil;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                           forView:[[CCDirector sharedDirector] view] cache:YES];
    webView_.delegate =self;
    [[[CCDirector sharedDirector] view] addSubview:webView_];
    [[[CCDirector sharedDirector] view] addSubview:toolBar];
    
    [UIView commitAnimations];
    [webView_ loadRequest:req];

    
}

-(void)webViewDidStartLoad:webView_{
    [activityInducator_ startAnimating];
}

-(void)webViewDidFinishLoad:webView_{
    [activityInducator_ stopAnimating];
}

//webViewを閉じる-------------------------------------------------------------------
- (void)goBack {
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                           forView:[[CCDirector sharedDirector] view] cache:YES];
    [webView_ removeFromSuperview];
    [toolBar removeFromSuperview];
    [UIView commitAnimations];
}


- (void) dealloc
{
    
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
	[super dealloc];
}


@end
