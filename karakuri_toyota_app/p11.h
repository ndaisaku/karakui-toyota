//
//  p11.h
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//
#import <GameKit/GameKit.h>
#import "cocos2d.h"
#import "Box2D.h"
#import "GLES-Render.h"
#import "SoundEffect.h"

//Pixel to metres ratio. Box2D uses metres as the unit for measurement.
//This ratio defines how many pixels correspond to 1 Box2D "metre"
//Box2D is optimized for objects of 1x1 metre therefore it makes sense
//to define the ratio so that your most common object type is 1x1 metre.
#define PTM_RATIO 32

@interface p11 : CCLayer{
    CCSprite *kago;
    CCSprite *usagi;
    CCSprite *funeButton;
    CCLayer *p11Kumo1;
    CCLayer *p11Kumo2;
    CCLayer *p11Kumo3;
    CGSize winsize;
    
    bool kagoHidden;
    bool funeHidden;
    bool hebiHidden;
     CCTexture2D *spriteTexture_;	// weak ref
     CCTexture2D *spriteTexture2_;	// weak ref
     b2World* world;					// strong ref
     GLESDebugDraw *m_debugDraw;		// strong ref
     b2BodyDef bodyDef;
     b2MouseJoint *_mouseJoint;
     b2Body* groundBody;
     b2BuoyancyController *bc;
    CCSpriteBatchNode *bachNood;
    CCSprite *mount;
    
    CCSprite *cloud1;
}
+(CCScene *) scene;
@end
