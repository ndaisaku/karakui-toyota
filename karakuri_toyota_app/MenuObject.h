//
//  MenuObject.h
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/06/06.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CCTransitionHelper.h"

@interface MenuObject : CCNode {
    CCSprite *spriteBack;
    bool helpHidden;
    bool mojiHidden;
    
    CCSprite *helpBg;
    CCSprite *textImage;
    CCSprite *textBg;
    
    CCSprite *spriteImage[10];
    CCSprite *alertBackground;
    CCSprite *alertBox;
    
    
    bool p5_count_left;
    bool p5_count_right;
    bool p5_count_top;
    bool alert_hajime;
}

+(void)Pagenum:(int)pagenum;


@end
