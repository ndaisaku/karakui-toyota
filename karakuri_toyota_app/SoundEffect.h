//
//  SoundEffect.h
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/10/27.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SimpleAudioEngine.h"

@interface SoundEffect : NSObject {
    ALuint se;
}
+(void)seLoad;
+(void)sePlay:(NSString *)soundName;
@end
