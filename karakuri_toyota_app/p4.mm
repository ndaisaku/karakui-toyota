//
//  p4.mm
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 13/02/05.
//  Copyright __MyCompanyName__ 2013年. All rights reserved.
//

// Import the interfaces
#import "p4.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#import "PhysicsSprite.h"
#import "Box2DExamples.h"
#import "GB2ShapeCache.h"

enum {
	kTagParentNode = 1,
	kTagParentNode2 = 2,
    kTagParentNode3 = 3,
    kTagParentNode4 = 4,
};

#define BUOYANCYOFFSET 90.0f

#pragma mark - p4

@interface p4()
-(void) initPhysics;
-(void) addNewSpriteAtPosition:(CGPoint)p;
@end

@implementation p4

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	p4 *layer = [p4 node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
	if( (self=[super init])) {
        self.ignoreAnchorPointForPosition = NO;		
		self.isTouchEnabled = YES;
		self.isAccelerometerEnabled = NO;
        
        
        //CGSize s = [CCDirector sharedDirector].winSize;
		
		// init physics
		[self initPhysics];
        
        

		
		//Set up sprite
		
#if 1
		// Use batch node. Faster
		CCSpriteBatchNode *parent = [CCSpriteBatchNode batchNodeWithFile:@"blocks2.png" capacity:100];
		spriteTexture_ = [parent texture];
        
        CCSpriteFrameCache *frameChache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameChache addSpriteFramesWithFile:@"p4_hana.plist"];
        CCSpriteBatchNode *parent2 = [CCSpriteBatchNode batchNodeWithFile:@"p4_hana.pvr.gz"];
        spriteTexture2_ = [parent2 texture];
        
#else
		// doesn't use batch node. Slower
		spriteTexture_ = [[CCTextureCache sharedTextureCache] addImage:@"blocks2.png"];
		CCNode *parent = [CCNode node];
        
        spriteTexture2_ = [[CCTextureCache sharedTextureCache] addImage:@"p4_hana.pvr.gz"];
		CCNode *parent2 = [CCNode node];
        
#endif
		[self addChild:parent z:0 tag:kTagParentNode];
        [self addChild:parent2 z:0 tag:kTagParentNode2];

		for (int i=0; i<120; i++) {
            [self addNewSpriteAtPosition:ccp(CCRANDOM_0_1()*1000, CCRANDOM_0_1()*150)];
            
        }
        
		
		[self scheduleUpdate];
        [self addFlowerSprite];
	}
	return self;
}

-(void) dealloc
{
    
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    
	delete world;
	world = NULL;
	
	delete m_debugDraw;
	m_debugDraw = NULL;
	
	[super dealloc];
}

-(void) initPhysics
{
	
	CGSize s = [[CCDirector sharedDirector] winSize];
	
	b2Vec2 gravity;
	gravity.Set(0.0f, -2.0f);
    bool doSleep = false;
    
	world = new b2World(gravity,doSleep);
	
	
	// Do we want to let bodies sleep?
	
	world->SetContinuousPhysics(true);
    
    /*
	m_debugDraw = new GLESDebugDraw( PTM_RATIO );
	world->SetDebugDraw(m_debugDraw);
	uint32 flags = 0;
	flags += b2Draw::e_shapeBit;
    flags += b2Draw::e_jointBit;
    flags += b2Draw::e_aabbBit;
    flags += b2Draw::e_pairBit;
    flags += b2Draw::e_centerOfMassBit;
    flags += b2Draw::e_controllerBit;
    
	m_debugDraw->SetFlags(flags);
    */

	
	// Define the ground body.
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0, 0); // bottom-left corner
	
	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	groundBody = world->CreateBody(&groundBodyDef);
	
	// Define the ground box shape.
	b2EdgeShape groundBox;
	
	// bottom
	
	groundBox.Set(b2Vec2(0,-100/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,-100/PTM_RATIO));
	groundBody->CreateFixture(&groundBox,0);
	
	// top
	groundBox.Set(b2Vec2(0,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO));
	groundBody->CreateFixture(&groundBox,0);
	
	// left
	groundBox.Set(b2Vec2(-10,s.height/PTM_RATIO), b2Vec2(-10,0));
	groundBody->CreateFixture(&groundBox,0);
	
	// right
	groundBox.Set(b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,-100/PTM_RATIO));
	groundBody->CreateFixture(&groundBox,0);
    
    //船のポリゴンシェイプ
    b2BodyDef shipBody;
	shipBody.position.Set(0, 0);
	b2Body *body1 = world->CreateBody(&shipBody);
    [[GB2ShapeCache sharedShapeCache] addShapesWithFile:@"p4_ship_polygon.plist"];
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:body1 forShapeName:@"p4_ship"];
    

}

-(void) draw
{
	//
	// IMPORTANT:
	// This is only for debug purposes
	// It is recommend to disable it
	//
	[super draw];
	
	ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
	
	kmGLPushMatrix();
	
	world->DrawDebugData();
	
	kmGLPopMatrix();
}
-(void) addFlowerSprite{
    
    float xpos = 16.0;
    float ypos = 9.0;
    float ypos2 = 9.0+0.3;
	
    
    b2Body* ground = NULL;
    {
        b2BodyDef bd;
        ground = world->CreateBody(&bd);
        
        b2EdgeShape shape;
        shape.Set(b2Vec2(-0.0f, 0.0f), b2Vec2(0.0, 0.0));
        ground->CreateFixture(&shape, 0.0f);
    }
    b2Body* ground2 = NULL;
    {
        b2BodyDef bd2;
        ground2 = world->CreateBody(&bd2);
        
        b2EdgeShape shape;
        shape.Set(b2Vec2(-0.0f, 0.0f), b2Vec2(0.0, 0.0));
        ground2->CreateFixture(&shape, 0.0f);
    }
    
    b2Body* ground3 = NULL;
    {
        b2BodyDef bd3;
        ground3 = world->CreateBody(&bd3);
        
        b2EdgeShape shape;
        shape.Set(b2Vec2(-0.0f, 0.0f), b2Vec2(0.0, 0.0));
        ground3->CreateFixture(&shape, 0.0f);
    }

    b2Body* ground4 = NULL;
    {
        b2BodyDef bd4;
        ground4 = world->CreateBody(&bd4);
        
        b2EdgeShape shape;
        shape.Set(b2Vec2(-0.0f, 0.0f), b2Vec2(0.0, 0.0));
        ground4->CreateFixture(&shape, 0.0f);
    }
    
    b2Body* ground5= NULL;
    {
        b2BodyDef bd5;
        ground5 = world->CreateBody(&bd5);
        
        b2EdgeShape shape;
        shape.Set(b2Vec2(-0.0f, 0.0f), b2Vec2(0.0, 0.0));
        ground5->CreateFixture(&shape, 0.0f);
    }
    
    b2Body* ground6 = NULL;
    {
        b2BodyDef bd6;
        ground6 = world->CreateBody(&bd6);
        
        b2EdgeShape shape;
        shape.Set(b2Vec2(-0.0f, 0.0f), b2Vec2(0.0, 0.0));
        ground6->CreateFixture(&shape, 0.0f);
    }
    
    b2PolygonShape shape;
    shape.SetAsBox(1.4f, 0.3f);
    
    b2PolygonShape shape1_1;
    shape1_1.SetAsBox(1.4f, 1.0f);
    
    b2PolygonShape shape1_2;
    shape1_2.SetAsBox(0.01f, 0.3f);
    
    b2PolygonShape shape2_1;
    shape2_1.SetAsBox(1.0f, 0.7f);
    
    b2PolygonShape shape2_2;
    shape2_2.SetAsBox(0.01f, 0.3f);
    
    b2PolygonShape shape3_1;
    shape3_1.SetAsBox(1.0f, 0.7f);
    
    b2PolygonShape shape3_2;
    shape3_2.SetAsBox(0.01f, 0.3f);
    
    b2PolygonShape shape4_1;
    shape4_1.SetAsBox(1.0f, 0.7f);
    
    b2PolygonShape shape4_2;
    shape4_2.SetAsBox(0.01f, 0.3f);

    
    b2PolygonShape shape2;
    shape2.SetAsBox(1.4f, 1.0f);
    
    b2PolygonShape shape3;
    shape3.SetAsBox(1.4f, 0.7f);
    
    b2FixtureDef fd;
    fd.shape = &shape1_1;
    fd.density =1.0f;
    //fd.friction = 1.0;
    //fd.restitution = 0.01;
    fd.filter.maskBits = 0x0000;
    
    b2FixtureDef fd1_2;
    fd1_2.shape = &shape1_2;
    fd1_2.density = 30000.0f;
    //fd1_2.friction = 1.0000;
    //fd1_2.restitution = 0;
    fd1_2.filter.maskBits = 0x0000;
    
    b2FixtureDef fd2;
    fd2.shape = &shape2_1;
    fd2.density = 1.2f;
    fd2.friction = 0.1;
    fd2.restitution = 0.05;
    fd2.filter.maskBits = 0x0000;
    
    b2FixtureDef fd2_2;
    fd2_2.shape = &shape2_2;
    fd2_2.density =30000.0f;
    fd2_2.friction = 0.1;
    fd2_2.restitution = 0.05;
    fd2_2.filter.maskBits = 0x0000;
    
    b2FixtureDef fd3;
    fd3.shape = &shape3_1;
    fd3.density = 50.0f;
    fd3.friction = 1.0;
    fd3.restitution = 0.05;
    fd3.filter.maskBits = 0x0000;
    
    b2FixtureDef fd3_2;
    fd3_2.shape = &shape3_2;
    fd3_2.density =4000.0;
    fd3_2.friction = 2.0;
    fd3_2.restitution = 0.1;
    fd3_2.filter.maskBits = 0x0000;
    
    b2FixtureDef fd4;
    fd4.shape = &shape3_1;
    fd4.density = 8.0f;
    fd4.friction = 2.0;
    fd4.filter.maskBits = 0x0000;
    
    b2FixtureDef fd4_2;
    fd4_2.shape = &shape3_2;
    fd4_2.density =30000.0f;
    fd4_2.friction = 2.0;
    fd4_2.restitution = 0.1;
    fd4_2.filter.maskBits = 0x0000;
    
    
    b2FixtureDef fd5;
    fd5.shape = &shape4_1;
    fd5.density = 1.0f;
    fd5.friction = 2.0;
    fd5.filter.maskBits = 0x0000;
    
    b2FixtureDef fd5_2;
    fd5_2.shape = &shape4_2;
    fd5_2.density =30000.0f;
    fd5_2.friction = 2.0;
    fd5_2.restitution = 0.1;
    fd5_2.filter.maskBits = 0x0000;
    
    b2FixtureDef fd6;
    fd6.shape = &shape3_1;
    fd6.density = 1.0f;
    fd6.friction = 0.0;
    fd6.filter.maskBits = 0x0000;
    
    b2FixtureDef fd6_2;
    fd6_2.shape = &shape3_2;
    fd6_2.density =30000.0f;
    fd6_2.friction = 0.0;
    fd6_2.filter.maskBits = 0x0000;

    b2FixtureDef fd7;
    fd7.shape = &shape3;
    fd7.density = 8.0f;
    fd7.friction = 2.0;
    fd7.filter.maskBits = 0x0000;
    
    b2FixtureDef fd7_2;
    fd7_2.shape = &shape;
    fd7_2.density =1600.0;
    fd7_2.friction = 2.0;
    fd7_2.restitution = 0.1;
    fd7_2.filter.maskBits = 0x0000;
    
    b2FixtureDef fd8;
    fd8.shape = &shape3;
    fd8.density = 8.0f;
    fd8.friction = 2.0;
    fd8.filter.maskBits = 0x0000;
    
    b2FixtureDef fd8_2;
    fd8_2.shape = &shape;
    fd8_2.density =1600.0;
    fd8_2.friction = 2.0;
    fd8_2.restitution = 0.1;
    fd8_2.filter.maskBits = 0x0000;
    

    b2WeldJointDef jd;
    b2WeldJointDef jd2;
    b2WeldJointDef jd3;
    b2WeldJointDef jd4;
    b2WeldJointDef jd5;
    b2WeldJointDef jd6;
    b2WeldJointDef jd7;
    b2WeldJointDef jd8;
    
    b2Body* flowerBody = ground;
    b2Body* flowerBody2 = ground2;
    b2Body* flowerBody4 = ground4;
    b2Body* flowerBody5 = ground5;
    b2Body* flowerBody6 = ground6;
    
    //花１
    for (int32 i = 0; i < 10; ++i)
    {
        CCNode *parent = [self getChildByTag:kTagParentNode2];
        PhysicsSprite *sprite;
        b2BodyDef bd;
        b2Body* body;
        bd.type = b2_dynamicBody;
        //sprite.position = ccp(20.0f,11.0f+ 1.0f * i);

        if(i == 9){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(65,0,100,88)];
            [parent addChild:sprite];
            bd.position.Set(xpos,ypos+1.0f+ 0.6f * i);
            body = world->CreateBody(&bd);
            body->CreateFixture(&fd);

            
        }else if(i == 0){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(-48,0,46,36)];
            [parent addChild:sprite];
            bd.position.Set(xpos,ypos2+ 0.6f * i);
            body = world->CreateBody(&bd);
            body->CreateFixture(&fd1_2);

        }else if(i == 1){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*0,0,46,36)];
            [parent addChild:sprite];
            bd.position.Set(xpos,ypos2+ 0.6f * i);
            body = world->CreateBody(&bd);
            body->CreateFixture(&fd1_2);
 
            
        }else if(i == 3){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*0,0,46,36)];
            [parent addChild:sprite];
            bd.position.Set(xpos,ypos2+ 0.6f * i);
            body = world->CreateBody(&bd);
            body->CreateFixture(&fd1_2);
    
        }else if(i == 5){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*1,0,46,36)];
            [parent addChild:sprite];
            bd.position.Set(xpos,ypos2+ 0.6f * i);
            body = world->CreateBody(&bd);
            body->CreateFixture(&fd1_2);

        }else{
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*2,0,46,36)];
            [parent addChild:sprite];
            bd.position.Set(xpos,ypos2+ 0.6f * i);
            body = world->CreateBody(&bd);
            body->CreateFixture(&fd1_2);

        }
        
        
        b2Vec2 anchor(xpos, ypos+0.6f * i);
        jd.Initialize(flowerBody, body, anchor);
        world->CreateJoint(&jd);
        [sprite setPhysicsBody:body];
        flowerBody = body;
        
        
    }
    
    //花2
    for (int32 i = 0; i < 9; ++i)
    {
        CCNode *parent = [self getChildByTag:kTagParentNode2];
        PhysicsSprite *sprite;
        b2BodyDef bd2;
        b2Body* body2;
        bd2.type = b2_dynamicBody;
        //sprite.position = ccp(20.0f,11.0f+ 1.0f * i);
        
        if(i == 8){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+65,0,100,44)];
            [parent addChild:sprite];
            bd2.position.Set(xpos+4.0f,ypos+0.7f+ 0.6f * i);
            body2 = world->CreateBody(&bd2);
            body2->CreateFixture(&fd2);
        }else if(i == 0){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(-48,0,46,36)];
            [parent addChild:sprite];
            bd2.position.Set(xpos+4.0f,ypos2+ 0.6f * i);
            body2 = world->CreateBody(&bd2);
            body2->CreateFixture(&fd2_2);
        }else if(i == 1){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*1,0,46,36)];
            [parent addChild:sprite];
            bd2.position.Set(xpos+4.0f,ypos2+ 0.6f * i);
            body2 = world->CreateBody(&bd2);
            body2->CreateFixture(&fd2_2);
            
        }else if(i == 3){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*1,0,46,36)];
            [parent addChild:sprite];
            bd2.position.Set(xpos+4.0f,ypos2+ 0.6f * i);
            body2 = world->CreateBody(&bd2);
            body2->CreateFixture(&fd2_2);
        }else if(i == 5){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*0,0,46,36)];
            [parent addChild:sprite];
            bd2.position.Set(xpos+4.0f,ypos2+ 0.6f * i);
            body2 = world->CreateBody(&bd2);
            body2->CreateFixture(&fd2_2);
        }else{
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*2,0,46,36)];
            [parent addChild:sprite];
            bd2.position.Set(xpos+4.0f,ypos2+ 0.6f * i);
            body2 = world->CreateBody(&bd2);
            body2->CreateFixture(&fd2_2);
        }
        
        b2Vec2 anchor2(xpos+4.0f, ypos+ 0.6f * i);
        jd2.Initialize(flowerBody2, body2, anchor2);
        world->CreateJoint(&jd2);
        [sprite setPhysicsBody:body2];
        flowerBody2 = body2;

    }


    //花4
    for (int32 i = 0; i < 5; ++i)
    {
        CCNode *parent = [self getChildByTag:kTagParentNode2];
        PhysicsSprite *sprite;
        b2BodyDef bd4;
        b2Body* body4;
        bd4.type = b2_dynamicBody;
        //sprite.position = ccp(20.0f,11.0f+ 1.0f * i);
        
        if(i == 4){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(0,0,27,70)];
            [parent addChild:sprite];
            bd4.position.Set(xpos+7.0f,ypos+0.7f+0.6f * i);
            body4 = world->CreateBody(&bd4);
            body4->CreateFixture(&fd4);
            
        }else if(i == 0){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(-48,0,46,36)];
            [parent addChild:sprite];
            bd4.position.Set(xpos+7.0f,ypos2+0.6f * i);
            body4 = world->CreateBody(&bd4);
            body4->CreateFixture(&fd4_2);
            

        }else if(i == 2){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*1,0,46,36)];
            [parent addChild:sprite];
            bd4.position.Set(xpos+7.0f,ypos2+ 0.6f * i);
            body4 = world->CreateBody(&bd4);
            body4->CreateFixture(&fd4_2);
 
        }else{
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*2,0,46,36)];
            [parent addChild:sprite];
            bd4.position.Set(xpos+7.0f,ypos2+ 0.6f * i);
            body4 = world->CreateBody(&bd4);
            body4->CreateFixture(&fd4_2);

        }
        
        b2Vec2 anchor4(xpos+7.0f, ypos+ 0.6f * i);
        jd4.Initialize(flowerBody4, body4, anchor4);
        world->CreateJoint(&jd4);
        [sprite setPhysicsBody:body4];
        flowerBody4 = body4;
        
    }
    
    //花5
    for (int32 i = 0; i < 10; ++i)
    {
        CCNode *parent = [self getChildByTag:kTagParentNode2];
        PhysicsSprite *sprite;
        b2BodyDef bd5;
        b2Body* body5;
        bd5.type = b2_dynamicBody;
        //sprite.position = ccp(20.0f,11.0f+ 1.0f * i);
        
        if(i == 9){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(65,0,100,88)];
            sprite.position = ccp(-20,0);
            [parent addChild:sprite];
            bd5.position.Set(xpos+9.0f,ypos+1.0+0.6f * i);
            body5 = world->CreateBody(&bd5);
            body5->CreateFixture(&fd5);
        }else if(i == 0){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(-48,0,46,36)];
            [parent addChild:sprite];
            bd5.position.Set(xpos+9.0f,ypos2+ 0.6f * i);
            body5 = world->CreateBody(&bd5);
            body5->CreateFixture(&fd5_2);
        }else if(i == 1){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*0,0,46,36)];
            [parent addChild:sprite];
            bd5.position.Set(xpos+9.0f,ypos2+ 0.6f * i);
            body5 = world->CreateBody(&bd5);
            body5->CreateFixture(&fd5_2);
        }else if(i == 3){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*0,0,46,36)];
            [parent addChild:sprite];
            bd5.position.Set(xpos+9.0f,ypos2+ 0.6f * i);
            body5 = world->CreateBody(&bd5);
            body5->CreateFixture(&fd5_2);
        }else if(i == 5){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*1,0,46,36)];
            [parent addChild:sprite];
            bd5.position.Set(xpos+9.0f,ypos2+ 0.6f * i);
            body5 = world->CreateBody(&bd5);
            body5->CreateFixture(&fd5_2);
        }else{
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*2,0,46,36)];
            [parent addChild:sprite];
            bd5.position.Set(xpos+9.0f,ypos2+ 0.6f * i);
            body5 = world->CreateBody(&bd5);
            body5->CreateFixture(&fd5_2);
        }
        
        b2Vec2 anchor5(xpos+9.0f, ypos+ 0.6f * i);
        jd5.Initialize(flowerBody5, body5, anchor5);
        world->CreateJoint(&jd5);
        [sprite setPhysicsBody:body5];
        flowerBody5 = body5;
    }
    
    
    //花6
    for (int32 i = 0; i < 9; ++i)
    {
        CCNode *parent = [self getChildByTag:kTagParentNode2];
        PhysicsSprite *sprite;
        b2BodyDef bd6;
        b2Body* body6;
        bd6.type = b2_dynamicBody;
        
        if(i == 8){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(29,0,34,70)];
            [parent addChild:sprite];
            bd6.position.Set(xpos+11.0f,ypos+1.0f+0.6f * i);
            body6 = world->CreateBody(&bd6);
            body6->CreateFixture(&fd6);
            
            b2Vec2 anchor6(xpos+11.0f, ypos+1.0f+ 0.6f * i);
            jd6.Initialize(flowerBody6, body6, anchor6);
            world->CreateJoint(&jd6);
            [sprite setPhysicsBody:body6];
            flowerBody6 = body6;
            
        }else if(i == 0){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(-48,0,46,36)];
            [parent addChild:sprite];
            bd6.position.Set(xpos+11.0f,ypos2+ 0.6f * i);
            body6 = world->CreateBody(&bd6);
            body6->CreateFixture(&fd6_2);
            
            b2Vec2 anchor6(xpos+11.0f, ypos+ 0.6f * i);
            jd6.Initialize(flowerBody6, body6, anchor6);
            world->CreateJoint(&jd6);
            [sprite setPhysicsBody:body6];
            flowerBody6 = body6;
        }else if(i == 1){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*1,0,46,36)];
            [parent addChild:sprite];
            bd6.position.Set(xpos+11.0f,ypos2+ 0.6f * i);
            body6 = world->CreateBody(&bd6);
            body6->CreateFixture(&fd6_2);
            
            b2Vec2 anchor6(xpos+11.0f, ypos+ 0.6f * i);
            jd6.Initialize(flowerBody6, body6, anchor6);
            world->CreateJoint(&jd6);
            [sprite setPhysicsBody:body6];
            flowerBody6 = body6;
        }else if(i == 3){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*1,0,46,36)];
            [parent addChild:sprite];
            bd6.position.Set(xpos+11.0f,ypos2+ 0.6f * i);
            body6 = world->CreateBody(&bd6);
            body6->CreateFixture(&fd6_2);
            
            b2Vec2 anchor6(xpos+11.0f, ypos+ 0.6f * i);
            jd6.Initialize(flowerBody6, body6, anchor6);
            world->CreateJoint(&jd6);
            [sprite setPhysicsBody:body6];
            flowerBody6 = body6;
        }else if(i == 5){
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*0,0,46,36)];
            [parent addChild:sprite];
            bd6.position.Set(xpos+11.0f,ypos2+ 0.6f * i);
            body6 = world->CreateBody(&bd6);
            body6->CreateFixture(&fd6_2);
            
            b2Vec2 anchor6(xpos+11.0f, ypos+ 0.6f * i);
            jd6.Initialize(flowerBody6, body6, anchor6);
            world->CreateJoint(&jd6);
            [sprite setPhysicsBody:body6];
            flowerBody6 = body6;
        }else{
            sprite = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(102+99+65+48*2,0,46,36)];
            [parent addChild:sprite];
            bd6.position.Set(xpos+11.0f,ypos2+ 0.6f * i);
            body6 = world->CreateBody(&bd6);
            body6->CreateFixture(&fd6_2);
            
            b2Vec2 anchor6(xpos+11.0f, ypos+ 0.6f * i);
            jd6.Initialize(flowerBody6, body6, anchor6);
            world->CreateJoint(&jd6);
            [sprite setPhysicsBody:body6];
            flowerBody6 = body6;
        }
        
        
    }
    


}
-(void) addNewSpriteAtPosition:(CGPoint)p
{
    
    b2BuoyancyControllerDef bcd;
    

	//CCLOG(@"Add sprite %0.2f x %02.f",p.x,p.y);
	CCNode *parent = [self getChildByTag:kTagParentNode];
	
	//We have a 64x64 sprite sheet with 4 different 32x32 images.  The following code is
	//just randomly picking one of the images
	int idx = (CCRANDOM_0_1()*13);
	int idy = (CCRANDOM_0_1()*12);
	PhysicsSprite *sprite = [PhysicsSprite spriteWithTexture:spriteTexture_ rect:CGRectMake(38 * idx,38 * idy,38,38)];
	[parent addChild:sprite];
	
	sprite.position = ccp( p.x, p.y);
	
	// Define the dynamic body.
	//Set up a 1m squared box in the physics world
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO);
    bodyDef.userData = sprite;
	b2Body *body = world->CreateBody(&bodyDef);
	
	// Define another box shape for our dynamic body.
	//b2PolygonShape dynamicBox;
    b2CircleShape circle;
    circle.m_radius = 20.0/PTM_RATIO;
	//dynamicBox.SetAsBox(.5f, .5f);//These are mid points for our 1m box
    

    
    b2PolygonShape shape;
    shape.SetAsBox(0.5f, 0.125f);
    
    b2FixtureDef fd;
    fd.shape = &shape;
    fd.density = 20.0f;
    
    b2WeldJointDef jd;
    
  
	
	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;
	fixtureDef.density = 1.0f; //密度
	fixtureDef.friction = 0.3f; //摩擦係数
    fixtureDef.restitution = 0.5f; //反発
	body->CreateFixture(&fixtureDef);
    
    bcd.normal.Set(0.0f, 1.0f);
    bcd.offset = ptm(BUOYANCYOFFSET);
    bcd.density = 4.0f; //密度
    bcd.linearDrag = 2.0f; //水中反発
    bcd.angularDrag = 1.0f; //オブジェクト回転時の抵抗
    bcd.useWorldGravity = false;
    bcd.gravity = b2Vec2(0.5f, -2.0f);
    bc = (b2BuoyancyController *)world->CreateController(&bcd);
    
    
    bc->AddBody(body);
	[sprite setPhysicsBody:body];
    

     
    
}

-(void) update: (ccTime) dt
{
	//It is recommended that a fixed time step is used with Box2D for stability
	//of the simulation, however, we are using a variable time step here.
	//You need to make an informed choice, the following URL is useful
	//http://gafferongames.com/game-physics/fix-your-timestep/
	
	int32 velocityIterations = 8;
	int32 positionIterations = 1;
    
	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	world->Step(dt, velocityIterations, positionIterations);
    
    for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
        if (b->GetUserData() != NULL) {
            CCSprite *sprite = (CCSprite*)b->GetUserData();
            sprite.position = CGPointMake( b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO);

            if(sprite.position.x/PTM_RATIO < -9.0){
                //bc->RemoveBody(b);
                [self removeChild:sprite cleanup:YES];
                world->DestroyBody(b);
                [self ballCriate];
            }
        }
        
        
    }
    
}
//消えたボールを生成する
- (void)ballCriate{
    [self addNewSpriteAtPosition:ccp(500+CCRANDOM_0_1()*500, CCRANDOM_0_1()*150)];
}
- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	//Add a new body/atlas sprite at the touched location
    
    if (_mouseJoint != NULL) return;
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    for (b2Body* b = world->GetBodyList(); b; b = b->GetNext()){
        if (b->GetType() == b2_dynamicBody) {
            for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext()) {
                if (f->TestPoint(locationWorld)) {
                    b2MouseJointDef md;
                    md.bodyA = groundBody;
                    md.bodyB = b;
                    md.target = locationWorld;
                    md.collideConnected = true;
                    md.maxForce = 4000.0f * b->GetMass();
                    
                    _mouseJoint = (b2MouseJoint *)world->CreateJoint(&md);
                    b->SetAwake(true);
                }
            }
        }
    }
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint == NULL) return;
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    
    _mouseJoint->SetTarget(locationWorld);
    
}

-(void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint) {
        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
    
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_mouseJoint) {
        [SoundEffect sePlay:@"ofn_se_shirahige.mp3"];

        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
}

@end
