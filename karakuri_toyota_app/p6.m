//
//  p6.m
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "p6.h"


@implementation p6
float murabitoHeigth = -80;
-(id)init{
    if(self = [super init]){
        self.ignoreAnchorPointForPosition = NO;
        self.isTouchEnabled = YES;

        
        p6framechash = [CCSpriteFrameCache sharedSpriteFrameCache];
        [p6framechash addSpriteFramesWithFile:@"p6Resource.plist"];
        
        p6batchNode = [CCSpriteBatchNode batchNodeWithFile:@"p6Resource.pvr.gz"];
        [self addChild:p6batchNode];
        
        //レイヤーの追加
        p6MurabitoNode = [CCLayer node];
        p6MurabitoNode.anchorPoint = ccp(0.5f,0.5f);
        [self addChild:p6MurabitoNode z:1];
        
        //村人のy座標
        p6Murabito1  = [CCSprite spriteWithSpriteFrameName:@"p6_murabito1_1.png"];
        p6Murabito1.position = ccp(145,murabitoHeigth);
        p6Murabito1.anchorPoint = ccp(0.5,0);
        [p6MurabitoNode addChild:p6Murabito1 z:2];
        
        p6Murabito2  = [CCSprite spriteWithSpriteFrameName:@"p6_murabito1_2.png"];
        p6Murabito2.position = ccp(p6Murabito1.position.x+150,murabitoHeigth);
        p6Murabito2.anchorPoint = ccp(0.5,0);
        [p6MurabitoNode addChild:p6Murabito2 z:2];
        
        p6Murabito3  = [CCSprite spriteWithSpriteFrameName:@"p6_murabito1_3.png"];
        p6Murabito3.position = ccp(p6Murabito2.position.x+130,murabitoHeigth);
        p6Murabito3.anchorPoint = ccp(0.5,0);
        [p6MurabitoNode addChild:p6Murabito3 z:2];
        
        p6Murabito4  = [CCSprite spriteWithSpriteFrameName:@"p6_murabito1_4.png"];
        p6Murabito4.position = ccp(p6Murabito3.position.x+170,murabitoHeigth);
        p6Murabito4.anchorPoint = ccp(0.5,0);
        [p6MurabitoNode addChild:p6Murabito4 z:2];
        
        p6Murabito5  = [CCSprite spriteWithSpriteFrameName:@"p6_murabito1_5.png"];
        p6Murabito5.position = ccp(p6Murabito4.position.x+135,murabitoHeigth);
        p6Murabito5.anchorPoint = ccp(0.5,0);
        [p6MurabitoNode addChild:p6Murabito5 z:2];
        
        p6Murabito6  = [CCSprite spriteWithSpriteFrameName:@"p6_murabito1_6.png"];
        p6Murabito6.position = ccp(p6Murabito5.position.x+170,murabitoHeigth);
        p6Murabito6.anchorPoint = ccp(0.5,0);
        [p6MurabitoNode addChild:p6Murabito6 z:2];
        
    }
    return self;
}

- (void)p6_touchEvent:(int) no{
    
    id rote = [CCRotateBy actionWithDuration:0.5 angle:180];
    id ease = [CCEaseBounceOut actionWithAction:rote];
    id seq = [CCSequence actions:ease, nil];
    
    if (no == 1) {
        p6Murabito1_te  = [CCSprite spriteWithSpriteFrameName:@"p6_murabito1_1_te.png"];
        p6Murabito1_te.position = ccp(140,250);
        p6Murabito1_te.anchorPoint = ccp(0.5,1.0);
        [p6MurabitoNode addChild:p6Murabito1_te z:1];
        [p6Murabito1_te runAction:seq];
        p6MurabitoTouch1 = true;
        
    }else if (no == 2) {
        p6Murabito2_te  = [CCSprite spriteWithSpriteFrameName:@"p6_murabito1_2_te.png"];
        p6Murabito2_te.position = ccp(270,180);
        p6Murabito2_te.anchorPoint = ccp(0.5,1.0);
        [p6MurabitoNode addChild:p6Murabito2_te z:1];
        [p6Murabito2_te runAction:seq];
        
        p6MurabitoTouch2 = true;
        
    }else if (no == 3) {
        p6Murabito3_te  = [CCSprite spriteWithSpriteFrameName:@"p6_murabito1_3_te.png"];
        p6Murabito3_te.position = ccp(425,250);
        p6Murabito3_te.anchorPoint = ccp(0.5,1.0);
        [p6MurabitoNode addChild:p6Murabito3_te z:1];
        [p6Murabito3_te runAction:seq];
        
        p6MurabitoTouch3 = true;
    }else if (no == 4) {
        p6Murabito4_te  = [CCSprite spriteWithSpriteFrameName:@"p6_murabito1_4_te.png"];
        p6Murabito4_te.position = ccp(575,230);
        p6Murabito4_te.anchorPoint = ccp(0.5,1.0);
        [p6MurabitoNode addChild:p6Murabito4_te z:1];
        
        [p6Murabito4_te runAction:seq];
        p6MurabitoTouch4 = true;
        
    }else if (no == 5) {
        p6Murabito5_te  = [CCSprite spriteWithSpriteFrameName:@"p6_murabito1_5_te.png"];
        p6Murabito5_te.position = ccp(730,220);
        p6Murabito5_te.anchorPoint = ccp(0.5,1.0);
        [p6MurabitoNode addChild:p6Murabito5_te z:1];
        
        [p6Murabito5_te runAction:seq];
        p6MurabitoTouch5 = true;
    }
    else if (no == 6) {
        p6Murabito6_te  = [CCSprite spriteWithSpriteFrameName:@"p6_murabito1_6_te.png"];
        p6Murabito6_te.position = ccp(875,260);
        p6Murabito6_te.anchorPoint = ccp(0.5,1.0);
        [p6MurabitoNode addChild:p6Murabito6_te z:1];
        
        [p6Murabito6_te runAction:seq];
        p6MurabitoTouch6 = true;
    }
    
}

-(CGRect)spriteTouch2:(CCSprite *) sprite{
    float h = [sprite contentSize].height;
    float w = [sprite contentSize].width;
    float x = sprite.position.x-w/2;
    float y = sprite.position.y;
    CGRect rect = CGRectMake(x, y, w, h);
    
    return rect;
}




#pragma mark タイトルイベント
-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch  = [touches anyObject];
    CGPoint location = [touch locationInView:[touch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    
        
    if(!p6MurabitoTouch_all){
        CGRect obj01Rect2 = [self spriteTouch2:p6Murabito1];
        CGRect obj02Rect2 = [self spriteTouch2:p6Murabito2];
        CGRect obj03Rect2 = [self spriteTouch2:p6Murabito3];
        CGRect obj04Rect2 = [self spriteTouch2:p6Murabito4];
        CGRect obj05Rect2 = [self spriteTouch2:p6Murabito5];
        CGRect obj06Rect2 = [self spriteTouch2:p6Murabito6];
        
        if(CGRectContainsPoint(obj01Rect2, location) && !p6MurabitoTouch1){
            [self p6_touchEvent:1];
            [SoundEffect sePlay:@"ofn_se_p6_te.mp3"];

        }
        
        if(CGRectContainsPoint(obj02Rect2, location) && !p6MurabitoTouch2){
            [self p6_touchEvent:2];
            [SoundEffect sePlay:@"ofn_se_p6_te.mp3"];

        }
        if(CGRectContainsPoint(obj03Rect2, location) && !p6MurabitoTouch3){
            [self p6_touchEvent:3];
            [SoundEffect sePlay:@"ofn_se_p6_te.mp3"];

        }
        if(CGRectContainsPoint(obj04Rect2, location) && !p6MurabitoTouch4){
            [self p6_touchEvent:4];
            [SoundEffect sePlay:@"ofn_se_p6_te.mp3"];

        }
        if(CGRectContainsPoint(obj05Rect2, location) && !p6MurabitoTouch5){
            [self p6_touchEvent:5];
            [SoundEffect sePlay:@"ofn_se_p6_te.mp3"];

        }
        if(CGRectContainsPoint(obj06Rect2, location) && !p6MurabitoTouch6){
            [self p6_touchEvent:6];
            [SoundEffect sePlay:@"ofn_se_p6_te.mp3"];

        }
    }
}
-(void) dealloc
{
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [super dealloc];
}


@end
