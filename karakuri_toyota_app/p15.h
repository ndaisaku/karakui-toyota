//
//  p14.h
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import <MapKit/MapKit.h>


@interface p15 : CCLayer <UIWebViewDelegate,MKMapViewDelegate>{
    UIWebView *webView_;
    UIToolbar *toolBar;
    UIActivityIndicatorView *activityInducator_;
    MKMapView *mv;
    CLLocationCoordinate2D destination;
    CCLayer *layer_sanageyama;
    CCLayer *layer_jinjya;
    CCLayer *layer_maturi;
    CCLayer *layer_yahagi;
    CCLayer *layer_shirahige;
    
}

@end
