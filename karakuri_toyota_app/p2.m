//
//  p2.m
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "p2.h"


typedef enum nodeTags
{
    kTagoosunoW,
    kTagsusanoW,
    LayerTagUILayer,
} Tags;

@implementation p2

-(id) init{
    if( (self=[super init])) {
        
        self.ignoreAnchorPointForPosition = NO;
        self.isTouchEnabled = YES;
        
        CGSize winSize = [[CCDirector sharedDirector] winSize];

        CCSpriteFrameCache *frameChache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameChache addSpriteFramesWithFile:@"p2_road.plist"];
        
        CCSpriteBatchNode *p2roadBatch = [CCSpriteBatchNode batchNodeWithFile:@"p2_road.pvr.gz"];
        [self addChild:p2roadBatch];
        
        road = [CCSprite spriteWithSpriteFrameName:@"p2_road.jpg"];
        road.anchorPoint = ccp(0, 0);
        road.position = ccp(0, 0);
        [self addChild:road];
        
        road2 = [CCSprite spriteWithSpriteFrameName:@"p2_road.jpg"];
        road2.anchorPoint = ccp(0, 0);
        road2.position = ccp(1024, 0);
        [self addChild:road2];
        
        p2_ki1 = [CCSprite spriteWithSpriteFrameName:@"p2_ki.png"];
        p2_ki1.anchorPoint = ccp(0, 0);
        p2_ki1.position = ccp(1024, road.contentSize.height-20);
        [self addChild:p2_ki1];
        
        p2_ki2 = [CCSprite spriteWithSpriteFrameName:@"p2_ki2.png"];
        p2_ki2.anchorPoint = ccp(0, 0);
        p2_ki2.position = ccp(1200, road.contentSize.height-20);
        [self addChild:p2_ki2];
        
        
        
        //道の秒速の計算
        float sec = 50.0;
        spPersec = (road.contentSize.width-winSize.width)/sec;
        spPersec2 = winSize.width/spPersec;
        spPersec3 = (road2.contentSize.width)/spPersec;
        
        //木の速度
        float treeSec = 300.0;
        
        
        id roadMove = [CCMoveTo actionWithDuration:sec position:ccp(-road.contentSize.width+winSize.width, 0)];
        id roadMove2 = [CCMoveTo actionWithDuration:spPersec2 position:ccp(-road.contentSize.width, 0)];
        
        id func = [CCCallFunc actionWithTarget:self selector:@selector(p2Roadmove)];
        id func2 = [CCCallBlock actionWithBlock:^{
            road.position = ccp(1024, 0);
        }];
        id Seq = [CCSequence actions:roadMove,func,roadMove2,func2, nil];
        [road runAction:Seq];
        
        id treedMove = [CCMoveTo actionWithDuration:treeSec position:ccp(-road.contentSize.width+winSize.width, road.contentSize.height)];
        [p2_ki1 runAction:treedMove];
        
        id treedMove2 = [CCMoveTo actionWithDuration:treeSec position:ccp(-road.contentSize.width+winSize.width, road.contentSize.height)];
        [p2_ki2 runAction:treedMove2];
        
        
        //オオスノ歩く
        oosunoWalk = [CCLayer node];
        oosunoWalk.anchorPoint = ccp(0.5f, 0.5f);
        [self addChild:oosunoWalk z:1 tag:kTagoosunoW];
        
        NSMutableArray *p2Walk = [NSMutableArray array];
        for(int i=1; i<=2; i++){
            [p2Walk addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"p2_walk%d.png",i ]]];
        }
        CCAnimation *p2WalkAnimation = [CCAnimation animationWithSpriteFrames:p2Walk delay:0.40 ];
        
        CCSprite *p2walkSprit = [CCSprite spriteWithSpriteFrameName:@"p2_walk1.png"];
        p2walkSprit.position = ccp(winSize.width/2-43,road.contentSize.height-30);
        [oosunoWalk addChild:p2walkSprit];
        
        CCAnimate *p2walkAnimate = [CCAnimate actionWithAnimation:p2WalkAnimation];
        id rep = [CCRepeatForever actionWithAction:p2walkAnimate];
        [p2walkSprit runAction:rep];
        
        CCSprite *oosuno = [CCSprite spriteWithSpriteFrameName:@"p2_oosuno.png"];
        oosuno.position = ccp(winSize.width/2, road.contentSize.height+38);
        [oosunoWalk addChild:oosuno];
        
        //スサノオ歩く
        susanooWalk = [CCLayer node];
        susanooWalk.anchorPoint = ccp(0.5f, 0.5f);
        susanooWalk.position = ccp(-660,0);
        [self addChild:susanooWalk z:10 tag:kTagsusanoW];
        
        NSMutableArray *p2susanoWalk = [NSMutableArray array];
        for(int i=1; i<=2; i++){
            [p2susanoWalk addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:[NSString stringWithFormat:@"p2_susano_w%d.png",i ]]];
        }
        CCAnimation *p2susanooWalkAnimation = [CCAnimation animationWithSpriteFrames:p2susanoWalk delay:0.40 ];
        
        CCSprite *p2susanoowalkSprit = [CCSprite spriteWithSpriteFrameName:@"p2_susano_w1.png"];
        p2susanoowalkSprit.position = ccp(winSize.width/2-78,road.contentSize.height-30);
        [susanooWalk addChild:p2susanoowalkSprit];
        
        CCAnimate *p2susanowalkAnimate = [CCAnimate actionWithAnimation:p2susanooWalkAnimation];
        id rep2 = [CCRepeatForever actionWithAction:p2susanowalkAnimate];
        [p2susanoowalkSprit runAction:rep2];
        
        CCSprite *susanooken = [CCSprite spriteWithSpriteFrameName:@"p2_ken.png"];
        susanooken.anchorPoint = ccp(0,0.3f);
        susanooken.position = ccp(winSize.width/2, road.contentSize.height+35);
        [susanooWalk addChild:susanooken];
        
        id Rote = [CCRotateBy actionWithDuration:0.5f angle:30];
        id Rote2 = [CCRotateBy actionWithDuration:0.5f angle:-30];
        id seq2 = [CCSequence actions:Rote,Rote2, nil];
        id Repat = [CCRepeatForever actionWithAction:seq2];
        [susanooken runAction:Repat];
        
        
        
        CCSprite *susanoo = [CCSprite spriteWithSpriteFrameName:@"p2_susano.png"];
        susanoo.position = ccp(winSize.width/2, road.contentSize.height+38);
        [susanooWalk addChild:susanoo];
    }
    return self;
}

-(void)p2Roadmove{
    id func = [CCCallFunc actionWithTarget:self selector:@selector(p2Roadmove2)];
    id func2 = [CCCallBlock actionWithBlock:^{
        road2.position = ccp(1024, 0);
    }];
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    id roadMove = [CCMoveTo actionWithDuration:spPersec3 position:ccp(-road2.contentSize.width+winSize.width, 0)];
    id roadMove2 = [CCMoveTo actionWithDuration:spPersec2 position:ccp(-road2.contentSize.width, 0)];
    id Seq = [CCSequence actions:roadMove,func,roadMove2,func2, nil];
    [road2 runAction:Seq];
    
}

-(void)p2Roadmove2{
    
    id func = [CCCallFunc actionWithTarget:self selector:@selector(p2Roadmove)];
    id func2 = [CCCallBlock actionWithBlock:^{
        road.position = ccp(1024, 0);
    }];
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    id roadMove = [CCMoveTo actionWithDuration:spPersec3 position:ccp(-road.contentSize.width+winSize.width, 0)];
    id roadMove2 = [CCMoveTo actionWithDuration:spPersec2 position:ccp(-road.contentSize.width, 0)];
    id Seq = [CCSequence actions:roadMove,func,roadMove2, func2,nil];
    [road runAction:Seq];
    
}
-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    holdCount = 0.0f;
    [self schedule:@selector(scheduleTouchHoldCounter:)];
    
    UITouch *touch  = [touches anyObject];
    CGPoint location = [touch locationInView:[touch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    CGSize winSize =[[CCDirector sharedDirector] winSize];
    
    int randNo = CCRANDOM_0_1()*5+1;
    NSString *imagePath = [NSString stringWithFormat:@"p2_ya%d.png",randNo];
    CCSprite *p2Ya1 = [CCSprite spriteWithSpriteFrameName:imagePath];
    p2Ya1.position = ccp(location.x, winSize.height);
    [self addChild:p2Ya1];
    
    id func = [CCCallBlock actionWithBlock:^{
        [self removeChild:p2Ya1 cleanup:YES];
    }];
    id yaMove1 = [CCMoveBy actionWithDuration:2.0 position:ccp(-600,-road.contentSize.height-230+p2Ya1.contentSize.height/2)];
    id easeYa = [CCEaseSineOut actionWithAction:yaMove1];
    
    id Rote = [CCRotateBy actionWithDuration:2.0 angle:-540];
    id easeYa2 = [CCEaseSineOut actionWithAction:Rote];
    
    if(randNo >=4){
        [SoundEffect sePlay:@"ofn_se_p2_ken.mp3"];

        id spawn = [CCSpawn actions:easeYa,easeYa2, nil];
        id seq = [CCSequence actions:spawn,func, nil];
        [p2Ya1 runAction:seq];
        
    }else{
        [SoundEffect sePlay:@"ofn_se_p2_ya.mp3"];

        id seq = [CCSequence actions:easeYa,func, nil];
        [p2Ya1 runAction:seq];
    }
    
    //オオスノミコトとスサノオの動き
    
    CCSprite *osunoSprite = (CCSprite*)[self getChildByTag:kTagoosunoW];
    CCSprite *susanooSprite = (CCSprite*)[self getChildByTag:kTagsusanoW];
    
    id Moveby = [CCMoveTo actionWithDuration:1.0f position:ccp(660, 0)];
    [osunoSprite runAction:Moveby];
    
    id Moveby2 = [CCMoveTo actionWithDuration:1.0f position:ccp(0, 0)];
    [susanooSprite runAction:Moveby2];
}
-(void) ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{

}

-(void)scheduleTouchHoldCounter:(ccTime)dlt{
    holdCount += dlt;
    
    if(holdCount > 2)
    {
        id Moveto = [CCMoveTo actionWithDuration:1.5f position:ccp(0, 0)];
        [oosunoWalk runAction:Moveto];
        id Moveto2 = [CCMoveTo actionWithDuration:1.5f position:ccp(-660, 0)];
        [susanooWalk runAction:Moveto2];
        [self unschedule:_cmd];
    }
    
}

-(void) ccTouchesEnded:(UITouch *)touch withEvent:(UIEvent *)event{
    // winSize =[[CCDirector sharedDirector] winSize];
    CCLOG(@"ccTouchesEnded");

    id Moveto = [CCMoveTo actionWithDuration:1.5f position:ccp(0, 0)];
    [oosunoWalk runAction:Moveto];
    
    id Moveto2 = [CCMoveTo actionWithDuration:1.5f position:ccp(-660, 0)];
    [susanooWalk runAction:Moveto2];
}

- (void) dealloc
{
    
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
	[super dealloc];
}

@end
