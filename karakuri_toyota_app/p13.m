//
//  p13.m
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/10/26.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "p13.h"


@implementation p13
-(id)init{
    if(self = [super init]){
        self.ignoreAnchorPointForPosition = NO;
        self.isTouchEnabled = YES;
        CGSize winsize = [[CCDirector sharedDirector] winSize];
        
        CCSpriteFrameCache *frameChash = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameChash addSpriteFramesWithFile:@"webbtn.plist"];
        
        CCSpriteBatchNode *frameBach = [CCSpriteBatchNode batchNodeWithFile:@"webbtn.pvr.gz"];
        [self addChild:frameBach];
        
        CCSprite *btn_website1 = [CCSprite spriteWithSpriteFrameName:@"btn_webpage.png"];
        CCSprite *btn_website1_2 = [CCSprite spriteWithSpriteFrameName:@"btn_on_webpage.png"];
        
        CCSprite *btn_website2 = [CCSprite spriteWithSpriteFrameName:@"btn_webpage.png"];
        CCSprite *btn_website2_2 = [CCSprite spriteWithSpriteFrameName:@"btn_on_webpage.png"];

        CCSprite *btn_website3 = [CCSprite spriteWithSpriteFrameName:@"btn_webpage.png"];
        CCSprite *btn_website3_2 = [CCSprite spriteWithSpriteFrameName:@"btn_on_webpage.png"];

        CCSprite *btn_website4 = [CCSprite spriteWithSpriteFrameName:@"btn_webpage.png"];
        CCSprite *btn_website4_2 = [CCSprite spriteWithSpriteFrameName:@"btn_on_webpage.png"];
        
        //website
        
        CCMenuItemSprite * btnItem_web1 = [CCMenuItemSprite itemWithNormalSprite:btn_website1 selectedSprite:btn_website1_2 target:self selector:@selector(webView:)];
        btnItem_web1.position = ccp(380, 185);
        btnItem_web1.tag = 1;
        
        CCMenuItemSprite * btnItem_web2 = [CCMenuItemSprite itemWithNormalSprite:btn_website2 selectedSprite:btn_website2_2 target:self selector:@selector(webView:)];
        btnItem_web2.position = ccp(380, 20);
        btnItem_web2.tag = 2;
        
        
        CCMenuItemSprite * btnItem_web3 = [CCMenuItemSprite itemWithNormalSprite:btn_website3 selectedSprite:btn_website3_2 target:self selector:@selector(webView:)];
        btnItem_web3.position = ccp(380, -60);
        btnItem_web3.tag = 3;
        
        CCMenuItemSprite * btnItem_web4 = [CCMenuItemSprite itemWithNormalSprite:btn_website4 selectedSprite:btn_website4_2 target:self selector:@selector(webView:)];
        btnItem_web4.position = ccp(380, -220);
        btnItem_web4.tag = 4;
        
        
        CCMenu *webMenu = [CCMenu menuWithItems:btnItem_web1,btnItem_web2,btnItem_web3,btnItem_web4,nil];
        
        [webMenu setPosition: ccp(winsize.width/2, winsize.height/2)];
        [self addChild:webMenu];


    
    }
    return self;
}

#pragma mark web view
-(void)webView:(id)sender{
    NSString *path = [[NSString alloc] init];
    if ( [sender tag] == 1) {
        path = [[NSString alloc] initWithFormat:@"http://mitsuie.firebird.jp/home.html"];
        
    }else if ( [sender tag] == 2){
        path = [[NSString alloc] initWithFormat:@"http://blogs.yahoo.co.jp/asteishiguro"];
    }else if ( [sender tag] == 3){
        path = [[NSString alloc] initWithFormat:@"https://www.facebook.com/ToyotaSanageKinoki"];
    }else if ( [sender tag] == 4){
        path = [[NSString alloc] initWithFormat:@"http://pub.ne.jp/yumemi_trunk/"];
    }
    
    
    NSURL *url = [NSURL URLWithString:path];
    NSURLRequest *req = [NSURLRequest requestWithURL:url];
    
    webView_  = [[UIWebView alloc] initWithFrame:CGRectMake(0, 46, 1024, 768)];
    webView_.bounds = CGRectMake(0, 46, 1024, 768);
    
    webView_.scalesPageToFit = YES;
    
    toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 1024, 46)];
    toolBar.translucent = NO;
    //toolBar.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
    
    //インジケーターの追加
    activityInducator_ = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityInducator_.frame = CGRectMake(0, 0, 20, 20);
    UIBarButtonItem *inducator = [[UIBarButtonItem alloc] initWithCustomView:activityInducator_];
    UIBarButtonItem *adjustment =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    //戻るボタン
    UIBarButtonItem *goBackButton = [[UIBarButtonItem alloc]init];
    goBackButton.title = @"閉じる";
    goBackButton.style = UIBarButtonItemStyleBordered;
    goBackButton.target = self;
    goBackButton.action = @selector(goBack);
    
    NSArray *elements = [[NSArray alloc] initWithObjects:goBackButton,adjustment,inducator,nil];
    [adjustment release];
    [inducator release];
    [goBackButton release];
    adjustment = nil;
    inducator = nil;
    goBackButton = nil;
    [toolBar setItems:elements animated:YES];
    [elements release];
    elements = nil;
    [path release];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
                           forView:[[CCDirector sharedDirector] view] cache:YES];
    webView_.delegate =self;
    [[[CCDirector sharedDirector] view] addSubview:webView_];
    [[[CCDirector sharedDirector] view] addSubview:toolBar];
    
    [UIView commitAnimations];
    [webView_ loadRequest:req];
    
}


//webViewを閉じる-------------------------------------------------------------------
- (void)goBack {
	[UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0f];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
                           forView:[[CCDirector sharedDirector] view] cache:YES];
    [webView_ removeFromSuperview];
    [toolBar removeFromSuperview];
    [UIView commitAnimations];
}

-(void)dealloc{
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    
    [webView_ release];
    [activityInducator_ release];
    [super dealloc];
}
@end
