//
//  p2.h
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SoundEffect.h"

@interface p2 : CCLayer {
    CCSprite *road2;
    CCSprite *road;
    CCSprite *p2_ki1;
    CCSprite *p2_ki2;
    
    CCLayer *oosunoWalk;
    CCLayer *susanooWalk;
    float spPersec;
    float spPersec2;
    float spPersec3;
    
    float holdCount;
    
}

@end
