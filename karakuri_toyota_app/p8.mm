//
//  p8.mm
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 13/02/05.
//  Copyright __MyCompanyName__ 2013年. All rights reserved.
//

// Import the interfaces
#import "p8.h"

// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#import "PhysicsSprite.h"
#import "GB2ShapeCache.h"

enum {
	kTagParentNode = 1,
    kTagParentNode2 = 2,
};


#pragma mark - p8

@interface p8()
-(void) initPhysics;
-(void) addNewSpriteAtPosition:(CGPoint)p;
@end

@implementation p8

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	p8 *layer = [p8 node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}

-(id) init
{
	if( (self=[super init])) {
        
		// enable events
		self.ignoreAnchorPointForPosition = NO;
		self.isTouchEnabled = YES;
		self.isAccelerometerEnabled = NO;
        CGSize winsize = [[CCDirector sharedDirector] winSize];
        routNo = 1;
        routCount = 1;
        
        red =128;
        green = 255;
        blue = 0;


        //CGSize s = [CCDirector sharedDirector].winSize;
		
		// init physics
		[self initPhysics];
		
		// create reset button
		//[self createMenu];
		
		//Set up sprite
        cloudFrameCash = [CCSpriteFrameCache sharedSpriteFrameCache];
        [cloudFrameCash addSpriteFramesWithFile:@"p8_Resource.plist"];
        
        cloudBatch = [CCSpriteBatchNode batchNodeWithFile:@"p8_Resource.pvr.gz"];
        [self addChild:cloudBatch];
        
        p8mountain = [CCSprite spriteWithSpriteFrameName:@"p8_mount.png"];
        p8mountain.position = ccp(winsize.width/2-5, winsize.height/2-8);
        [self addChild:p8mountain];
        
        p8mountain2 = [CCSprite spriteWithSpriteFrameName:@"p8_mount2.png"];
        p8mountain2.position = ccp(winsize.width/2-5, winsize.height/2-8);
        p8mountain2.opacity = 0;
        [self addChild:p8mountain2];
        
        p8mountain3 = [CCSprite spriteWithSpriteFrameName:@"p8_mount3.png"];
        p8mountain3.position = ccp(winsize.width/2-5, winsize.height/2-8);
        p8mountain3.opacity = 0;
        [self addChild:p8mountain3];
        
        p8mountain4 = [CCSprite spriteWithSpriteFrameName:@"p8_mount4.png"];
        p8mountain4.position = ccp(winsize.width/2-5, winsize.height/2-8);
        p8mountain4.opacity = 0;
        [self addChild:p8mountain4];
        
        p8mountain5 = [CCSprite spriteWithSpriteFrameName:@"p8_mount5.png"];
        p8mountain5.position = ccp(winsize.width/2-5, winsize.height/2-8);
        p8mountain5.opacity = 0;
        [self addChild:p8mountain5];
        
        p8mountain6 = [CCSprite spriteWithSpriteFrameName:@"p8_mount6.png"];
        p8mountain6.position = ccp(winsize.width/2-5, winsize.height/2-8);
        p8mountain6.opacity = 0;
        [self addChild:p8mountain6];
        
        
        
        
        
    
        
#if 1
		// Use batch node. Faster
        CCSpriteFrameCache *gearFrame = [CCSpriteFrameCache sharedSpriteFrameCache];
        [gearFrame addSpriteFramesWithFile:@"p8_gear.plist"];
        CCSpriteBatchNode *parent = [CCSpriteBatchNode batchNodeWithFile:@"p8_gear.pvr.gz"];
        
        CCSpriteFrameCache *gearFrame2 = [CCSpriteFrameCache sharedSpriteFrameCache];
        [gearFrame2 addSpriteFramesWithFile:@"p8_gear2.plist"];
        CCSpriteBatchNode *parent2 = [CCSpriteBatchNode batchNodeWithFile:@"p8_gear2.pvr.gz"];
        
		spriteTexture_ = [parent texture];
		spriteTexture2_ = [parent2 texture];
#else
		// doesn't use batch node. Slower
		spriteTexture_ = [[CCTextureCache sharedTextureCache] addImage:@"p8_gear_center.png"];
		CCNode *parent = [CCNode node];
        
        spriteTexture2_ = [[CCTextureCache sharedTextureCache] addImage:@"p8_gear2.png"];
		CCNode *parent2 = [CCNode node];
#endif
		[self addChild:parent z:2 tag:kTagParentNode];
		[self addChild:parent2 z:3 tag:kTagParentNode2];
        
        //gearSprite = [CCSprite spriteWithSpriteFrameName:@"p8_gear.png"];
        //[self addChild:gearSprite];
		
        //gearSpriteCenter = [CCSprite spriteWithSpriteFrameName:@"p8_gear2.png"];
        //[self addChild:gearSpriteCenter];
        
        [self addNewSpriteAtPosition:ccp(CCRANDOM_0_1()*1000, CCRANDOM_0_1()*150)];
        
        
        [self scheduleUpdate];
	}
	return self;
}

-(void) dealloc
{
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
	delete world;
	world = NULL;
	
	delete m_debugDraw;
	m_debugDraw = NULL;
	
	[super dealloc];
}


-(void) initPhysics
{
	
	CGSize s = [[CCDirector sharedDirector] winSize];
	
	b2Vec2 gravity;
	gravity.Set(20.0, 0.0f);
    bool doSleep = false;

	world = new b2World(gravity,doSleep);
	
	// Do we want to let bodies sleep?
	
	world->SetContinuousPhysics(true);
	
	m_debugDraw = new GLESDebugDraw( PTM_RATIO );
	world->SetDebugDraw(m_debugDraw);
	/*
	uint32 flags = 0;
    flags += b2Draw::e_shapeBit;
    flags += b2Draw::e_jointBit;
    flags += b2Draw::e_aabbBit;
    flags += b2Draw::e_pairBit;
    flags += b2Draw::e_centerOfMassBit;
	m_debugDraw->SetFlags(flags);
	*/
	
	// Define the ground body.
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0, 0); // bottom-left corner
	
	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	groundBody = world->CreateBody(&groundBodyDef);
	
	// Define the ground box shape.
	b2EdgeShape groundBox;
	
	// bottom
	
	groundBox.Set(b2Vec2(0,0), b2Vec2(s.width/PTM_RATIO,0));
	groundBody->CreateFixture(&groundBox,0);
	
	// top
	groundBox.Set(b2Vec2(0,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO));
	groundBody->CreateFixture(&groundBox,0);
	
	// left
	groundBox.Set(b2Vec2(0,s.height/PTM_RATIO), b2Vec2(0,0));
	groundBody->CreateFixture(&groundBox,0);
	
	// right
	groundBox.Set(b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,0));
	groundBody->CreateFixture(&groundBox,0);
}

-(void) draw
{
	//
	// IMPORTANT:
	// This is only for debug purposes
	// It is recommend to disable it
	//
	[super draw];
	
	ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
	
	kmGLPushMatrix();
	
	world->DrawDebugData();
	
	kmGLPopMatrix();
}

-(void) addNewSpriteAtPosition:(CGPoint)p
{
    
    //CCLOG(@"Add sprite %0.2f x %02.f",p.x,p.y);
	CCNode *parent = [self getChildByTag:kTagParentNode];
	CCNode *parent2 = [self getChildByTag:kTagParentNode2];
    
	//We have a 64x64 sprite sheet with 4 different 32x32 images.  The following code is
	//just randomly picking one of the images
	PhysicsSprite *sprite = [PhysicsSprite spriteWithTexture:spriteTexture_ rect:CGRectMake(2,2.5,159,718)];
	[parent addChild:sprite];
    sprite.position = ccp( p.x, p.y);
    
    PhysicsSprite *sprite2 = [PhysicsSprite spriteWithTexture:spriteTexture2_ rect:CGRectMake(2,1,32,32)];
	[parent2 addChild:sprite2];
    sprite2.position = ccp( p.x, p.y);
    
    
    //float boxhsize = gearSprite.contentSize.height / PTM_RATIO*0.5f;
    //float boxwsize = gearSprite.contentSize.width / PTM_RATIO*0.5f;
    
    //float boxhsize2 = gearSpriteCenter.contentSize.height / PTM_RATIO*0.5f;
    //float boxwsize2 = gearSpriteCenter.contentSize.width / PTM_RATIO*0.5f;
	
	// Define the dynamic body.
    
    b2CircleShape circle1;
    circle1.m_radius = 2.0f;
    
    b2CircleShape circle2;
    circle2.m_radius = 0.5f;
    
    b2PolygonShape box;
    box.SetAsBox(0.5f, 10.0f);
    
    b2BodyDef bd1;
    bd1.type = b2_dynamicBody;
    bd1.position.Set(16.0f, 12.0f);
    bd1.angularDamping = 10.0f;
    body1 = world->CreateBody(&bd1);
    
    
    [[GB2ShapeCache sharedShapeCache] addShapesWithFile:@"p8_gear_polygon.plist"];
    [[GB2ShapeCache sharedShapeCache] addFixturesToBody:body1 forShapeName:@"p8_polygon"];
    
    
    jd1.bodyA = groundBody;
    jd1.bodyB = body1;
    jd1.enableLimit = true;
    jd1.localAnchorA = bd1.position;
    jd1.localAnchorB = body1->GetLocalPoint(bd1.position);
    jd1.referenceAngle = body1->GetAngle() - groundBody->GetAngle();
    jd1.upperAngle = -0.05;
    jd1.lowerAngle = -6.28f*20;
    m_joint1 = (b2RevoluteJoint*)world->CreateJoint(&jd1);
    
    
    b2BodyDef bd2;
    bd2.type = b2_dynamicBody;
    bd2.position.Set(17.2f, 12.0f);
    bd2.angularDamping = 10.0f;
    body2 = world->CreateBody(&bd2);
    
    b2FixtureDef fixtureDef2;
    fixtureDef2.shape = &circle2;
    fixtureDef2.density = 1.0f;
    fixtureDef2.restitution = 0.01f;
    fixtureDef2.filter.maskBits = 0x0000;
	body2->CreateFixture(&fixtureDef2);
    
    
    b2RevoluteJointDef jd2;
    jd2.Initialize(groundBody, body2, bd2.position);
    jd2.collideConnected = false;
    m_joint2 = (b2RevoluteJoint*)world->CreateJoint(&jd2);
    
    
    b2GearJointDef jd4;
    jd4.bodyA = body1;
    jd4.bodyB = body2;
    jd4.joint1 = m_joint1;
    jd4.joint2 = m_joint2;
    jd4.ratio = 1.0f;
    m_joint4 = (b2GearJoint*)world->CreateJoint(&jd4);
    
    [sprite setPhysicsBody:body1];
    [sprite2 setPhysicsBody:body2];
    
    
    
}
-(void) p8AddCloud:(int) no{
    
    if(no == 1 && !boolAmagumi1){
        int spriteNo = CCRANDOM_0_1()*4+1;
        //int positionWidth = CCRANDOM_0_1()*170;
        int positionWidth = CCRANDOM_0_1()*1024;

        int positionHeight = CCRANDOM_0_1()*180;
        NSString *path = [NSString stringWithFormat:@"p8_amagumo%d.png",spriteNo];
        CCSprite *amagumo1 = [CCSprite spriteWithSpriteFrameName:path];
        amagumo1.position =ccp(positionWidth, 630+positionHeight);
        amagumo1.scale = 0.0;
        [self addChild:amagumo1 z:1];
        
        id Scale = [CCScaleTo actionWithDuration:0.5 scale:1.0];
        id Ease = [CCEaseBounceOut actionWithAction:Scale];
        
        [amagumo1 runAction:Ease];

        if (routNo < 12*routCount){
            routNo += 1;
            contBool =false;
        }
        [SoundEffect sePlay:@"ofn_se_p8_pon.mp3"];

        if (routNo == 12*routCount && !contBool) {
            routCount += 1;
            contBool =true;
        }


    }
}

-(void) update: (ccTime) dt
{
	//It is recommended that a fixed time step is used with Box2D for stability
	//of the simulation, however, we are using a variable time step here.
	//You need to make an informed choice, the following URL is useful
	//http://gafferongames.com/game-physics/fix-your-timestep/
	
	int32 velocityIterations = 8;
	int32 positionIterations = 1;
    
	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	world->Step(dt, velocityIterations, positionIterations);
    
    

    
}

- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	//Add a new body/atlas sprite at the touched location
    
    if (_mouseJoint != NULL) return;
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    for (b2Body* b = world->GetBodyList(); b; b = b->GetNext()){
        if (b->GetType() == b2_dynamicBody) {
            for (b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext()) {
                if (f->TestPoint(locationWorld)) {
                    b2MouseJointDef md;
                    md.bodyA = groundBody;
                    md.bodyB = b;
                    md.target = locationWorld;
                    md.collideConnected = true;
                    md.maxForce = 50000.0f * b->GetMass();
                    
                    _mouseJoint = (b2MouseJoint *)world->CreateJoint(&md);
                    b->SetAwake(true);
                }
            }
        }
    }
}

-(void)ccTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint == NULL) return;


    //if(routNo <= 300){
    int angle = -CC_RADIANS_TO_DEGREES(m_joint1->GetJointAngle());
    
    //CCLOG(@"routCount@%d",-CC_RADIANS_TO_DEGREES(m_joint1->GetJointAngle()));

    
    if(30*routNo <= angle){
        [self p8AddCloud:1];
        [SoundEffect sePlay:@"ofn_se_p8_kata.mp3"];

    }
    if (routCount <= 4) {
        id Fade = [CCFadeTo actionWithDuration:1.0 opacity:255];
        [p8mountain2 runAction:Fade];
    }else if (routCount <= 8) {
        id Fade = [CCFadeTo actionWithDuration:1.0 opacity:255];
        [p8mountain3 runAction:Fade];
    }else if (routCount <= 12) {
        id Fade = [CCFadeTo actionWithDuration:1.0 opacity:255];
        [p8mountain4 runAction:Fade];
    }else if (routCount <= 16) {
        id Fade = [CCFadeTo actionWithDuration:1.0 opacity:255];
        [p8mountain5 runAction:Fade];
    }else if (routCount <= 20) {
        id Fade = [CCFadeTo actionWithDuration:1.0 opacity:255];
        [p8mountain6 runAction:Fade];
    }
    
    
    UITouch *myTouch = [touches anyObject];
    CGPoint location = [myTouch locationInView:[myTouch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    b2Vec2 locationWorld = b2Vec2(location.x/PTM_RATIO, location.y/PTM_RATIO);
    
    _mouseJoint->SetTarget(locationWorld);
    
}

-(void)ccTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if (_mouseJoint) {
        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
    
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if (_mouseJoint) {
        world->DestroyJoint(_mouseJoint);
        _mouseJoint = NULL;
    }
}


/*
#pragma mark GameKit delegate

-(void) achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

-(void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}
 */

@end
