#import "MasterScene.h"
#import "ScenesManager.h"


@implementation MasterScene
+ (CCScene *)sceneWithSceneId:(NSInteger)sceneId nextBool:(BOOL)nextPage {
    CCScene *scene = [MasterScene node];
    CCLayer *layer = [FirstLayer layerWithSceneId:sceneId];
    
    [scene addChild:layer];
    return scene;
}
//@synthesize menuObj;
@end

#pragma mark -


//FirstLayerクラス
@implementation FirstLayer
#pragma mark インスタンスの初期化/解放

// インスタンスの初期化を行うメソッド。
// このシーン（レイヤー）で使用するオブジェクトを作成・初期化します。
+ (id)layerWithSceneId:(NSInteger)sceneId{
    return [[[self alloc] initWithSceneId:sceneId] autorelease];
}

-(id)initWithSceneId:(NSInteger)sceneId{
    self = [super init];
    if(self){
        self.tag = sceneId;
        sceneId2 = sceneId;
        self.isTouchEnabled = YES;
        

        [MenuObject Pagenum:sceneId2];
        
        //画面サイズ
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        
        if(sceneId2 != 0){
            self.menuObj = [MenuObject node];
            self.menuObj.position = ccp(-winSize.width/2+40,-winSize.height/2+40);
            [self addChild:self.menuObj z:100];
        }
        
        
        
        //背景の設置
        NSString *backgroundPath = [[NSString alloc] init];
        if(sceneId2 == 0){
            backgroundPath = [NSString stringWithFormat:@"index.pvr.gz"];
        }else if(sceneId2 == 7 || sceneId == 15){
            backgroundPath = [NSString stringWithFormat:@"p2.pvr.gz"];
        }else if(sceneId2 == 12){
            backgroundPath = [NSString stringWithFormat:@"p12.pvr.gz"];

        }else if(sceneId2 == 13){
            backgroundPath = [NSString stringWithFormat:@"credit.pvr.gz"];
        }else if(sceneId2 == 14){
            backgroundPath = [NSString stringWithFormat:@"credit2.pvr.gz"];
        }else{
            backgroundPath = [NSString stringWithFormat:@"p%d.pvr.gz",sceneId2];
        }
       
        CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:backgroundPath];
        spriteBack = [CCSprite spriteWithTexture:background rect:CGRectMake(0,0,winSize.width,winSize.height)];
        spriteBack.anchorPoint = CGPointMake(0.0f, 0.0f);
        [self addChild: spriteBack];
        
        
         /*
        デバイスの回転を感知する
        if (sceneId2 == 5) {
            
        }else{
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
            [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
        }
        */
        if(sceneId2 == 0){
            self.p0 = [p0 node];
            self.p0.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p0];
        }
 
        if(sceneId2 == 1){
            self.p1 = [p1 node];
            self.p1.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p1];
        }
        if(sceneId2 == 2){
            self.p2 = [p2 node];
            self.p2.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p2];
        }
        if(sceneId2 == 3){
            self.p3 = [p3 node];
            self.p3.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p3];
        }
        if (sceneId2 == 4) {
            self.p4 = [p4 node];
            self.p4.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p4];

        }
        if (sceneId2 == 5) {
            self.p5 = [p5 node];
            self.p5.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p5];

        }
        if (sceneId2 == 6) {
            self.p6 = [p6 node];
            self.p6.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p6];
            
        }
        if (sceneId2 == 7) {
            self.p7 = [p7 node];
            self.p7.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p7];
            
        }
        if (sceneId2 == 8) {
            self.p8 = [p8 node];
            self.p8.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p8];
        }
        if (sceneId2 == 9) {
            self.p9 = [p9 node];
            self.p9.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p9];
        }
        if (sceneId2 == 10) {
            self.p10 = [p10 node];
            self.p10.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p10];
        }
        
        if (sceneId2 == 11) {
            self.p11 = [p11 node];
            self.p11.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p11];
        }

        if (sceneId2 == 13) {
            self.p13 = [p13 node];
            self.p13.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p13];
        }
        if (sceneId2 == 14) {
            self.p14 = [p14 node];
            self.p14.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p14];
        }
        if (sceneId2 == 15) {
            self.p15 = [p15 node];
            self.p15.position = ccp(winSize.width/2,winSize.height/2);
            [self addChild:self.p15];
        }
    }
    return self;

}
-(void) onExitTransitionDidStart{
    [self FadeOut];

}
-(void) onEnterTransitionDidFinish{
    //ナレーション
    [super onEnterTransitionDidFinish];
    [self narration];
}

-(void) onExit{
    [super onExit];
}

#pragma mark ナレーション
-(void)narration{
    NSUserDefaults *userDefault =[NSUserDefaults standardUserDefaults];
    int setting_narrar = [userDefault integerForKey:@"SETTING2"];
    
    if(!setting_narrar){
        NSString *narrationMp3 = [[NSString alloc] initWithFormat:@"narration%d.mp3",sceneId2 ];
        playnarra = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Left];

        [playnarra load:narrationMp3];
        [playnarra setNumberOfLoops:0];
        playnarra.volume = 1.0;
        playnarra.delegate = self;
        [playnarra play];

        [narrationMp3 release];

    }
}
-(void)cdAudioSourceDidFinishPlaying:(CDLongAudioSource *)audioSource{
    if(sceneId2 > 0 ){
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        CCSprite *tugihezButton = [CCSprite spriteWithSpriteFrameName:@"tugihe.png"];
        tugihezButton.position = ccp(winSize.width/2+175,40);
        tugihezButton.scale = 0.0f;
        [self addChild:tugihezButton z:14];
        
        id scale =[CCScaleTo actionWithDuration:0.5f scale:1.0];
        id ease = [CCEaseBounceOut actionWithAction:scale];
        [tugihezButton runAction:ease];
    }
    
}




#pragma mark タッチイベント
-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch  = [touches anyObject];
    CGPoint location = [touch locationInView:[touch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];


}

#pragma mark イベント処理

-(void)FadeOut{
    CDLongAudioSource *player2 = [[CDAudioManager sharedManager] audioSourceForChannel:kASC_Left];
    CDLongAudioSourceFader* fader = [[CDLongAudioSourceFader alloc] init:player2 interpolationType:kIT_Exponential startVal:player2.volume endVal:0.0];
    [fader setStopTargetWhenComplete:YES];
    //Create a property modifier action to wrap the fader
    CDXPropertyModifierAction* action = [CDXPropertyModifierAction actionWithDuration:1.0 modifier:fader];
    [fader release];//Action will retain
    id func = [CCCallBlock actionWithBlock:^{
    }];
    [[CCDirector sharedDirector].actionManager  addAction:[CCSequence actions:action,func, nil] target:player2 paused:NO];
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
	[super dealloc];
}
@end
