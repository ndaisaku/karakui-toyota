//
//  p1.m
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "p1.h"

@implementation p1
-(id)init{
    if( (self=[super init])) {
        self.ignoreAnchorPointForPosition = NO;
        self.isTouchEnabled = YES;

        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"p1_gimmickImage.plist"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"p1_gimmickImage.pvr.gz"];
        [self addChild: spriteSheet];

        osuno = [CCSprite spriteWithSpriteFrameName:@"ousuno.png"];
        osuno.position = ccp(655,450);
        [self addChild: osuno z:10];
        
        
        susanoo = [CCSprite spriteWithSpriteFrameName:@"susanoo.png"];
        susanoo.position = ccp(370,447);
        [self addChild: susanoo z:11];
 
        
        
        hasami2 = [CCSprite spriteWithSpriteFrameName:@"hasami.png"];
        hasami2.position = ccp(655,450);
        hasami2.opacity = 0.0;
        hasami2.anchorPoint = CGPointMake(0,1);
        hasami2.rotation = 60;
        
        fude = [CCSprite spriteWithSpriteFrameName:@"fude.png"];
        fude.position = ccp(660,460);
        fude.opacity = 0.0;
        fude.anchorPoint = CGPointMake(0,1);
        fude.rotation = 60;
        
        hon = [CCSprite spriteWithSpriteFrameName:@"hon.png"];
        hon.position = ccp(665,450);
        hon.opacity = 0.0;
        hon.anchorPoint = CGPointMake(0,1);
        hon.rotation = 60;
        
        hiyoko = [CCSprite spriteWithSpriteFrameName:@"hiyoko.png"];
        hiyoko.opacity = 0.0;
        hiyoko.position = ccp(660,460);
        hiyoko.anchorPoint = CGPointMake(0,0);
        hiyoko.rotation = 100;
        
        taimatu = [CCSprite spriteWithSpriteFrameName:@"taimatu.png"];
        taimatu.position = ccp(350,465);
        taimatu.opacity = 0.0;
        taimatu.anchorPoint = CGPointMake(1,0);
        taimatu.rotation = -100;
        
        alley = [CCSprite spriteWithSpriteFrameName:@"alley.png"];
        alley.position = ccp(338,455);
        alley.opacity = 0.0;
        alley.anchorPoint = CGPointMake(1,1);
        alley.rotation = -60;
        
        niku = [CCSprite spriteWithSpriteFrameName:@"niku.png"];
        niku.opacity = 0.0;
        niku.position = ccp(334,458);
        niku.anchorPoint = CGPointMake(1,1);
        niku.rotation = -60;
        
        ya = [CCSprite spriteWithSpriteFrameName:@"ya.png"];
        ya.opacity = 0.0;
        ya.position = ccp(338,450);
        ya.anchorPoint = CGPointMake(1,1);
        ya.rotation = -60;
        
        [self addChild: fude];
        [self addChild: hasami2];
        [self addChild: hon];
        [self addChild: hiyoko];
        
        [self addChild: taimatu];
        [self addChild: alley];
        [self addChild: niku];
        [self addChild: ya];
    }
    return self;
}

-(void)move:(int)touchNo{
    id rote = [CCRotateTo actionWithDuration:1.0f angle:0];
    id rote2 = [CCRotateTo actionWithDuration:1.0f angle:0];
    id rote3 = [CCRotateTo actionWithDuration:1.0f angle:0];
    id rote4 = [CCRotateTo actionWithDuration:1.0f angle:0];
    id rote5_1 = [CCRotateTo actionWithDuration:0.1f angle:-10];
    id rote5_2 = [CCRotateTo actionWithDuration:0.3f angle:0];
    
    CCEaseBounceOut *bounce = [CCEaseBounceOut actionWithAction:rote];
    CCEaseBounceOut *bounce2 = [CCEaseBounceOut actionWithAction:rote2];
    CCEaseBounceOut *bounce3 = [CCEaseBounceOut actionWithAction:rote3];
    CCEaseBounceOut *bounce4 = [CCEaseBounceOut actionWithAction:rote4];
    CCEaseBounceOut *bounce5 = [CCEaseBounceOut actionWithAction:rote5_2];
    
    id seq = [CCSequence actions:rote5_1,bounce5, nil];
    
    switch (touchNo) {
        case 1:
            hasami2.opacity = 255.0;
            [hasami2 runAction:bounce];
            break;
        case 2:
            fude.opacity = 255.0;
            [fude runAction:bounce2];
            break;
        case 3:
            hon.opacity = 255.0;
            [hon runAction:bounce3];
            break;
        case 4:
            hiyoko.opacity = 255.0;
            [hiyoko runAction:bounce4];
            break;
        default:
            break;
    }
    
    if (p1_touchNo1 >4) {
        [hiyoko runAction:seq];
    }
    
    
    
}

-(void)move2:(int)touchNo{
    id rote = [CCRotateTo actionWithDuration:1.0f angle:0];
    id rote2 = [CCRotateTo actionWithDuration:1.0f angle:0];
    id rote3 = [CCRotateTo actionWithDuration:1.0f angle:0];
    id rote4 = [CCRotateTo actionWithDuration:1.0f angle:0];
    id rote5_1 = [CCRotateTo actionWithDuration:0.1f angle:-10];
    id rote5_2 = [CCRotateTo actionWithDuration:0.3f angle:0];
    
    CCEaseBounceOut *bounce = [CCEaseBounceOut actionWithAction:rote];
    CCEaseBounceOut *bounce2 = [CCEaseBounceOut actionWithAction:rote2];
    CCEaseBounceOut *bounce3 = [CCEaseBounceOut actionWithAction:rote3];
    CCEaseBounceOut *bounce4 = [CCEaseBounceOut actionWithAction:rote4];
    CCEaseBounceOut *bounce5 = [CCEaseBounceOut actionWithAction:rote5_2];
    
    id seq = [CCSequence actions:rote5_1,bounce5, nil];

    

    switch (touchNo) {
        case 1:
            ya.opacity = 255.0;
            [ya runAction:bounce];
            break;
        case 2:
            alley.opacity = 255.0;
            [alley runAction:bounce2];
            break;
        case 3:
            niku.opacity = 255.0;
            [niku runAction:bounce3];
            break;
        case 4:
            taimatu.opacity = 255.0;
            [taimatu runAction:bounce4];
            break;
        default:
            break;
    }
    if (p1_touchNo2 >4) {
        [taimatu runAction:seq];
    }
    
}

#pragma mark タッチイベント
-(CGRect)spriteTouch:(CCSprite *) sprite{
    float h = [sprite contentSize].height;
    float w = [sprite contentSize].width;
    float x = sprite.position.x-w/2;
    float y = sprite.position.y-h/2;
    CGRect rect = CGRectMake(x, y, w, h);
    
    return rect;
}

-(CGRect)spriteTouch2:(CCSprite *) sprite{
    float h = [sprite contentSize].height;
    float w = [sprite contentSize].width;
    float x = sprite.position.x-w/2;
    float y = sprite.position.y;
    CGRect rect = CGRectMake(x, y, w, h);
    
    return rect;
}

-(void) ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

    UITouch *touch  = [touches anyObject];
    CGPoint location = [touch locationInView:[touch view]];
    location = [[CCDirector sharedDirector] convertToGL:location];
    
    CGRect obj01Rect = [self spriteTouch:osuno];
    
    if(CGRectContainsPoint(obj01Rect, location) && p1_touchNo1 < 4){
        p1_touchNo1++;
        [self move:p1_touchNo1];
        [SoundEffect sePlay:@"ofn_se_p1_ohusuvoice.mp3"];

    }else if(CGRectContainsPoint(obj01Rect, location) && p1_touchNo1 >= 4){
        p1_touchNo1 = 5;
        [self move:p1_touchNo1];
        [SoundEffect sePlay:@"ofn_se_p1_hiyoko1.mp3"];
    }
    
    CGRect obj02Rect = [self spriteTouch:susanoo];
    
    if(CGRectContainsPoint(obj02Rect, location) && p1_touchNo2 < 4){
        p1_touchNo2++;
        [self move2:p1_touchNo2];
        [SoundEffect sePlay:@"ofn_se_p1_yamatovoice.mp3"];

    }else if(CGRectContainsPoint(obj02Rect, location) && p1_touchNo2 >= 4){
        p1_touchNo2 = 5;
        [self move2:p1_touchNo2];
        [SoundEffect sePlay:@"ofn_se_p1_honou.mp3"];
    }
}

- (void) dealloc
{
    
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
	[super dealloc];
}

@end
