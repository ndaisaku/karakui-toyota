//
//  p5.m
//  karakuri_toyota_app
//
//  Created by 中西 大作 on 2013/07/18.
//  Copyright 2013年 __MyCompanyName__. All rights reserved.
//

#import "p5.h"


@implementation p5
CGSize winSize;

-(id)init{
    if (self = [super init]) {

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
         
        //回転のしたかの判断
        p5_count_right = false;
        p5_count_left = false;
        p5_count_top = false;
        
        
        self.ignoreAnchorPointForPosition = NO;
        
        winSize = [[CCDirector sharedDirector] winSize];
        
        CCTexture2D *background = [[CCTextureCache sharedTextureCache] addImage:@"p5.pvr.gz"];
        spriteBack = [CCSprite spriteWithTexture:background rect:CGRectMake(0,0,winSize.width,winSize.height)];
        spriteBack.position = ccp(winSize.width/2,0);
        spriteBack.anchorPoint = CGPointMake(0.5f, 0.0f);
        [self addChild: spriteBack];
        
        CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
        [frameCache addSpriteFramesWithFile:@"p5_sozai.plist"];
        CCSpriteBatchNode *spriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"p5_sozai.pvr.gz"];
        [self addChild: spriteSheet];
        
        
        
        spriteImage[0] = [CCSprite spriteWithSpriteFrameName:@"p5_oosuno1.png"];
        spriteImage[0].position = ccp(winSize.width/2,winSize.height*0.43);
        [self addChild: spriteImage[0]];
        
        spriteImage[1]  = [CCSprite spriteWithSpriteFrameName:@"p5_ono.png"];
        spriteImage[1].anchorPoint = ccp(0, 0.8);
        spriteImage[1].position = ccp(winSize.width*0.872,winSize.height*0.705);
        [self addChild: spriteImage[1]];
        
        
        spriteImage[2]  = [CCSprite spriteWithSpriteFrameName:@"p5_kanaduchi.png"];
        spriteImage[2].anchorPoint = ccp(0.52, 0.9);
        spriteImage[2].position = ccp(winSize.width*0.465,winSize.height*0.85);
        [self addChild: spriteImage[2]];
        
        spriteImage[3]  = [CCSprite spriteWithSpriteFrameName:@"p5_kuwa.png"];
        spriteImage[3].anchorPoint = ccp(1.0, 0.15);
        spriteImage[3].position = ccp(winSize.width*0.125,winSize.height*0.30);
        [self addChild: spriteImage[3]];
        
        spriteImage[4]  = [CCSprite spriteWithSpriteFrameName:@"p5_oosuno2.png"];
        spriteImage[4].position = ccp(winSize.width*0.46,winSize.height*0.81);
        [self addChild: spriteImage[4]];
        
        spriteImage[5]  = [CCSprite spriteWithSpriteFrameName:@"p5_snake.png"];
        spriteImage[5].position = ccp(winSize.width*0.15,winSize.height*0.13);
        id scale = [CCScaleTo actionWithDuration:0.0 scale:0.5];
        [self addChild: spriteImage[5]];
        [spriteImage[5] runAction:scale];
        
        //斧葉っぱ
        spriteImage[9]  = [CCSprite spriteWithSpriteFrameName:@"p5_ono_leaf1.png"];
        spriteImage[9].anchorPoint = ccp(1.0f,0.5f);
        spriteImage[9].opacity = 0;
        spriteImage[9].position = ccp(winSize.width*0.935,winSize.height*0.60);
        [self addChild: spriteImage[9]];
        
        spriteImage[10]  = [CCSprite spriteWithSpriteFrameName:@"p5_ono_leaf2.png"];
        spriteImage[10].anchorPoint = ccp(1.0f,0.5f);
        spriteImage[10].opacity = 0;
        spriteImage[10].position = ccp(winSize.width*0.935,winSize.height*0.53);
        [self addChild: spriteImage[10]];
        
        
        //金槌葉っぱ
        spriteImage[11]  = [CCSprite spriteWithSpriteFrameName:@"p5_kanazuchi_leaf.png"];
        spriteImage[11].opacity = 0;
        spriteImage[11].position = ccp(winSize.width*0.58,winSize.height*0.86);
        [self addChild: spriteImage[11]];
        
        spriteImage[12]  = [CCSprite spriteWithSpriteFrameName:@"p5_kama_leaf.png"];
        spriteImage[12].opacity = 0;
        spriteImage[12].position = ccp(winSize.width*0.35,winSize.height*0.88);
        [self addChild: spriteImage[12]];
        
        //大根
        spriteImage[13]  = [CCSprite spriteWithSpriteFrameName:@"p5_daikon.png"];
        spriteImage[13].position = ccp(winSize.width*0.05,winSize.height*0.715);
        [self addChild: spriteImage[13]];
        
        spriteImage[14]  = [CCSprite spriteWithSpriteFrameName:@"p5_daikon_kakushi.png"];
        spriteImage[14].position = ccp(spriteImage[14].contentSize.width/2,winSize.height*0.71);
        [self addChild: spriteImage[14]];
        
        
        //太陽
        spriteImage[7]  = [CCSprite spriteWithSpriteFrameName:@"p5_sun.png"];
        spriteImage[7].anchorPoint = ccp(0.0f,0.5f);
        spriteImage[7].position = ccp(winSize.width*0.5,winSize.height*0.4);
        [self addChild: spriteImage[7]];
        
        //月
        spriteImage[8]  = [CCSprite spriteWithSpriteFrameName:@"p5_moon.png"];
        spriteImage[8].anchorPoint = ccp(1.0f,0.5f);
        spriteImage[8].position = ccp(winSize.width*0.5,winSize.height*0.4);
        [self addChild: spriteImage[8]];
    }
    return self;
}

-(void)p5_Action:(int) no{
    
    
    //斧が地面についた時
    id OnoLeafFadeIn1 = [CCFadeIn actionWithDuration:0.2];
    id OnoLeafFadeOut1 = [CCFadeOut actionWithDuration:0.2];
    
    id OnoLeafFadeIn2 = [CCFadeIn actionWithDuration:0.2];
    id OnoLeafFadeOut2 = [CCFadeOut actionWithDuration:0.2];

    id OnoLeafMove1 = [CCMoveTo actionWithDuration:0.5 position:ccp(spriteImage[9].position.x-30, spriteImage[9].position.y+10)];
    id OnoLeafMove2 = [CCMoveTo actionWithDuration:0.5 position:ccp(spriteImage[10].position.x-30, spriteImage[10].position.y-10)];
    
    id OnoLeafMove3 = [CCMoveTo actionWithDuration:0 position:ccp(spriteImage[9].position.x+30, spriteImage[9].position.y-10)];
    id OnoLeafMove4 = [CCMoveTo actionWithDuration:0 position:ccp(spriteImage[10].position.x+30, spriteImage[10].position.y+10)];

    id OnoLeafEase1 = [CCEaseExponentialOut actionWithAction:OnoLeafMove1];
    id OnoLeafEase2 = [CCEaseExponentialOut actionWithAction:OnoLeafMove2];
    
    id OneSpwn1 =[CCSpawn actions:OnoLeafFadeIn1,OnoLeafEase1,OnoLeafFadeOut1,nil];
    id OneSpwn2 =[CCSpawn actions:OnoLeafFadeIn2,OnoLeafEase2,OnoLeafFadeOut2,nil];
    
    id OnoLeafSeq1 = [CCSequence actions:[CCDelayTime actionWithDuration:0.35],OneSpwn1,OnoLeafMove3,nil];
    id OnoLeafSeq2 = [CCSequence actions:[CCDelayTime actionWithDuration:0.35],OneSpwn2,OnoLeafMove4,nil];
    
    id OnoLeafBlock = [CCCallBlock actionWithBlock:^{
        [spriteImage[9] runAction:OnoLeafSeq1];
        [spriteImage[10] runAction:OnoLeafSeq2];
        [SoundEffect sePlay:@"ofn_se_p5_ki.mp3"];

    }];
    
    id rote = [CCRotateTo actionWithDuration:1.0 angle:90];
    id ease = [CCEaseSineOut actionWithAction:rote];
    id rote2 = [CCRotateTo actionWithDuration:0.8 angle:0];
    id ease2 = [CCEaseBounceOut actionWithAction:rote2];
    id spaw = [CCSpawn actions:ease2,OnoLeafBlock, nil];
    id seq = [CCSequence actions:ease,spaw,nil];
    id rep = [CCRepeatForever actionWithAction:seq];
    
    
    //金槌が地面についた時
    id KanadutiLeafFadeIn1 = [CCFadeIn actionWithDuration:0.2];
    id KanadutiLeafFadeOut1 = [CCFadeOut actionWithDuration:0.2];
    
    id KanadutiLeafFadeIn2 = [CCFadeIn actionWithDuration:0.2];
    id KanadutiLeafFadeOut2 = [CCFadeOut actionWithDuration:0.2];
    
    id KanadutiLeafMove1 = [CCMoveTo actionWithDuration:0.5 position:ccp(spriteImage[11].position.x+10, spriteImage[11].position.y-30)];
    id KanadutiLeafMove2 = [CCMoveTo actionWithDuration:0.5 position:ccp(spriteImage[12].position.x-10, spriteImage[12].position.y-30)];
    
    id KanadutiLeafMove1_2 = [CCMoveTo actionWithDuration:0 position:ccp(spriteImage[11].position.x-10, spriteImage[11].position.y+30)];
    id KanadutiLeafMove2_2 = [CCMoveTo actionWithDuration:0 position:ccp(spriteImage[12].position.x+10, spriteImage[12].position.y+30)];
    
    id KanadutiLeafEase1 = [CCEaseExponentialOut actionWithAction:KanadutiLeafMove1];
    id KanadutiLeafEase2 = [CCEaseExponentialOut actionWithAction:KanadutiLeafMove2];
    
    id KanadutiSpwn1 =[CCSpawn actions:KanadutiLeafFadeIn1,KanadutiLeafEase1,KanadutiLeafFadeOut1,nil];
    id KanadutiSpwn2 =[CCSpawn actions:KanadutiLeafFadeIn2,KanadutiLeafEase2,KanadutiLeafFadeOut2,nil];
    
    id KanadutiLeafSeq1 = [CCSequence actions:[CCDelayTime actionWithDuration:0.35],KanadutiSpwn1,KanadutiLeafMove1_2,nil];
    id KanadutiLeafSeq2 = [CCSequence actions:[CCDelayTime actionWithDuration:0.35],KanadutiSpwn2,KanadutiLeafMove2_2,nil];
    
    id KanadutiLeafBlock1 = [CCCallBlock actionWithBlock:^{
        [spriteImage[11] runAction:KanadutiLeafSeq1];

    }];
    
    id KanadutiLeafBlock2 = [CCCallBlock actionWithBlock:^{
        [spriteImage[12] runAction:KanadutiLeafSeq2];


    }];
    
    id KanadutiLeafBlock3 = [CCCallBlock actionWithBlock:^{
        [SoundEffect sePlay:@"ofn_se_p5_kanaduti.mp3"];
    }];
    
    id KanadutiLeafBlock4 = [CCCallBlock actionWithBlock:^{
        [SoundEffect sePlay:@"ofn_se_p5_kama.mp3"];
    }];

    
    
    id rote3 = [CCRotateTo actionWithDuration:1.0 angle:40];
    id ease3 = [CCEaseBounceOut actionWithAction:rote3];
    id rote4 = [CCRotateTo actionWithDuration:0.8 angle:-35];
    id ease4 = [CCEaseBounceOut actionWithAction:rote4];
    id spaw2 = [CCSpawn actions:ease4,KanadutiLeafBlock3, nil];
    id spaw3 = [CCSpawn actions:KanadutiLeafBlock4,ease3, nil];
    id seq2 = [CCSequence actions:spaw3,KanadutiLeafBlock1,spaw2,KanadutiLeafBlock2,nil];
    id rep2 = [CCRepeatForever actionWithAction:seq2];
    
    
    
    //大根
    id KuwaLeafMove1 = [CCMoveBy actionWithDuration:0.5 position:ccp(25, 0)];
    id KuwaLeafEase1 = [CCEaseExponentialOut actionWithAction:KuwaLeafMove1];
    id KuwaLeafSeq = [CCSequence actions:[CCDelayTime actionWithDuration:0.3],KuwaLeafEase1,nil];

    
    id daikon_nukeru = [CCRotateBy actionWithDuration:0.5 angle:-360];
    id daikon_nukeru_ease = [CCEaseSineIn actionWithAction:daikon_nukeru];
    
    ccBezierConfig bezier;
    bezier.controlPoint_1 = ccp(0,0);
    bezier.controlPoint_2 = ccp(100,90);
    bezier.endPosition = ccp(-250,90);
    
    id daikon_nukeru2 = [CCBezierBy actionWithDuration:0.5 bezier:bezier];
    id daikon_nukeru_ease2 = [CCEaseSineIn actionWithAction:daikon_nukeru2];
    
    id daikon_spaw = [CCSpawn actions:daikon_nukeru_ease,daikon_nukeru_ease2,nil];
    id KuwaLeafBlock2 = [CCCallBlock actionWithBlock:^{
        spriteImage[13].position = ccp(winSize.width*0.05,winSize.height*0.715);
    }];
    
    id daikon_seq = [CCSequence actions:[CCDelayTime actionWithDuration:0.2],daikon_spaw,KuwaLeafBlock2,nil];
    
    id KuwaLeafBlock1 = [CCCallBlock actionWithBlock:^{
        daicon_count++;
        CCLOG(@"%d",daicon_count);
        if(daicon_count == 4){
            daicon_count = 0;
            [spriteImage[13] runAction:daikon_seq];
        }else{
            [spriteImage[13] runAction:KuwaLeafSeq];
        }
    }];
    id kuwBlock1 = [CCCallBlock actionWithBlock:^{[SoundEffect sePlay:@"ofn_se_p5_kuwa.mp3"];}];

    id rote5 = [CCRotateTo actionWithDuration:1.0 angle:90];
    id ease5 = [CCEaseSineOut actionWithAction:rote5];
    id rote6 = [CCRotateTo actionWithDuration:0.8 angle:0];
    id ease6 = [CCEaseBounceOut actionWithAction:rote6];
    id spaw4 = [CCSpawn actions:ease6,kuwBlock1,nil];

    id seq3 = [CCSequence actions:ease5,KuwaLeafBlock1,spaw4,nil];
    id rep3 = [CCRepeatForever actionWithAction:seq3];
    
    //画面サイズ

    
    if(no == 1 && !p5_count_bottom){
        [spriteImage[1] runAction:rep];
        spriteImage[11].position = ccp(winSize.width*0.58,winSize.height*0.86);
        spriteImage[12].position = ccp(winSize.width*0.35,winSize.height*0.88);
        spriteImage[13].position = ccp(winSize.width*0.05,winSize.height*0.715);
        
        [spriteImage[2] stopAllActions];
        id rote = [CCRotateTo actionWithDuration:0.8 angle:0];
        id ease = [CCEaseBounceOut actionWithAction:rote];
        [spriteImage[2] runAction:ease];
        
        [spriteImage[3] stopAllActions];
        id rote2 = [CCRotateTo actionWithDuration:0.8 angle:0];
        id ease2 = [CCEaseBounceOut actionWithAction:rote2];
        [spriteImage[3] runAction:ease2];
        
        id rote3 = [CCRotateTo actionWithDuration:0.8f angle:-90];
        id ease3 = [CCEaseElasticOut actionWithAction:rote3];
        [spriteImage[7] runAction:ease3];
        
        id rote4 = [CCRotateTo actionWithDuration:0.8f angle:-90];
        id ease4 = [CCEaseElasticOut actionWithAction:rote4];
        [spriteImage[8] runAction:ease4];
        
        //蛇は一回だけ動きます
        if(!p5_count_left){
            [SoundEffect sePlay:@"ofn_se_p5_snakemove1.caf"];

            id move = [CCMoveBy actionWithDuration:0.8 position:ccp(30, 0)];
            id snake_ease3 = [CCEaseSineOut actionWithAction:move];
            [spriteImage[5] runAction:snake_ease3];

        }
        
        
        p5_count_left = true;

        
    }else if (no == 2 && !p5_count_bottom){
        [spriteImage[2] runAction:rep2];

        //アクションをストップ
        spriteImage[9].position = ccp(winSize.width*0.935,winSize.height*0.60);
        spriteImage[10].position = ccp(winSize.width*0.935,winSize.height*0.53);

        
        [spriteImage[1] stopAllActions];
        id rote = [CCRotateTo actionWithDuration:0.8 angle:0];
        id ease = [CCEaseBounceOut actionWithAction:rote];
        [spriteImage[1] runAction:ease];
        
        [spriteImage[3] stopAllActions];
        id rote2 = [CCRotateTo actionWithDuration:0.8 angle:0];
        id ease2 = [CCEaseBounceOut actionWithAction:rote2];
        [spriteImage[3] runAction:ease2];
        
        id rote3 = [CCRotateTo actionWithDuration:0.8f angle:-180];
        id ease3 = [CCEaseElasticOut actionWithAction:rote3];
        [spriteImage[7] runAction:ease3];
        
        id rote4 = [CCRotateTo actionWithDuration:0.8f angle:-180];
        id ease4 = [CCEaseElasticOut actionWithAction:rote4];
        [spriteImage[8] runAction:ease4];
        
        //蛇は一回だけ動きます
        if(!p5_count_top){
            [SoundEffect sePlay:@"ofn_se_p5_snakemove1.caf"];
            id move = [CCMoveBy actionWithDuration:0.8 position:ccp(30, 0)];
            id snake_ease3 = [CCEaseSineOut actionWithAction:move];
            [spriteImage[5] runAction:snake_ease3];

        }
        p5_count_top = true;
        
    }else if (no == 3 && !p5_count_bottom){
        
        //アクションをストップ
        spriteImage[9].position = ccp(winSize.width*0.935,winSize.height*0.60);
        spriteImage[10].position = ccp(winSize.width*0.935,winSize.height*0.53);
        
        spriteImage[11].position = ccp(winSize.width*0.58,winSize.height*0.86);
        spriteImage[12].position = ccp(winSize.width*0.35,winSize.height*0.88);

        
        [spriteImage[1] stopAllActions];
        id rote = [CCRotateTo actionWithDuration:0.8 angle:0];
        id ease = [CCEaseBounceOut actionWithAction:rote];
        [spriteImage[1] runAction:ease];
        
        [spriteImage[2] stopAllActions];
        id rote2 = [CCRotateTo actionWithDuration:0.8 angle:0];
        id ease2 = [CCEaseBounceOut actionWithAction:rote2];
        [spriteImage[2] runAction:ease2];
        
        id rote3 = [CCRotateTo actionWithDuration:0.8f angle:-270];
        id ease3 = [CCEaseElasticOut actionWithAction:rote3];
        [spriteImage[7] runAction:ease3];
        
        id rote4 = [CCRotateTo actionWithDuration:0.8f angle:-270];
        id ease4 = [CCEaseElasticOut actionWithAction:rote4];
        [spriteImage[8] runAction:ease4];
        
        //蛇は一回だけ動きます
        if(!p5_count_right){
            [SoundEffect sePlay:@"ofn_se_p5_snakemove1.caf"];
            id move = [CCMoveBy actionWithDuration:0.8 position:ccp(30, 0)];
            id snake_ease3 = [CCEaseSineOut actionWithAction:move];
            [spriteImage[5] runAction:snake_ease3];
        }
        
        //大根が飛び出します
        [spriteImage[3] runAction:rep3];

        p5_count_right = true;
        
    }else{
        //アクションをストップ
        [spriteImage[1] stopAllActions];
        id rote = [CCRotateTo actionWithDuration:0.8 angle:0];
        id ease = [CCEaseBounceOut actionWithAction:rote];
        [spriteImage[1] runAction:ease];
        
        [spriteImage[2] stopAllActions];
        id rote2 = [CCRotateTo actionWithDuration:0.8 angle:0];
        id ease2 = [CCEaseBounceOut actionWithAction:rote2];
        [spriteImage[2] runAction:ease2];
        
        [spriteImage[3] stopAllActions];
        id rote3 = [CCRotateTo actionWithDuration:0.8 angle:0];
        id ease3 = [CCEaseBounceOut actionWithAction:rote3];
        [spriteImage[3] runAction:ease3];
        
        id rote5 = [CCRotateTo actionWithDuration:0.8f angle:0];
        id ease5 = [CCEaseElasticOut actionWithAction:rote5];
        [spriteImage[7] runAction:ease5];
        
        id rote4 = [CCRotateTo actionWithDuration:0.8f angle:0];
        id ease4 = [CCEaseElasticOut actionWithAction:rote4];
        [spriteImage[8] runAction:ease4];
        
        
        
        
        
        if(p5_count_right && p5_count_top && p5_count_left && !p5_count_bottom){
            [self removeChild:spriteImage[13] cleanup:YES];
            [self removeChild:spriteImage[14] cleanup:YES];

            p5_count_bottom = true;
            id scale = [CCScaleTo actionWithDuration:0.8 scale:1.4];
            id ease4 = [CCEaseElasticOut actionWithAction:scale];
            id scale2 = [CCScaleTo actionWithDuration:0.8 scale:1.5];
            id move = [CCMoveTo actionWithDuration:0.0 position:ccp(winSize.width/2, winSize.height*0.62)];
            id ease5 = [CCEaseElasticOut actionWithAction:scale2];
            [spriteBack runAction:ease4];
            [spriteImage[0] runAction:ease5];
            [spriteImage[0] runAction:move];
            spriteImage[1].opacity = 0.0;
            spriteImage[2].opacity = 0.0;
            spriteImage[3].opacity = 0.0;
            spriteImage[4].opacity = 0.0;
            spriteImage[5].opacity = 0.0;
            spriteImage[8].opacity = 0.0;
            spriteImage[7].opacity = 0.0;
            
            //蛇アニメーション
            [SoundEffect sePlay:@"ofn_se_p5_snakemove3.caf"];

            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"p5_hebi.plist"];
            bachNood = [CCSpriteBatchNode batchNodeWithFile:@"p5_hebi.png" ];
            [self addChild:bachNood];
            
            NSMutableArray *hebiAnimeFrames =[NSMutableArray array];
            for(int i = 1; i<=2; i++){
                [hebiAnimeFrames addObject:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName: [NSString stringWithFormat:@"p5_snake3_%d.png",i]]];
            }
            CCAnimation *hebiAnime2 = [CCAnimation animationWithSpriteFrames:hebiAnimeFrames delay:0.2f];
            
            CGSize winSize =[CCDirector sharedDirector].winSize;
            CCSprite *hebiSprite = [CCSprite spriteWithSpriteFrameName:@"p5_snake3_1.png"];
            hebiSprite.scaleX = 0.8;
            hebiSprite.scaleY = 0.8;
            hebiSprite.anchorPoint = ccp(0, 0.5);
            hebiSprite.position = ccp(-hebiSprite.contentSize.width, winSize.height*0.125);
            CCAnimate *chameleonAction = [CCAnimate actionWithAnimation:hebiAnime2];
            
            CCAction *hebiAnimation3 = [CCRepeat actionWithAction:chameleonAction times:5.0];
            
            [hebiSprite runAction:hebiAnimation3];
            [bachNood addChild:hebiSprite];
            
            id CallFunc =[CCCallFunc actionWithTarget:self selector:@selector(p5_finish_motion)];
            
            ccBezierConfig config;
            config.controlPoint_1 = ccp(0, 0);
            config.controlPoint_2 = ccp(winSize.width*0.12,winSize.height*0.12);
            config.endPosition = ccp(winSize.width*0.24,winSize.height*0.25);
            id bezier = [CCBezierBy actionWithDuration:0.5f bezier:config];
            
            id ease = [CCEaseExponentialOut actionWithAction:bezier];
            id rote = [CCRotateBy actionWithDuration:0.5f angle:-28];
            id ease2 = [CCEaseElasticOut actionWithAction:rote];
            id snakeScale =[CCScaleBy actionWithDuration:1.0f scaleX:0.5f scaleY:1.0f];
            id ease3 = [CCEaseExponentialOut actionWithAction:snakeScale];
            id CCSequenceSnakeRote = [CCSequence actions:ease2,ease3, nil];
            
            id snakeScale2 =[CCScaleBy actionWithDuration:0.3f scaleX:2.0f scaleY:1.0f];
            id spawn2 = [CCSpawn actions:snakeScale2,ease, nil];
            
            id snakeMove = [CCMoveTo actionWithDuration:2.0f position:ccp(winSize.width*0.02, winSize.height*0.125)];
            id easeSnakeMove = [CCEaseExponentialOut actionWithAction:snakeMove];
            
            id Sequence =[CCSequence actions:easeSnakeMove,[CCDelayTime actionWithDuration:0.05],CCSequenceSnakeRote,[CCDelayTime actionWithDuration:0.3],spawn2,CallFunc, nil];
            [hebiSprite runAction:Sequence];
            
        }
        
    }
}

-(void)p5_finish_motion{
    
    [SoundEffect sePlay:@"ofn_se_p5_snakebite1.mp3"];

    [self removeChild:spriteImage[0] cleanup:YES];
    [self removeChild:bachNood cleanup:YES];
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    spriteImage[6]  = [CCSprite spriteWithSpriteFrameName:@"p5_oosuno3.png"];
    spriteImage[6].anchorPoint = ccp(0.8, 0.0);
    spriteImage[6].position = ccp(winSize.width*0.5,winSize.height*0.405);
    [self addChild: spriteImage[6]];
    
    id shaky = [CCShaky3D actionWithRange:10 shakeZ:NO grid:ccg(5, 5) duration:1.0];
    id block = [CCCallBlock actionWithBlock:^{
        int no =CCRANDOM_0_1()*3+1;
        if (no==4) {no = 3;}
        NSString *path = [[NSString alloc] initWithFormat:@"ofn_se_p5_dead%d.caf",no];
        [SoundEffect sePlay:path];
        [path release];
    }];
    id sequence = [CCSequence actions:[CCDelayTime actionWithDuration:0.5f],block,shaky,[CCStopGrid action],nil];
    [spriteImage[6] runAction:sequence];

    
}

- (void)deviceOrientationDidChange:(NSNotification*)notification {
    UIDeviceOrientation orientation;
    orientation = [UIDevice currentDevice].orientation;
    if(orientation == UIDeviceOrientationUnknown) {
        NSLog(@"不明");
        //[self p5_Action:0];
    }
    if(orientation == UIDeviceOrientationPortrait) {
        //NSLog(@"縦(ホームボタン下)");
        [self p5_Action:1];
        
    }
    if(orientation == UIDeviceOrientationPortraitUpsideDown) {
        //NSLog(@"縦(ホームボタン上)");
        [self p5_Action:3];
    }
    if(orientation == UIDeviceOrientationLandscapeLeft) {
        //ホームボタンが右の時
        [self p5_Action:0];
    }
    if(orientation == UIDeviceOrientationLandscapeRight) {
        NSLog(@"横(ホームボタン左)");
        [self p5_Action:2];
    }
}

-(void) dealloc
{
    [self removeAllChildrenWithCleanup:YES];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [super dealloc];
}


@end
